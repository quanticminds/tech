/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quanticminds.jme3.artemis.components;

import com.artemis.Component;
import com.jme3.math.Vector3f;

/**
 *
 * @author adriano
 */
public class Position extends Component {
    public Vector3f velocity;
    public Vector3f position;
    public float rotation;
}
