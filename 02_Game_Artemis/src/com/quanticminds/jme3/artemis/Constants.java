package com.quanticminds.jme3.artemis;

public final class Constants {
        public final class Levels {
            public static final String LEVEL_TOWNCITY = "Scenes/town/city.j3o";
        }
        
        public final class Models{
            public static final String PLAYER_MODEL = 
                                            "Models/Sinbad/Sinbad.mesh.j3o";
        }
	
	public final class Groups {
		public static final String PLAYER_BULLETS = "player bullets";
		public static final String PLAYER_SHIP = "player ship";
		public static final String ENEMY_SHIPS = "enemy ships";
		public static final String ENEMY_BULLETS = "enemy bullets";
	}

}
