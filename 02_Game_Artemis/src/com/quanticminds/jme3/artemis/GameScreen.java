/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quanticminds.jme3.artemis;

import com.artemis.World;
import com.jme3.app.Application;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.asset.AssetManager;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.quanticminds.jme3.artemis.systems.MoveSystem;
import com.quanticminds.jme3.artemis.systems.CollisionSystem;
import com.quanticminds.jme3.artemis.systems.SpatialSystem;
import java.util.Iterator;

/**
 *
 * @author adriano
 */
public class GameScreen extends AbstractAppState {
    private Game game;
    private World world;
    
    private Node rootNode;
    private Node levelRootNode;
    private AssetManager assetsManager;
    
    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
    
        this.game = (Game)app;
        this.assetsManager = this.game.getAssetManager();
        this.rootNode = this.game.getRootNode();
        this.world = new World();
        
//        spriteRenderSystem = world.setSystem(new SpriteRenderSystem(camera), true);
//        healthRenderSystem = world.setSystem(new HealthRenderSystem(camera), true);
//        hudRenderSystem = world.setSystem(new HudRenderSystem(camera), true);

        this.world.setSystem(new SpatialSystem(this));
        this.world.setSystem(new MoveSystem(this));
        
        this.world.initialize();

        this.loadLevel(Constants.Levels.LEVEL_TOWNCITY);
        this.attachLevel();
        
        EntityFactory.createPlayer(this, new Vector3f(3, 3, -40)).addToWorld();

//        for(int i = 0; 500 > i; i++) {
//                EntityFactory.createStar(this).addToWorld();
//        }
    }
    /**
     * loads the specified level node
     *
     * @param name
     */
    public void loadLevel(String name) {
        this.levelRootNode = (Node) this.assetsManager.loadModel(name);
//        Spatial mesh = this.worldRootNode.getChild("NavMesh");
    }

    /**
     * detaches the level and clears the cache
     */
    public void closeLevel() {
//        entities.clear();
//        newId = 0;
//        this.physicsSpace.removeAll(this.worldRootNode);
        this.rootNode.detachChild(this.getRootNode());
        
        //aqui eu devo remover as entitidades
    }

    /**
     * attaches the level node to the rootnode
     */
    public void attachLevel() {
//        this.physicsSpace.addAll(this.worldRootNode);
        this.rootNode.attachChild(this.levelRootNode);
    }
    
    @Override
    public void update(float tpf) {
        if(this.isEnabled()) {
            this.world.setDelta(tpf);
            this.world.process();
        }
    }
    
    @Override
    public void cleanup() {
        super.cleanup();
        //TODO: clean up what you initialized in the initialize method,
        //e.g. remove all spatials from rootNode
        //this is called on the OpenGL thread after the AppState has been detached
    }

    /**
     * @return the world
     */
    public World getWorld() {
        return this.world;
    }

    /**
     * @return the assetsManager
     */
    public AssetManager getAssetsManager() {
        return this.assetsManager;
    }

    /**
     * @return the levelRootNode
     */
    public Node getRootNode() {
        return this.rootNode;
    }
}
