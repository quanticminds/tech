/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quanticminds.jme3.artemis.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Mapper;
import com.artemis.systems.EntityProcessingSystem;
import com.artemis.utils.Bag;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.quanticminds.jme3.artemis.GameScreen;
import com.quanticminds.jme3.artemis.components.Collision;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author adriano
 */
public class CollisionSystem extends EntityProcessingSystem{

    @Mapper ComponentMapper<Collision> cmp;
    
    Node rootNode;
    private Bag<Node> nodes;
    private List<Entity> entities;
        
    public CollisionSystem(GameScreen gameScreen){
        super(Aspect.getAspectForOne(Collision.class));
        this.rootNode = gameScreen.getRootNode();
        
        this.nodes = new Bag<Node>();
        this.entities = new ArrayList<Entity>();
    }
    @Override
    protected void process(Entity e) {
             Collision ph = cmp.get(e);
            Node mesh = (Node)((Node)this.rootNode.getChild(0)).getChild(ph.modelName);
            
            if(mesh != null){
                mesh.setLocalTranslation(ph.position);
                if(!this.entities.contains(e)){
                    this.nodes.set(e.getId(), mesh);
                    this.entities.add(e);
                }
            }       
        
    }
}
