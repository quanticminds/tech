/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quanticminds.jme3.artemis.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Mapper;
import com.artemis.systems.EntityProcessingSystem;
import com.artemis.utils.Bag;
import com.jme3.asset.AssetManager;
import com.jme3.scene.Node;
import com.quanticminds.jme3.artemis.GameScreen;
import com.quanticminds.jme3.artemis.components.Spatial;
import java.util.HashMap;

/**
 *
 * @author adriano
 */
public class SpatialSystem extends EntityProcessingSystem {
    
    @Mapper 
    ComponentMapper<Spatial> ms;
    
    Node rootNode;
    AssetManager assetManager;
    HashMap<Integer, Node> nodes;
    
    public SpatialSystem(GameScreen gameScreen){
        super(Aspect.getAspectForOne(Spatial.class));
        this.assetManager = gameScreen.getAssetsManager();
        this.rootNode = gameScreen.getRootNode();
    }
    
    public Node getNodeSafe(int entityId){
        if(nodes.containsKey(entityId)) {
            return nodes.get(entityId);
        }
        return null;
    }
    
    public Node getNode(int entityId){
        return nodes.get(entityId);
    }

    @Override
    protected void initialize() {
        this.nodes = new HashMap<Integer, Node>();
        super.initialize();
    }

    @Override
    protected void finalize() throws Throwable {
        this.nodes.clear();
        super.finalize();
    }
    
    
    
    @Override
    protected void process(Entity e) {
//        Spatial sp = ms.get(e);
//        
//        if(sp.action == Spatial.Action.LOAD){
//            if(!this.entities.contains(e)){
//                Node spatial = (Node)this.assetManager.loadModel(sp.fileName);
//                ((Node)this.rootNode.getChild(0)).attachChild(spatial);
//                this.nodes.put(e.getId(), spatial);
//                this.entities.add(e);
//            }
//        }
    }

    @Override
    protected void inserted(Entity e) {
        Spatial sp = ms.get(e);
        
        if(!this.nodes.containsKey(e.getId())){
            Node spatial = (Node)this.assetManager.loadModel(sp.fileName);
            ((Node)this.rootNode.getChild(0)).attachChild(spatial);
            this.nodes.put(e.getId(), spatial);
        }
        super.inserted(e);
    }

    @Override
    protected void removed(Entity e) {
        if(this.nodes.containsKey(e.getId())){
            ((Node)this.rootNode.getChild(0))
                    .detachChild(this.nodes.get(e.getId()));
            this.nodes.remove(e.getId());
        }
        super.removed(e);
    }
}
