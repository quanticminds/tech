/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quanticminds.jme3.artemis.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Mapper;
import com.artemis.systems.EntityProcessingSystem;
import com.artemis.utils.Bag;
import com.jme3.bullet.control.CharacterControl;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.quanticminds.jme3.artemis.GameScreen;
import com.quanticminds.jme3.artemis.components.Move;
import com.quanticminds.jme3.artemis.components.Collision;
import com.quanticminds.jme3.artemis.components.Position;
import com.quanticminds.jme3.artemis.components.Spatial;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author adriano
 */
public class MoveSystem extends EntityProcessingSystem{

    @Mapper ComponentMapper<Position> cmp;    
    
    ComponentMapper<Spatial> cms;    
    
    private Node rootNode;
    private Bag<CharacterControl> nodes;
    private SpatialSystem spatialSystem;
        
    public MoveSystem(GameScreen gameScreen){
        super(Aspect.getAspectForOne(Position.class));
        this.rootNode = gameScreen.getRootNode();
    }

    @Override
    protected void initialize() {
        this.spatialSystem = this.world.getSystem(SpatialSystem.class);
        this.nodes = new Bag<CharacterControl>();
    }
    
    @Override
    protected void process(Entity e) {
//        Position p = cmp.get(e);
//
//        if(this.nodes.isIndexWithinBounds(e.getId())){
//            this.nodes.get(e.getId())
//                    .setPhysicsLocation(p.position);
//        }
    }

    @Override
    protected void inserted(Entity e) {            
        Position p = cmp.get(e);        
        CharacterControl cc = this.spatialSystem.getNode(e.getId())
                .getControl(CharacterControl.class);

        if(cc != null){
            cc.setPhysicsLocation(p.position);
            if(!this.nodes.isIndexWithinBounds(e.getId())){
                this.nodes.set(e.getId(), cc);
            }
        }
        
        super.inserted(e);
    }

    @Override
    protected void removed(Entity e) {
        if(this.nodes.isIndexWithinBounds(e.getId())){
            this.nodes.remove(e.getId());
        }
        super.removed(e);
    }
    
    
}
