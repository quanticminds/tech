/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quanticminds.app.state;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;

/**
 *
 * @author Adriano Ribeiro
 */
public class GameLogicAppState extends AbstractAppState {
    
    SimpleApplication app;
    
    @Override
    public void initialize(AppStateManager stateManager, Application app){
        super.initialize(stateManager,app);
        
        this.app = (SimpleApplication)app;
    }
    
    @Override
    public void update(float tpf){
        if(isEnabled()){
            
        }
    }
}
