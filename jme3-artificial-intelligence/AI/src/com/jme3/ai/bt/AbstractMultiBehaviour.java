/**
 * The MIT License (MIT)
 * Copyright (c) 2013 Adriano Ribeiro <adribeiro@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 * 
 **/


package com.jme3.ai.bt;

import java.util.Iterator;
import java.util.LinkedList;

/**
 *
 * @author adriano.ribeiro
 */
public abstract class AbstractMultiBehaviour extends AbstractBehaviour{
    
    /**
     *
     */ 
    public AbstractMultiBehaviour(String name) {
        super(name);
        this.controller = new MultiBehaviourController(this);
    }
   
   /**
    *
    */    
    @Override
    public void doExecute(){
        
        if(this.controller.isFinished()){
            return;
        }
        
        if(((MultiBehaviourController)this.controller).getBehaviour() == null){
            return;
        }
        
        if(!this.controller.isStarted()){
            this.controller.behaviourStart();
        }
        
        if(!((MultiBehaviourController)this.controller).isStarted()){
            ((MultiBehaviourController)this.controller).behaviourStart();
        }
        
        ((MultiBehaviourController)this.controller).getBehaviour().doExecute();

        if(((MultiBehaviourController)this.controller).isFinished()){

            ((MultiBehaviourController)this.controller).behaviourFinish();
            if(((MultiBehaviourController)this.controller).isSucceded()){
            
                this.childBehaviourSucceeded();
                
            }else if(((MultiBehaviourController)this.controller).isFailed()){
                
                this.childBehaviourFailed();
            }
        }
    }

    @Override
    public void start() {
        
    }

    @Override
    public void finish() {
        
    }

    
    public abstract void addBehaviour(Behaviour behaviour);
      
    public abstract boolean hasNextBehaviour();
      
    public abstract Behaviour getNextBehaviour();
    
    public abstract int getBehavioursSize();
   
    public abstract Behaviour getFirstBehaviour();
    
    public abstract Iterator<Behaviour> getBehaviours();
    
   /**
    *
    */
    public abstract void childBehaviourSucceeded();
    
   /**
    *
    */
    public abstract void childBehaviourFailed();
}
