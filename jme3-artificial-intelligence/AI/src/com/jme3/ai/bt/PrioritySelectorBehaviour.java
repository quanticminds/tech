/**
 * The MIT License (MIT)
 * Copyright (c) 2013 Adriano Ribeiro <adribeiro@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 * 
 **/


package com.jme3.ai.bt;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;


/**
 * http://aigamedev.com/open/articles/selector/
 * 
 * @author adriano.ribeiro
 */
public class PrioritySelectorBehaviour extends AbstractMultiBehaviour{
    
    private LinkedList<PriorityDecorator> behaviours;
    private int currentIndex = -1;
    
    /**
     *
     */ 
    public PrioritySelectorBehaviour(String name) {
        super(name);
        this.behaviours = new LinkedList<PriorityDecorator>();
    }

    @Override
    public void addBehaviour(Behaviour behaviour) {
        this.behaviours.add((PriorityDecorator)behaviour);
        Collections.sort(this.behaviours);
    }

    @Override
    public boolean hasNextBehaviour() {
        if((this.currentIndex + 1) < this.behaviours.size()){
            return true;
        }
        
        return false;
    }

    @Override
    public Behaviour getNextBehaviour() {
        Behaviour b = ((MultiBehaviourController)this.getController()).getBehaviour();
        int nextIdx = this.behaviours.indexOf(b)+1;
        if(nextIdx < this.behaviours.size()){
            Behaviour command = this.behaviours.get(nextIdx);
            this.currentIndex = nextIdx;
            return command;
        }else{
            return null;
        }
    }

    @Override
    public int getBehavioursSize() {
        return this.behaviours.size();
    }

    @Override
    public Behaviour getFirstBehaviour() {
        return this.behaviours.peekFirst();
    }

    @Override
    public Iterator<Behaviour> getBehaviours() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isConditionsOk() {
        return this.behaviours.size() > 0;
    }    
    
   /**
    *
    */    
    @Override
    public void start(){
        this.currentIndex = this.behaviours.indexOf(this.getFirstBehaviour());
        this.controller.setBehaviour(this.getFirstBehaviour());
    }
    
    

   /**
    * So, just make sure the following termination status codes are 
    * dealt with appropriately:
    * 
    * - If a child behavior succeeds, the selector can terminate 
    *  successfully also.
    * - In case a child behavior fails cleanly, the selector may backtrack and 
    * try the next child in order.
    * - When a critical error occurs, the selector should bail out with the 
    * same error.
    */
    @Override
    public void childBehaviourSucceeded(){
        
        if(this.hasNextBehaviour()){
            
            Behaviour b = this.getNextBehaviour();
            this.controller.setBehaviour(b);
            
            if(b.isConditionsOk()){
                //run next behaviour
                this.doExecute();
            }
            
        }else{

            this.controller.finalizeWithSuccess();
        }
    }
    
   /**
    *
    */
    @Override
    public void childBehaviourFailed(){
        this.controller.finalizeWithFailure();
    }

    
}
