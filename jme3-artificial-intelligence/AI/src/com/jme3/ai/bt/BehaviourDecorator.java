/**
 * The MIT License (MIT)
 * Copyright (c) 2013 Adriano Ribeiro <adribeiro@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 * 
 **/


package com.jme3.ai.bt;

/**
 *
 * @author adriano.ribeiro
 */
public abstract class BehaviourDecorator extends AbstractBehaviour{

    protected Behaviour behaviour;
    
    public BehaviourDecorator(Behaviour behaviour) {
        this(behaviour, "Command Decorated Name");        
    }
    
    public BehaviourDecorator(Behaviour behaviour, String name) {
        super(name);
        
        this.behaviour = behaviour;
        this.behaviour.getController().setBehaviour(this);        
    }

    @Override
    public boolean isConditionsOk(){
        return this.behaviour.isConditionsOk();
    }

    @Override
    public void start(){
        this.behaviour.start();
    }

    @Override
    public void finish(){
        this.behaviour.finish();
    }

    @Override
    public BehaviourController getController() {
        return this.behaviour.getController();
    }
}
