/**
 * The MIT License (MIT)
 * Copyright (c) 2013 Adriano Ribeiro <adribeiro@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 * 
 **/


package com.jme3.ai.bt;

import java.util.Iterator;
import java.util.LinkedList;

/**
 *
 * @author adriano.ribeiro
 */
public class MultiBehaviourController extends BehaviourController{
  

    public MultiBehaviourController(Behaviour behaviour) {
        super(behaviour);
    }
    
    public void addBehaviour(Behaviour behaviour){
        ((AbstractMultiBehaviour)this.behaviour).addBehaviour(behaviour);
    }
    
    public int getBehavioursSize(){
        return ((AbstractMultiBehaviour)this.behaviour).getBehavioursSize();
    }
    
    public Behaviour getBehaviour(){
        return this.behaviour;
    }
    
    public Behaviour getFirstBehaviour(){
        return ((AbstractMultiBehaviour)this.behaviour).getFirstBehaviour();
    }
    
    public Iterator<Behaviour> getBehaviours(){
        return ((AbstractMultiBehaviour)this.behaviour).getBehaviours();
    }
    
    @Override
    public void reset(){
        super.reset();
    }
}
