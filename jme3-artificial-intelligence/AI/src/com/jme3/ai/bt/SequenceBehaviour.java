/**
 * The MIT License (MIT)
 * Copyright (c) 2013 Adriano Ribeiro <adribeiro@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 * 
 **/


package com.jme3.ai.bt;

import java.util.Iterator;
import java.util.LinkedList;

/**
 * http://aigamedev.com/open/article/sequence/
 * 
 * @author adriano.ribeiro
 */
public class SequenceBehaviour extends AbstractMultiBehaviour{
        
    private LinkedList<Behaviour> behaviours = new LinkedList<Behaviour>();
    private boolean childSucceded = false;

    /**
     *
     */ 
    public SequenceBehaviour(String name) {
        super(name);
    }

    @Override
    public void addBehaviour(Behaviour behaviour){
        this.behaviours.add(behaviour);
    }
    
    @Override
    public boolean hasNextBehaviour(){
        Behaviour b = ((MultiBehaviourController)this.getController()).getBehaviour();
        if((this.behaviours.indexOf(b)+1) < this.behaviours.size()){
            return true;
        }
        
        return false;
    }
    
    @Override            
    public Behaviour getNextBehaviour(){
        Behaviour b = ((MultiBehaviourController)this.getController()).getBehaviour();
        int nextIdx = this.behaviours.indexOf(b)+1;
        if(nextIdx < this.behaviours.size()){
            Behaviour command = this.behaviours.get(nextIdx);
            return command;
        }else{
            return null;
        }
    }
    
    @Override
    public int getBehavioursSize(){
        return this.behaviours.size();
    }
   
    @Override
    public Behaviour getFirstBehaviour(){
        return this.behaviours.getFirst();
    }
    
    @Override
    public Iterator<Behaviour> getBehaviours(){
        return this.behaviours.iterator();
    }
    
   /**
    *
    */
    @Override
    public boolean isConditionsOk(){
        return this.behaviours.size() > 0;
    }    
    
   /**
    *
    */    
    @Override
    public void start(){
        this.controller.setBehaviour(this.getFirstBehaviour());
    }
    
    

   /**
    * This gives you the basic elements of a search:
    * 
    * - If a child behavior succeeds, the sequence continues its 
    * execution.
    * - If a child behavior fails cleanly, then the sequence code backtracks to 
    * find other candidate behaviors.
    * - If a child behavior has an unexpected error, the sequence also returns 
    * that error since the search cannot continue.
    * 
    */
    @Override
    public void childBehaviourSucceeded(){
        
        if(!this.childSucceded)
            this.childSucceded = true;
        
        if(this.hasNextBehaviour()){
            
            Behaviour b = this.getNextBehaviour();
            this.controller.setBehaviour(b);
            
            if(b.isConditionsOk()){
                //run next behaviour
                this.doExecute();
            }
            
        }else{

            this.controller.finalizeWithSuccess();
        }
    }
    
   /**
    *
    */
    @Override
    public void childBehaviourFailed(){
        
        // all childreen are failed
        if(!this.childSucceded && !this.hasNextBehaviour())
            this.controller.finalizeWithFailure();
    }
}
