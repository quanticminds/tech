/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jme3.ai.tests;

import com.jme3.app.SimpleApplication;
import com.jme3.system.AppSettings;

/**
 *
 * @author Adriano Ribeiro
 */
public class TestBasicBehaviorTree extends SimpleApplication {

    public static void main(String[] args) {
        TestBasicBehaviorTree app = new TestBasicBehaviorTree();
        AppSettings settings = new AppSettings(true);
        settings.setResolution(1368,720);
        app.setSettings(settings);
        app.start();
    }

    @Override
    public void start() {

    }

    @Override
    public void simpleInitApp() {
        
    }

    @Override
    public void simpleUpdate(float tpf) {

    }
}
