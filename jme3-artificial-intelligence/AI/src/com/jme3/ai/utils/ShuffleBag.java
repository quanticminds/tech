/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jme3.ai.utils;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 *
 * @author Adriano Ribeiro
 * http://gamedev.tutsplus.com/tutorials/implementation/shuffle-bags-making-random-feel-more-random/
 * 
 */
public class ShuffleBag<E> {
    private List<E> elements;
    private E curElement;
    private int curIndex = -1;
    private Random random;
    
    public ShuffleBag(){
        this.elements = new LinkedList<E>();
    }
        
    
    public void add(E element){
        this.add(element, 1);
    }
    
    public void add(E element, int frequency){
        for(int i = 0; i < frequency; i++){
            this.elements.add(element);
        }
        
        this.curIndex = this.elements.size()-1;
    }
    
    public E getNext(){
        
        if(curIndex < 1){
            this.curIndex = this.elements.size() - 1;
            this.curElement = this.elements.get(0);
            return this.curElement;
        }
        
        int idx = this.random.nextInt(curIndex);
        this.curElement = this.elements.get(idx);
        this.elements.set(idx, this.elements.get(this.curIndex));
        this.elements.set(curIndex,this.curElement);
        this.curIndex--;
        
        return this.curElement;        
    }
}
