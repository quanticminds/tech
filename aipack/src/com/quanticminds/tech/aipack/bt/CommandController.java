/**
 * The MIT License (MIT)
 * Copyright (c) 2013 Adriano Ribeiro <adribeiro@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 * 
 **/


package com.quanticminds.tech.aipack.bt;

/**
 *
 * @author adriano.ribeiro
 */
public class CommandController {

    /**
    * true is command is finished
    */
    private boolean finished;
    
    /** 
     * true command finish ok, false command failed
     */
    private boolean success;
    
    /**
     * command already running
     */
    protected boolean started;
    
    /**
     * real code to doAction
     */
    protected Command currentCommand;
    

    public CommandController(Command command) {
        this.finished = false;
        this.success = false;
        this.started = false;
        this.currentCommand = command;
    }
    
    /*
     * controlled init for command
     */
    public void commandStart(){
        this.started = true;
        this.currentCommand.start();
    }
    
    /*
     * controlled finish for command
     */
    public void commandFinish(){
        this.finished = false;
        this.started = false;
        this.currentCommand.finish();
    }

    /*
     * set new command to control
     */
    public void setCommand(Command command) {
        this.currentCommand = command;
    }
    
    /*
     * force command terminate with success
     */
    public void finalizeWithSuccess(){
        this.finished = true;
        this.success = true;
    }
    
    /*
     * force command terminate with failure
     */
    public void finalizeWithFailure(){
        this.finished = true;
        this.success = false;        
    }

    /*
     * command finish with success
     */
    public boolean isSucceded() {
        return this.success;
    }
    
    /*
     * command failed
     */
    public boolean isFailed() {
        return !this.success;
    }

    /*
     * command isn't running
     */
    public boolean isFinished() {
        return this.finished;
    }

    /*
     * command already initialized
     */
    public boolean isStarted() {
        return this.started;
    }
    
    /*
     * not initialized yet.
     */
    public void reset(){
        this.finished = false;
    }
}
