/**
 * The MIT License (MIT)
 * Copyright (c) 2013 Adriano Ribeiro <adribeiro@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 * 
 **/

package com.quanticminds.tech.aipack.bt;

import com.quanticminds.tech.aipack.ContextData;
import java.util.Iterator;

/**
 *
 * @author adriano.ribeiro
 */
public class Selector extends AbstractSerialCommand {

    public Selector(ContextData world) {
        super(world);
    }

    public Selector(ContextData world, String name) {
        super(world, name);
    }
    
    public Command chooseNewCommand(){
        Iterator<Command> itr = this.controller.getCommands();
        while(itr.hasNext()){
            Command command = itr.next();
            if(command.isConditionsOk()){
               return command; 
            }
        }        
        return null;
    }

    @Override
    public void start() {
         Command command = chooseNewCommand();
         command.getController().commandStart();
         this.controller.setCommand(command);
         
    }
    
    @Override
    public void commandFailed() {
        Command command = chooseNewCommand();
        
        if(command == null){
            this.controller.finalizeWithFailure();
        }
    }

    @Override
    public void commandSucceeded() {
        this.controller.finalizeWithSuccess();
    }
}
