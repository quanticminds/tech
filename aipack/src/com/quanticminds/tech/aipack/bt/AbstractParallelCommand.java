/**
 * The MIT License (MIT)
 * Copyright (c) 2013 Adriano Ribeiro <adribeiro@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 * 
 **/


package com.quanticminds.tech.aipack.bt;

import com.quanticminds.tech.aipack.ContextData;

/**
 *
 * @author adriano.ribeiro
 */
public abstract class AbstractParallelCommand implements Command{

    private String name;
    protected ContextData world;
    protected MultiCommandController controller;
    
    /**
     *
     */
    public AbstractParallelCommand(ContextData world) {
        this(world,"Parallel Commands");
    }
    
    /**
     *
     */ 
    public AbstractParallelCommand(ContextData world, String name) {
        this.world = world;
        this.name = name;
        this.controller = new MultiCommandController(this);
    }
    
    /**
     *
     */ 
    public String getName(){
        return this.name;
    }
     
   /**
    *
    */    
    public CommandController getController() {
        return this.controller;
    }

   /**
    *
    */
    public boolean isConditionsOk(){
        return this.controller.getCommandsCount() > 0;
    }
    
   /**
    *
    */    
    public void doAction(){
        
        if(this.controller.isFinished()){
            return;
        }
        
        if(this.controller.getCommand() == null){
            return;
        }
        
        if(!this.controller.isStarted()){
            this.controller.commandStart();
        }
        
        CommandController commandController = this.controller.getCommand().getController();
        
        if(!commandController.isStarted()){
            commandController.commandStart();
        }
        
        this.controller.getCommand().doAction();

        if(commandController.isFinished()){
            commandController.commandFinish();
            
            if(commandController.isSucceded()){            
                this.commandSucceeded();
                
            }else if(commandController.isFailed()){                
                this.commandFailed();
            }
        }
        
        // run next command if it exist
        if(this.controller.hasNextCommand()){
            this.controller.getNextCommand();
        
        // if not back to beging until finish
        }else{
            this.controller.setCommand(this.controller.getFirstCommand());
        }
    }
    
   /**
    *
    */    
    public void start(){

    }

   /**
    *
    */
    public void finish(){
        
    }
    
   /**
    *
    */
    public abstract void commandSucceeded();
    
   /**
    *
    */
    public abstract void commandFailed();
}
