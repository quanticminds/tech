/**
 * The MIT License (MIT)
 * Copyright (c) 2013 Adriano Ribeiro <adribeiro@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 * 
 **/


package com.quanticminds.tech.aipack.bt;

import java.util.Iterator;
import java.util.LinkedList;

/**
 *
 * @author adriano.ribeiro
 */
public class MultiCommandController extends CommandController{
  
    protected LinkedList<Command> commands = new LinkedList<Command>();

    public MultiCommandController(Command command) {
        super(command);
    }
    
    public void addCommand(Command command){
        commands.add(command);
    }
    
    public int getCommandsCount(){
        return this.commands.size();
    }
    
    public Command getCommand(){
        return this.currentCommand;
    }
    
    public Command getFirstCommand(){
        return this.commands.getFirst();
    }
    
    public Iterator<Command> getCommands(){
        return this.commands.iterator();
    }
    
    public boolean hasNextCommand(){
        if((this.commands.indexOf(this.currentCommand)+1) < this.commands.size()){
            return true;
        }
        return false;
    }
        
    public Command getNextCommand(){
        int next = this.commands.indexOf(this.currentCommand)+1;
        if(next < this.commands.size()){
            Command command = this.commands.get(next);
            setCommand(command);
            return command;
        }else{
            return null;
        }
    }
           
    @Override
    public void reset(){
        super.reset();
        this.setCommand(this.commands.getFirst());
    }
}
