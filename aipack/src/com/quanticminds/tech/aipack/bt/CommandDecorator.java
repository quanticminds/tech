/**
 * The MIT License (MIT)
 * Copyright (c) 2013 Adriano Ribeiro <adribeiro@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 * 
 **/


package com.quanticminds.tech.aipack.bt;

import com.quanticminds.tech.aipack.DefaultContextData;

/**
 *
 * @author adriano.ribeiro
 */
public abstract class CommandDecorator extends AbstractCommand{

    protected Command command;
    
    public CommandDecorator(DefaultContextData world, Command command) {
        this(world, command, "Command Decorated Name");        
    }
    
    public CommandDecorator(DefaultContextData world, Command command, String name) {
        super(world, name);
        
        this.command = command;
        this.command.getController().setCommand(this);        
    }

    @Override
    public boolean isConditionsOk(){
        return this.command.isConditionsOk();
    }

    @Override
    public void start(){
        this.command.start();
    }

    @Override
    public void finish(){
        this.command.finish();
    }

    @Override
    public CommandController getController() {
        return this.command.getController();
    }
}
