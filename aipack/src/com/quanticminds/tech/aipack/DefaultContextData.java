/**
 * The MIT License (MIT)
 * Copyright (c) 2013 Adriano Ribeiro <adribeiro@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 * 
 **/


package com.quanticminds.tech.aipack;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 *
 * @author adriano.ribeiro
 */
public class DefaultContextData implements ContextData{

    /**
     * thread safe data storage.
     */
    private ConcurrentMap<String,Object> worldData = new ConcurrentHashMap<String, Object>();

    public boolean containsData(String dataKey){
        if(this.worldData.containsKey(dataKey)){
            return true;
        }        
        return false;
    }
    /**
     * add or update new data to world shared structure.
     * @param dataKey
     * @param value 
     */
    public void setData(String dataKey, Object value){
        this.worldData.put(dataKey, value);
    }
    
    /**
     * query data from world shared data by key and Type
     * @param <T>
     * @param dataKey
     * @return return null if htere is not exist data for this key.
     */
    public <T> T getData(String dataKey){
        return (T)this.worldData.get(dataKey);  
    }
    
    public void clear(){
        this.worldData.clear();
    }
}
