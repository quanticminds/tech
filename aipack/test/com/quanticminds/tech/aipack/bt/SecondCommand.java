/**
 * The MIT License (MIT)
 * Copyright (c) 2013 Adriano Ribeiro <adribeiro@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 * 
 **/


package com.quanticminds.tech.aipack.bt;

import com.quanticminds.tech.aipack.ContextData;

/**
 *
 * @author adriano.ribeiro
 */
public class SecondCommand extends AbstractCommand {
    
    int counter = -1;
    
    public SecondCommand(ContextData world, int counter) {
        this(world, "SecondCommand");
    }
    
    public SecondCommand(ContextData world, String name){
        super(world, name);
    }
    
    @Override
    public boolean isConditionsOk() {
        
        if(!this.world.containsData("secondCanRun")){
            return false;
        }
        
        if(this.world.getData("secondCanRun")){
            return true;
        }
        
        return false;
    }

    @Override
    public void start() {
        this.counter = 0;
    }

    @Override
    public void finish() {
        this.counter = -1;
    }

    public void doAction() {
        this.counter += 1;
       
        if(this.counter == 1){
            this.controller.finalizeWithSuccess();
        }else{
            this.controller.finalizeWithFailure();
        }
    }    
}
