/**
 * The MIT License (MIT)
 * Copyright (c) 2013 Adriano Ribeiro <adribeiro@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 * 
 **/

package com.quanticminds.tech.aipack.bt;

import com.quanticminds.tech.aipack.DefaultContextData;
import com.quanticminds.tech.aipack.ContextData;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


/**
 *
 * @author adriano
 */
public class CommandTest {
    ContextData world = new DefaultContextData();
    
    public CommandTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        
    }
    
    @AfterClass
    public static void tearDownClass() {
        
    }
    
    @Before
    public void setUp() {
        world.setData("canRun", true);
    }
    
    @After
    public void tearDown() {
        world.clear();
    }

    @Test
    public void testSimpleCommandStarted() {
        Command command = new SimpleCommand(world, "My simple command");
        command.getController().commandStart();
        assertEquals(command.getController().isFinished(), false);
        assertEquals(command.getController().isStarted(), true);
    }
    
    @Test
    public void testSimpleCommandSuccess() {
        Command command = new SimpleCommand(world, "My simple command");
        command.getController().commandStart();
        command.doAction();
        assertEquals(command.getController().isFinished(), true);
        assertEquals(command.getController().isSucceded(), true);
    }
    
    @Test
    public void testSimpleCommandFailed() {
        Command command = new SimpleCommand(world, "My simple command");
        world.setData("canRun", false);
        command.getController().commandStart();
        command.doAction();        
        command.doAction();
        assertEquals(command.getController().isFinished(), true);
        assertEquals(command.getController().isSucceded(), false);
    }
    
    @Test
    public void testSequenceCommandStarted() {
        Command command = new Sequence(world,"My command sequence");
        
        ((MultiCommandController)command.getController())
                .addCommand(new SimpleCommand(world, "My simple command"));
        ((MultiCommandController)command.getController())
                .addCommand(new SecondCommand(world, "Second command"));        

        command.getController().commandStart();
       
        assertEquals(command.getController().isFinished(), false);
        assertEquals(command.getController().isSucceded(), false);
    }
    
    @Test
    public void testSequenceCommandSuccess() {
        Command command = new Sequence(world,"My command sequence");
        
        ((MultiCommandController)command.getController())
                .addCommand(new SimpleCommand(world, "My simple command"));
        ((MultiCommandController)command.getController())
                .addCommand(new SecondCommand(world, "Second command"));        

        command.doAction();
        command.doAction();
        
        assertEquals(command.getController().isFinished(), true);
        assertEquals(command.getController().isSucceded(), true);
    }
    
    @Test
    public void testSequenceCommandFailed() {
        Command command = new Sequence(world,"My command sequence");
        
        ((MultiCommandController)command.getController())
                .addCommand(new SimpleCommand(world, "My simple command"));
        ((MultiCommandController)command.getController())
                .addCommand(new SecondCommand(world, "Second command"));        

        command.doAction();
        
        assertEquals(command.getController().isFinished(), false);
        assertEquals(command.getController().isSucceded(), false);
    }
    
        @Test
    public void testSelectorCommandStarted() {
        Command command = new Selector(world,"My command sequence");
        
        ((MultiCommandController)command.getController())
                .addCommand(new SimpleCommand(world, "My simple command"));
        ((MultiCommandController)command.getController())
                .addCommand(new SecondCommand(world, "Second command"));        

        command.getController().commandStart();
       
        assertEquals(command.getController().isFinished(), false);
        assertEquals(command.getController().isSucceded(), false);
    }
    
    @Test
    public void testSelectorCommandSuccess() {
        Command command = new Selector(world,"My command sequence");
        
        ((MultiCommandController)command.getController())
                .addCommand(new SimpleCommand(world, "My simple command"));
        ((MultiCommandController)command.getController())
                .addCommand(new SecondCommand(world, "Second command"));        

        command.doAction();
        command.doAction();
        
        assertEquals(command.getController().isFinished(), true);
        assertEquals(command.getController().isSucceded(), true);
    }
    
    @Test
    public void testSelectorCommandFailed() {
        Command command = new Selector(world,"My command sequence");
        
        ((MultiCommandController)command.getController())
                .addCommand(new SelectableOneCommand(world, "My simple command"));
        ((MultiCommandController)command.getController())
                .addCommand(new SecondCommand(world, "Second command"));        

        command.doAction();
        command.doAction();
        
        assertEquals(command.getController().isFinished(), true);
        assertEquals(command.getController().isSucceded(), false);
    }
}
