/**
 * *******************
 *
 * Copyright (c) 2011, 2012 Quantic Minds Software Ltda. All Rights Reserved.
 *
 *********************
 * Game.java
 *
 * Version: 0.1.0
 *
 *
 */
package com.quanticminds.jme3;

import com.jme3.app.SimpleApplication;
import com.jme3.input.ChaseCamera;
import com.jme3.niftygui.NiftyJmeDisplay;
import com.quanticminds.jme3.controls.UserCommandControl;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.render.TextRenderer;
import de.lessvoid.nifty.screen.Screen;

/**
 *
 * @author adriano
 */
public class Game extends SimpleApplication {
    private GameManager gameManager;
    private World gameWorld;
    private UserCommandControl commandControl;
    private Nifty nifty;
    private NiftyJmeDisplay niftyDisplay;
    private TextRenderer statusText;
    private ChaseCamera chaseCam;

    /**
     * starts the nifty gui system
     */
    private void startNifty() {
        guiNode.detachAllChildren();
        guiNode.attachChild(fpsText);
        niftyDisplay = new NiftyJmeDisplay(assetManager,
                inputManager,
                audioRenderer,
                guiViewPort);
        nifty = niftyDisplay.getNifty();
//        try {
//            nifty.fromXml("Interface/ClientUI.xml", "load_game", this);
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//        statusText = nifty.getScreen("load_game").findElementByName("layer").findElementByName("panel").findElementByName("status_text").getRenderer(TextRenderer.class);
        guiViewPort.addProcessor(niftyDisplay);
    }

    public Nifty getHudManager() {
        return this.nifty;
    }
    
    public World getWorld(){
        return this.gameWorld;
    }

    public GameManager getGameManager(){
        return this.gameManager;
    }
    /**
     *
     * @return
     */
    public ChaseCamera getChaseCamera(){
        return this.chaseCam;
    }
    
    @Override
    public void simpleInitApp() {
        startNifty();

        this.chaseCam = new ChaseCamera(cam, inputManager);
        this.chaseCam.setSmoothMotion(true);
        this.chaseCam.setChasingSensitivity(100);
        this.chaseCam.setTrailingEnabled(true);
        this.gameWorld = new World(this);
        getStateManager().attach(this.gameWorld);
        this.gameManager = new GameManager(this);
    }

    /**
     * sets the status text of the main login view, threadsafe
     *
     * @param text
     */
    public void setStatusText(final String text) {
//        enqueue(new Callable<Void>() {
//
//            public Void call() throws Exception {
//                statusText.setText(text);
//                return null;
//            }
//        });
    }

    /**
     * updates the list of players in the lobby gui, threadsafe
     */
    public void updatePlayerData() {
//        Logger.getLogger(ClientMain.class.getName()).log(Level.INFO, "Updating player data");
//        enqueue(new Callable<Void>() {
//
//            public Void call() throws Exception {
//                Screen screen = nifty.getScreen("lobby");
//                Element panel = screen.findElementByName("layer").findElementByName("panel").findElementByName("players_panel").findElementByName("players_list").findElementByName("panel");
//                List<PlayerData> players = PlayerData.getHumanPlayers();
//                for (Iterator<Element> it = new LinkedList<Element>(panel.getElements()).iterator(); it.hasNext();) {
//                    Element element = it.next();
//                    element.markForRemoval();//disable();
//                }
//                TextCreator labelCreator = new TextCreator("unknown player");
//                labelCreator.setStyle("my-listbox-item-style");
//                for (Iterator<PlayerData> it = players.iterator(); it.hasNext();) {
//                    PlayerData data = it.next();
//                    Logger.getLogger(ClientMain.class.getName()).log(Level.INFO, "List player {0}", data);
//                    labelCreator.setText(data.getStringData("name"));
//                    labelCreator.create(nifty, screen, panel);
//                }
//                return null;
//            }
//        });
    }

    /**
     * brings up the lobby display
     */
    public void lobby() {
//        chaseCam.setDragToRotate(false);
        inputManager.setCursorVisible(true);
//        nifty.gotoScreen("lobby");
    }

    /**
     * send message to start selected game
     */
    public void startGame() {
        //TODO: map selection
//        client.send(new StartGameMessage("Scenes/MonkeyZone.j3o"));
    }

    /**
     * loads a level, basically does everything on a seprate thread except
     * updating the UI and attaching the level
     *
     * @param name
     * @param modelNames
     */
    public void loadLevel(final String name, final String[] modelNames) {
//        final TextRenderer statusText = nifty.getScreen("load_level").findElementByName("layer").findElementByName("panel").findElementByName("status_text").getRenderer(TextRenderer.class);
//        if (name.equals("null")) {
//            enqueue(new Callable<Void>() {
//
//                public Void call() throws Exception {
//                    worldManager.closeLevel();
//                    lobby();
//                    return null;
//                }
//            });
//            return;
//        }
//        new Thread(new Runnable() {
//
//            public void run() {
//                try {
//                    enqueue(new Callable<Void>() {
//
//                        public Void call() throws Exception {
//                            nifty.gotoScreen("load_level");
//                            statusText.setText("Loading Terrain..");
//                            return null;
//                        }
//                    }).get();
//                    worldManager.loadLevel(name);
//                    enqueue(new Callable<Void>() {
//
//                        public Void call() throws Exception {
//                            statusText.setText("Creating NavMesh..");
//                            return null;
//                        }
//                    }).get();
//                    worldManager.createNavMesh();
//                    enqueue(new Callable<Void>() {
//
//                        public Void call() throws Exception {
//                            statusText.setText("Loading Models..");
//                            return null;
//                        }
//                    }).get();
//                    worldManager.preloadModels(modelNames);
//                    enqueue(new Callable<Void>() {
//
//                        public Void call() throws Exception {
//                            worldManager.attachLevel();
//                            statusText.setText("Done Loading!");
//                            nifty.gotoScreen("default_hud");
//                            inputManager.setCursorVisible(false);
////                            chaseCam.setDragToRotate(false);
//                            return null;
//                        }
//                    }).get();
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }).start();
    }

    public void bind(Nifty nifty, Screen screen) {
    }

    public void onStartScreen() {
    }

    public void onEndScreen() {
    }
}
