/**
 * *******************
 *
 * Copyright (c) 2011, 2012 Quantic Minds Software Ltda. All Rights Reserved.
 *
 *********************
 * World.java
 *
 * Version: 0.1.0
 *
 *
 */
package com.quanticminds.jme3;

import com.jme3.app.state.AbstractAppState;
import com.jme3.asset.AssetManager;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.PhysicsSpace;
import com.jme3.bullet.collision.PhysicsCollisionObject;
import com.jme3.bullet.collision.PhysicsRayTestResult;
import com.jme3.bullet.control.CharacterControl;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.Control;
import com.quanticminds.jme3.controls.AutonomousCharacterControl;
import com.quanticminds.jme3.controls.AutonomousControl;
import com.quanticminds.jme3.controls.CharacterAnimControl;
import com.quanticminds.jme3.controls.CommandControl;
import com.quanticminds.jme3.controls.ManualCharacterControl;
import com.quanticminds.jme3.controls.ManualControl;
import com.quanticminds.jme3.controls.MovementControl;
import com.quanticminds.jme3.controls.UserCommandControl;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author adriano
 */
public class World extends AbstractAppState {

    Game gameApp;
    AssetManager assetManager;
    Node rootNode;
    Node worldRootNode;
    private long myPlayerId = -2;
    private long myGroupId = -2;
    //private NavMesh navMesh = new NavMesh();
    private HashMap<Long, Spatial> entities = new HashMap<Long, Spatial>();
    private int newId = 0;
    private PhysicsSpace physicsSpace;
    private List<Control> userControls = new LinkedList<Control>();
    private UserCommandControl commandInterface;
    private CommandControl worldCommandControl;

    public World(Game gameApp) {
        this.gameApp = gameApp;
        this.rootNode = this.gameApp.getRootNode();
        this.assetManager = this.gameApp.getAssetManager();
        this.gameApp.getStateManager().attach(new BulletAppState());
        this.physicsSpace = this.gameApp.getStateManager()
                .getState(BulletAppState.class).getPhysicsSpace();
        this.worldCommandControl = new CommandControl(this, 0);
        this.rootNode.addControl(this.worldCommandControl);
    }

    @Override
    public void cleanup() {
        super.cleanup();

        //TODO: clean up what you initialized in the initialize method,
        //e.g. remove all spatials from rootNode
        //this is called on the OpenGL thread after the AppState has been detached

        this.rootNode.removeControl(this.worldCommandControl);
        this.worldRootNode.detachAllChildren();
        this.rootNode.detachChild(this.worldRootNode);
    }

    /**
     * adds a control to the list of controls that are added to the spatial
     * currently controlled by the user (chasecam, ui control etc.)
     *
     * @param control
     */
    public void addUserControl(Control control) {
        userControls.add(control);
    }

    public long getMyPlayerId() {
        return myPlayerId;
    }

    public void setMyPlayerId(long myPlayerId) {
        this.myPlayerId = myPlayerId;
    }

    public long getMyGroupId() {
        return myGroupId;
    }

    public void setMyGroupId(long myGroupId) {
        this.myGroupId = myGroupId;
    }

    /**
     * get the NavMesh of the currently loaded level
     *
     * @return
     */
//    public NavMesh getNavMesh() {
//        return navMesh;
//    }
    /**
     * get the world root node (not necessarily the application rootNode!)
     *
     * @return
     */
    public Node getWorldRoot() {
        return this.worldRootNode;
    }

    public PhysicsSpace getPhysicsSpace() {
        return physicsSpace;
    }

    /**
     * loads the specified level node
     *
     * @param name
     */
    public void loadLevel(String name) {
        this.worldRootNode = (Node) assetManager.loadModel(name);
        Spatial mesh = this.worldRootNode.getChild("NavMesh");
    }

    /**
     * detaches the level and clears the cache
     */
    public void closeLevel() {
        for (Iterator<PlayerData> it = PlayerData.getPlayers().iterator(); it.hasNext();) {
            PlayerData playerData = it.next();
            playerData.setData("entity_id", -1l);
        }
        entities.clear();
        newId = 0;
        this.physicsSpace.removeAll(this.worldRootNode);
        this.rootNode.detachChild(this.worldRootNode);
    }
//
//    /**
//     * preloads the models with the given names
//     * @param modelNames
//     */
//    public void preloadModels(String[] modelNames) {
//        for (int i = 0; i < modelNames.length; i++) {
//            String string = modelNames[i];
//            assetManager.loadModel(string);
//        }
//    }

    /**
     * creates the nav mesh for the loaded level
     */
    public void createNavMesh() {
//        Mesh mesh = new Mesh();
        //version a: from mesh
//        GeometryBatchFactory.mergeGeometries(findGeometries(worldRoot, new LinkedList<Geometry>()), mesh);
//        Mesh optiMesh = generator.optimize(mesh);
//
//        navMesh.loadFromMesh(optiMesh);
//
//        //TODO: navmesh only for debug
//        Geometry navGeom = new Geometry("NavMesh");
//        navGeom.setMesh(optiMesh);
//        Material green = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
//        green.setColor("Color", ColorRGBA.Green);
//        green.getAdditionalRenderState().setWireframe(true);
//        navGeom.setMaterial(green);
//
//        worldRoot.attachChild(navGeom);
    }

    /**
     * attaches the level node to the rootnode
     */
    public void attachLevel() {
        this.physicsSpace.addAll(this.worldRootNode);
        this.rootNode.attachChild(this.worldRootNode);
    }

    /**
     * adds a new player with new id (used on server only)
     *
     * @param id
     * @param groupId
     * @param name
     * @param aiId
     */
    public long addNewPlayer(int groupId, String name, int aiId) {
        long playerId = PlayerData.getNew(name);
        addPlayer(playerId, groupId, name, aiId);
        return playerId;
    }

    /**
     * adds a player (sends message if server)
     *
     * @param id
     * @param groupId
     * @param name
     * @param aiId
     */
    public void addPlayer(long id, int groupId, String name, int aiId) {
        Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Adding player: {0}", id);

        PlayerData player = null;
        player = new PlayerData(id, groupId, name, aiId);
        PlayerData.add(id, player);
    }

    /**
     * removes a player
     *
     * @param id
     */
    public void removePlayer(long id) {
        Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Removing player: {0}", id);

        long entityId = PlayerData.getLongData(id, "entity_id");
        if (entityId != -1) {
            enterEntity(id, -1);
        }
        long characterId = PlayerData.getLongData(id, "character_entity_id");
        removeEntity(characterId);

        PlayerData.remove(id);
    }

    /**
     * gets the entity with the specified id
     *
     * @param id
     * @return
     */
    public Spatial getEntity(long id) {
        return entities.get(id);
    }

    /**
     * gets the entity belonging to a PhysicsCollisionObject
     *
     * @param object
     * @return
     */
    public Spatial getEntity(PhysicsCollisionObject object) {
        Object obj = object.getUserObject();
        if (obj instanceof Spatial) {
            Spatial spatial = (Spatial) obj;
            if (entities.containsValue(spatial)) {
                return spatial;
            }
        }
        return null;
    }

    /**
     * finds the entity id of a given spatial if there is one
     *
     * @param entity
     * @return
     */
    public long getEntityId(Spatial entity) {
        for (Iterator<Map.Entry<Long, Spatial>> it = entities.entrySet().iterator(); it.hasNext();) {
            Map.Entry<Long, Spatial> entry = it.next();
            if (entry.getValue() == entity) {
                return entry.getKey();
            }
        }
        return -1;
    }

    /**
     * gets the entity belonging to a PhysicsCollisionObject
     *
     * @param object
     * @return
     */
    public long getEntityId(PhysicsCollisionObject object) {
        Object obj = object.getUserObject();
        if (obj instanceof Spatial) {
            Spatial spatial = (Spatial) obj;
            if (spatial != null) {
                return getEntityId(spatial);
            }
        }
        return -1;
    }

    /**
     * adds a new entity (only used on server)
     *
     * @param modelIdentifier
     * @param location
     * @param rotation
     * @return
     */
    public long addNewEntity(String modelIdentifier, Vector3f location, Quaternion rotation) {
        long id = 0;
        while (entities.containsKey(id)) {
            id++;
        }
        newId++;
        addEntity(newId, modelIdentifier, location, rotation);
        return newId;
    }

    /**
     * add an entity (vehicle, immobile house etc), always related to a spatial
     * with specific userdata like hp, maxhp etc. (sends message if server)
     *
     * @param id
     * @param modelIdentifier
     * @param location
     * @param rotation
     */
    public void addEntity(long id, String modelIdentifier, Vector3f location, Quaternion rotation) {
        Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Adding entity: {0}", id);

        Node entityModel = (Node) assetManager.loadModel(modelIdentifier);
        setEntityTranslation(entityModel, location, rotation);

        //FIX-ME: this shuld be made by entity CommandQueueControl
        if (entityModel.getControl(CharacterControl.class) != null) {
            entityModel.addControl(new CharacterAnimControl());
        }
        
        entityModel.addControl(new CommandControl(this, id));
        
        entityModel.setUserData("player_id", -1);
        entityModel.setUserData("group_id", -1);
        entityModel.setUserData("entity_id", id);
        this.entities.put(id, entityModel);

        this.physicsSpace.addAll(entityModel);
        this.worldRootNode.attachChild(entityModel);
    }

    /**
     * removes the entity with the specified id, exits player if inside (sends
     * message if server)
     *
     * @param id
     */
    public void removeEntity(long id) {
        Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Removing entity: {0}", id);

        Spatial spat = entities.remove(id);
        if (spat == null) {
            Logger.getLogger(this.getClass().getName()).log(Level.WARNING, "try removing entity thats not there: {0}", id);
            return;
        }
        Long playerId = (Long) spat.getUserData("player_id");
        removeTransientControls(spat);
//        removeAIControls(spat);
        if (playerId == myPlayerId) {
            removeUserControls(spat);
        }
        if (playerId != -1) {
            PlayerData.setData(playerId, "entity_id", -1);
        }
        //TODO: removing from aiManager w/o checking if necessary
//        if (!isServer()) {
//            commandInterface.removePlayerEntity(playerId);
//        }
        spat.removeFromParent();
        physicsSpace.removeAll(spat);
    }

    /**
     * disables an entity so that it is not displayed
     *
     * @param id
     */
    public void disableEntity(long id) {
        Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Disabling entity: {0}", id);

        Spatial spat = getEntity(id);
        spat.removeFromParent();
        physicsSpace.removeAll(spat);
    }

    /**
     * reenables an entity after it has been disabled
     *
     * @param id
     * @param location
     * @param rotation
     */
    public void enableEntity(long id, Vector3f location, Quaternion rotation) {
        Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Enabling entity: {0}", id);

        Spatial spat = getEntity(id);
        setEntityTranslation(spat, location, rotation);
        this.worldRootNode.attachChild(spat);
        physicsSpace.addAll(spat);
    }

    /**
     * sets the translation of an entity based on its type
     *
     * @param entityModel
     * @param location
     * @param rotation
     */
    private void setEntityTranslation(Spatial entityModel, Vector3f location, Quaternion rotation) {
        CharacterControl ccCtrl = entityModel.getControl(CharacterControl.class);

        if (ccCtrl != null) {
            ccCtrl.setPhysicsLocation(location);
            ccCtrl.setViewDirection(rotation.mult(Vector3f.UNIT_Z)
                    .multLocal(1, 0, 1).normalizeLocal());
            return;
        }

        RigidBodyControl rbCtrl = entityModel.getControl(RigidBodyControl.class);

        if (rbCtrl != null) {
            rbCtrl.setPhysicsLocation(location);
            rbCtrl.setPhysicsRotation(rotation.toRotationMatrix());
        } else {
            entityModel.setLocalTranslation(location);
            entityModel.setLocalRotation(rotation);
        }
    }

    /**
     * handle player entering entity (sends message if server)
     *
     * @param playerId
     * @param entityId
     */
    public void enterEntity(long playerId, long entityId) {
        Logger.getLogger(this.getClass().getName()).log(Level.INFO,
                "Player {0} entering entity {1}",
                new Object[]{playerId, entityId});

        long curEntity = PlayerData.getLongData(playerId, "entity_id");
        int groupId = PlayerData.getIntData(playerId, "group_id");
        //reset current entity
        if (curEntity != -1) {
            Logger.getLogger(this.getClass().getName()).log(Level.INFO,
                    "Player {0} exiting current entity {1}",
                    new Object[]{playerId, curEntity});
            Spatial curEntitySpat = getEntity(curEntity);
            curEntitySpat.setUserData("player_id", -1l);
            curEntitySpat.setUserData("group_id", -1);
            removeTransientControls(curEntitySpat);
//            removeAIControls(curEntitySpat);
            if (playerId == myPlayerId) {
                removeUserControls(curEntitySpat);
            }
        }
        PlayerData.setData(playerId, "entity_id", entityId);
        //if we entered an entity, configure its controls, id -1 means enter no entity
        if (entityId != -1) {
            Spatial spat = getEntity(entityId);
            spat.setUserData("player_id", playerId);
            spat.setUserData("group_id", groupId);
            if (PlayerData.isHuman(playerId)) {
                makeManualControl(entityId);
                //move controls for local user to new spatial
                if (playerId == getMyPlayerId()) {
                    addUserControls(spat);
                }
            }
        }
    }

    /**
     * makes the specified entity ready to be manually controlled by adding a
     * ManualControl based on the entity type (vehicle etc)
     */
    private void makeManualControl(long entityId) {
        Spatial spat = getEntity(entityId);
        if (spat.getControl(CharacterControl.class) != null) {
            Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Make manual character control for entity {0} ", entityId);

            //add net sending for users own manual control
            //TODO: using group id as client id
            if ((Integer) spat.getUserData("group_id") == myGroupId) {
                spat.addControl(new ManualCharacterControl(entityId));
            } else {
                spat.addControl(new ManualCharacterControl());
            }
        }
    }

    /**
     * makes the specified entity ready to be controlled by an AIControl by
     * adding an AutonomousControl based on entity type.
     */
    private void makeAutoControl(long entityId) {
        Spatial spat = getEntity(entityId);
        if (spat.getControl(CharacterControl.class) != null) {
            Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Make autonomous character control for entity {0} ", entityId);

            spat.addControl(new AutonomousCharacterControl(entityId));

//            else {
//                spat.addControl(new AutonomousCharacterControl());
//            }
        }
    }

    /**
     * removes all movement controls (ManualControl / AutonomousControl) from
     * spatial
     *
     * @param spat
     */
    private void removeTransientControls(Spatial spat) {
        ManualControl manualControl = spat.getControl(ManualControl.class);
        if (manualControl != null) {
            spat.removeControl(manualControl);
        }
        AutonomousControl autoControl = spat.getControl(AutonomousControl.class);
        if (autoControl != null) {
            spat.removeControl(autoControl);
        }
    }

    /**
     * adds the user controls for human user to the spatial
     */
    private void addUserControls(Spatial spat) {
        for (Iterator<Control> it = userControls.iterator(); it.hasNext();) {
            Control control = it.next();
            spat.addControl(control);
        }
    }

    /**
     * removes the user controls for human user to the spatial
     */
    private void removeUserControls(Spatial spat) {
        for (Iterator<Control> it = userControls.iterator(); it.hasNext();) {
            Control control = it.next();
            spat.removeControl(control);
        }
    }

//    /**
//     * adds the command queue and triggers for user controlled ai entities
//     */
//    private void addAIControls(long playerId, long entityId) {
//        //TODO: use stored controls for playerId
//        Spatial spat = getEntity(entityId);
//        spat.addControl(new CommandControl(this, playerId, entityId));
////        Command command = new AttackCommand();
//        SphereTrigger trigger = new SphereTrigger(this);
//        spat.addControl(trigger);
//    }
    /**
     * removes the command queue and triggers for user controlled ai entities
     */
//    private void removeAIControls(Spatial spat) {
//        CommandControl aiControl = spat.getControl(CommandControl.class);
//        if (aiControl != null) {
//            spat.removeControl(aiControl);
//        }
//        TriggerControl triggerControl = spat.getControl(TriggerControl.class);
//        while (triggerControl != null) {
//            spat.removeControl(triggerControl);
//            triggerControl = spat.getControl(TriggerControl.class);
//        }
//    }
    /**
     * set user data of specified entity (sends message if server)
     *
     * @param id
     * @param name
     * @param data
     */
    public void setEntityUserData(long id, String name, Object data) {
        getEntity(id).setUserData(name, data);
    }

//    /**
//     * play animation on specified entity
//     * @param entityId
//     * @param animationName
//     * @param channel
//     */
//    public void playEntityAnimation(long entityId, String animationName, int channel) {
//    }
//
//    public void playWorldEffect(String effectName, Vector3f location, float time) {
//        Quaternion rotation = new Quaternion();
//        playWorldEffect(-1, effectName, location, rotation, location, rotation, time);
//    }
//
//    public void playWorldEffect(String effectName, Vector3f location, Quaternion rotation, float time) {
//        playWorldEffect(-1, effectName, location, rotation, location, rotation, time);
//    }
//
//    public void playWorldEffect(long id, String effectName, Vector3f location, Quaternion rotation, float time) {
//        playWorldEffect(id, effectName, location, rotation, location, rotation, time);
//    }
//
//    public void playWorldEffect(long id, String effectName, Vector3f location, Quaternion rotation, Vector3f endLocation, Quaternion endRotation, float time) {
//        syncManager.broadcast(new ServerEffectMessage(id, effectName, location, rotation, endLocation, endRotation, time));
//    }
    /**
     * does a ray test that starts at the entity location and extends in its
     * view direction by length, stores collision location in supplied
     * storeLocation vector, if collision object is an entity, returns entity
     *
     * @param entity
     * @param length
     * @param storeVector
     * @return
     */
    public Spatial doRayTest(Spatial entity, float length, Vector3f storeLocation) {
        MovementControl control = entity.getControl(MovementControl.class);
        Vector3f startLocation = control.getLocation();
        Vector3f endLocation = startLocation.add(control.getAimDirection().normalize().multLocal(length));
        List<PhysicsRayTestResult> results = getPhysicsSpace().rayTest(startLocation, endLocation);
        Spatial found = null;
        float dist = Float.MAX_VALUE;
        for (Iterator<PhysicsRayTestResult> it = results.iterator(); it.hasNext();) {
            PhysicsRayTestResult physicsRayTestResult = it.next();
            Spatial object = getEntity(physicsRayTestResult.getCollisionObject());
            if (object == entity) {
                continue;
            }
            if (physicsRayTestResult.getHitFraction() < dist) {
                dist = physicsRayTestResult.getHitFraction();
                if (storeLocation != null) {
                    FastMath.interpolateLinear(physicsRayTestResult.getHitFraction(), startLocation, endLocation, storeLocation);
                }
                found = object;
            }
        }
        return found;
    }

    /**
     * does a ray test, stores collision location in supplied storeLocation
     * vector, if collision object is an entity, returns entity
     *
     * @param storeLocation
     * @return
     */
    public Spatial doRayTest(Vector3f startLocation, Vector3f endLocation, Vector3f storeLocation) {
        List<PhysicsRayTestResult> results = getPhysicsSpace().rayTest(startLocation, endLocation);
        //TODO: sorting of results
        Spatial found = null;
        float dist = Float.MAX_VALUE;
        for (Iterator<PhysicsRayTestResult> it = results.iterator(); it.hasNext();) {
            PhysicsRayTestResult physicsRayTestResult = it.next();
            Spatial object = getEntity(physicsRayTestResult.getCollisionObject());
            if (physicsRayTestResult.getHitFraction() < dist) {
                dist = physicsRayTestResult.getHitFraction();
                if (storeLocation != null) {
                    FastMath.interpolateLinear(physicsRayTestResult.getHitFraction(), startLocation, endLocation, storeLocation);
                }
                found = object;
            }
        }
        return found;
    }
}
