/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quanticminds.jme3.commands;

import com.jme3.scene.Spatial;
import com.quanticminds.jme3.World;
import java.util.LinkedList;

/**
 *
 * @author adriano
 */
public abstract class CompositeCommand extends AbstractCommand{
    
    protected LinkedList<Command> commands = new LinkedList<Command>();
    
//    public Command initializeCommand(Command ... command) {
//        return command.initialize(world, entityId, spatial);
//    }
    
    public void addCommand(Command command) {
        for (int i = 0; i < commands.size(); i++) {
            Command command1 = commands.get(i);
            if (command1.getPriority() < command.getPriority()) {
                commands.add(i, command);
                return;
            }
        }
        commands.add(command);
    }
    
    public void removeCommand(Command command) {
        command.setRunning(false);
        commands.remove(command);
    }
    
    @Override
    public State doCommand(float tpf){
        
        return compositeDoCommand(tpf);
    }

    public abstract State compositeDoCommand(float tpf);
    

}
