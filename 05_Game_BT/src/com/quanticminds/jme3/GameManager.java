/**
 * *******************
 *
 * Copyright (c) 2011, 2012 Quantic Minds Software Ltda. All Rights Reserved.
 *
 *********************
 * Game.java
 *
 * Version: 0.1.0
 *
 *
 */
package com.quanticminds.jme3;

import com.jme3.app.Application;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.bullet.control.CharacterControl;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Spatial;
import de.lessvoid.nifty.Nifty;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Manages the actual gameplay on the server side
 *
 * @author normenhansen
 */
public class GameManager extends AbstractAppState {

    private World gameWorld;
    private boolean running;
    private Game gameApp;
    private Nifty hudManager;
//    String mapName;
//    String[] modelNames;

    public GameManager(Game gameApp){
        this.gameApp = gameApp;
        this.gameWorld = gameApp.getStateManager().getState(World.class);
        this.hudManager = this.gameApp.getHudManager();
    }
    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
    }

    /**
     * starts the game
     */
    public synchronized boolean startGame(String map) {
        if (running) {
            return false;
        }
        
        running = true;
//        mapName = map;
        
        //TODO: parse client side string, create preload model list automatically
//        modelNames = new String[]{"Models/HoverTank/HoverTank.j3o", "Models/Sinbad/Sinbad.j3o", "Models/Ferrari/Car.j3o"};//, "Models/Buggy/Buggy.j3o"

        this.gameWorld.loadLevel(map);
//        this.gameWorld.createNavMesh();
//        this.gameWorld.preloadModels(modelNames);
        this.gameWorld.attachLevel();

        //create character entities for all players, then enter the entites
        int i = 0;

        final long newPlayerId = 
                this.gameWorld.addNewPlayer(0, "Player 1", -1);
        for (Iterator<PlayerData> it = PlayerData.getPlayers().iterator(); 
                it.hasNext();) {
            PlayerData playerData = it.next();
            this.gameWorld.setMyPlayerId(newPlayerId);
            long entityId = 
                    this.gameWorld.addNewEntity("Models/Sinbad/Sinbad.mesh.j3o", 
                                                new Vector3f(3, 3, -40), 
                                                new Quaternion());
            playerData.setData("character_entity_id", entityId);
            this.gameWorld.enterEntity(newPlayerId, entityId);

            //create new ai player for user

//            long playearId = this.gameWorld.addNewPlayer(PlayerData.getIntData(playerData.getId(), "group_id"), "AI", 0);
//            long entitayId = this.gameWorld.addNewEntity("Models/Sinbad/Sinbad.j3o", new Vector3f(i * 3, 3, 3), new Quaternion());
//            PlayerData.setData(playearId, "character_entity_id", entitayId);
//            this.gameWorld.enterEntity(playearId, entitayId);

            //create a vehicle
//            this.gameWorld.addNewEntity("Models/Ferrari/Car.j3o", new Vector3f(i * 3, 3, -3), new Quaternion());
            i++;
        }
        return true;
    }

    /**
     * stops the game
     */
    public synchronized boolean stopGame() {
        if (!running) {
            return false;
        }

        this.gameWorld.closeLevel();
        running = false;
        return true;
    }

    /**
     * checks if the game is running
     *
     * @return
     */
    public synchronized boolean isRunning() {
        return running;
    }

    /**
     * called when an entity (human or AI) performs an action
     *
     * @param entityId
     * @param action
     * @param pressed
     */
    public void performAction(long entityId, int action, boolean pressed) {
        Spatial myEntity = this.gameWorld.getEntity(entityId);
        if (myEntity == null) {
            Logger.getLogger(GameManager.class.getName()).log(Level.WARNING, "Cannot find entity performing action!");
            return;
        }
        long player_id = (Long) myEntity.getUserData("player_id");
        if (player_id == -1) {
            Logger.getLogger(GameManager.class.getName()).log(Level.WARNING, "Cannot find player id for entity performing action!");
            return;
        }
        //enter entity
//        if (action == ActionMessage.ENTER_ACTION && pressed) {
//            performEnterEntity(player_id, myEntity);
//        } else if (action == ActionMessage.SHOOT_ACTION && pressed) {
//            performShoot(myEntity);
//        }
    }

    /**
     * handle player performing "enter entity" action
     *
     * @param player_id
     * @param myEntity
     */
    private void performEnterEntity(long player_id, Spatial myEntity) {
//        long characterId = PlayerData.getLongData(player_id, "character_entity_id");
//        long curEntityId = (Long) myEntity.getUserData("entity_id");
//        Spatial entity = worldManager.doRayTest(myEntity, 4, null);
//        if (entity != null && (Long) entity.getUserData("player_id") == -1l) {
//            if (curEntityId == characterId) {
//                worldManager.disableEntity(characterId);
//                worldManager.enterEntity(player_id, (Long) entity.getUserData("entity_id"));
//            } else {
//                worldManager.enterEntity(player_id, characterId);
//                worldManager.enableEntity(characterId, myEntity.getWorldTranslation().add(Vector3f.UNIT_Y), myEntity.getWorldRotation());
//            }
//        } else {
//            if (curEntityId != characterId) {
//                worldManager.enterEntity(player_id, characterId);
//                worldManager.enableEntity(characterId, myEntity.getWorldTranslation().add(Vector3f.UNIT_Y), myEntity.getWorldRotation());
//            }
//        }
    }

    /**
     * handle entity shooting
     *
     * @param myEntity
     */
    private void performShoot(Spatial myEntity) {
        CharacterControl control = myEntity.getControl(CharacterControl.class);
        if (control == null) {
            Logger.getLogger(GameManager.class.getName()).log(Level.WARNING, "Cannot shoot when not character!");
            return;
        }
//        worldManager.playWorldEffect("Effects/GunShotA.j3o", myEntity.getWorldTranslation(), 0.1f);
//        Vector3f hitLocation = new Vector3f();
//        Spatial hitEntity = worldManager.doRayTest(myEntity, 10, hitLocation);
//        if (hitEntity != null) {
//            long targetId = (Long) hitEntity.getUserData("entity_id");
//            Float hp = (Float) hitEntity.getUserData("HitPoints");
//            if (hp != null) {
//                hp -= 10;
//                worldManager.playWorldEffect("Effects/ExplosionA.j3o", hitLocation, 2.0f);
//                worldManager.setEntityUserData(targetId, "HitPoints", hp);
//                if (hp <= 0) {
//                    worldManager.removeEntity(targetId);
//                    worldManager.playWorldEffect("Effects/ExplosionB.j3o", hitEntity.getWorldTranslation(), 2.0f);
//                }
//            }
//        }
    }
}
