/*
 * Game.java
 *
 * Version:  0.1.0
 *
 * Authors:  
 *          Adriano Ribeiro <adriano.ribeiro at quanticmidns.com>
 *
 *********************
 *
 * Copyright (c) 2011, 2012  Quantic Minds Software Ltda.
 * All Rights Reserved.
 *
 *********************
 */

package com.quanticminds.jme3.samples;


import com.jme3.system.AppSettings;
import com.quanticminds.jme3.Constants;
import com.quanticminds.jme3.Game;
import com.quanticminds.jme3.controls.UserInputControl;

/**
 *
 * @author adriano
 */
public class BasicGame extends Game {
    

    public static void main(String[] args) {

        AppSettings settings = new AppSettings(true);
//        settings.setFrameRate(Constants.SCENE_FPS);
//        settings.setSettingsDialogImage("/Interface/Images/splash-small.jpg");
        settings.setTitle(Constants.VERSION);
        

        BasicGame app = new BasicGame();
        app.setShowSettings(false);
        settings.setResolution(1280, 720);
        app.setSettings(settings);
        app.setPauseOnLostFocus(false);
        app.start();
    }
    @Override
    public void simpleInitApp() {
        super.simpleInitApp();
        
        //adding/creating controls later attached to user controlled spatial
        getWorld().addUserControl(new UserInputControl(inputManager, cam));
//        getWorld().addUserControl(commandControl);
//        getWorld().addUserControl(new DefaultHUDControl(nifty.getScreen("default_hud")));
        getGameManager().startGame("Scenes/town/city.j3o");

    }
}
