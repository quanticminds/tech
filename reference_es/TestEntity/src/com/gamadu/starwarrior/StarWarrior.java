package com.gamadu.starwarrior;

import de.lessvoid.nifty.Nifty;
import java.util.Random;


import com.artemis.Entity;
import com.artemis.EntitySystem;
import com.artemis.SystemManager;
import com.artemis.World;
import com.gamadu.starwarrior.components.Health;
import com.gamadu.starwarrior.components.Player;
import com.gamadu.starwarrior.components.SpatialForm;
import com.gamadu.starwarrior.components.Transform;
import com.gamadu.starwarrior.components.Velocity;
import com.gamadu.starwarrior.systems.CollisionSystem;
import com.gamadu.starwarrior.systems.EnemyShipMovementSystem;
import com.gamadu.starwarrior.systems.EnemyShooterSystem;
import com.gamadu.starwarrior.systems.EnemySpawnSystem;
import com.gamadu.starwarrior.systems.ExpirationSystem;
import com.gamadu.starwarrior.systems.HealthBarRenderSystem;
import com.gamadu.starwarrior.systems.HudRenderSystem;
import com.gamadu.starwarrior.systems.MovementSystem;
import com.gamadu.starwarrior.systems.PlayerShipControlSystem;
import com.gamadu.starwarrior.systems.RenderSystem;
import com.jme3.app.SimpleApplication;
import com.jme3.light.AmbientLight;
import com.jme3.light.DirectionalLight;
import com.jme3.light.PointLight;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.niftygui.NiftyJmeDisplay;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.system.AppSettings;
import com.jme3.system.JmeCanvasContext;
import entity.editor.ui.control.EntityWatcherSystem;
import java.awt.Canvas;

public class StarWarrior extends SimpleApplication {

    private World world;
    private EntitySystem renderSystem;
    private EntitySystem hudRenderSystem;
    private EntitySystem controlSystem;
    private EntitySystem movementSystem;
    private EntitySystem enemyShooterSystem;
    private EntitySystem enemyShipMovementSystem;
    private EntitySystem collisionSystem;
    private EntitySystem healthBarRenderSystem;
    private EntitySystem enemySpawnSystem;
    private EntitySystem expirationSystem;
    EntityWatcherSystem entityWatcherSystem;
    private Nifty nifty;

    public void initWorld() {

        world = new World();

        SystemManager systemManager = world.getSystemManager();
        renderSystem = systemManager.setSystem(new RenderSystem(this));
        hudRenderSystem = systemManager.setSystem(new HudRenderSystem(this, nifty));
        movementSystem = systemManager.setSystem(new MovementSystem(this));
        controlSystem = systemManager.setSystem(new PlayerShipControlSystem(this));
        enemyShipMovementSystem = systemManager.setSystem(new EnemyShipMovementSystem(this));
        enemyShooterSystem = systemManager.setSystem(new EnemyShooterSystem());
        collisionSystem = systemManager.setSystem(new CollisionSystem());
        healthBarRenderSystem = systemManager.setSystem(new HealthBarRenderSystem(this, nifty));
        enemySpawnSystem = systemManager.setSystem(new EnemySpawnSystem(500, this));
        expirationSystem = systemManager.setSystem(new ExpirationSystem());
        
        entityWatcherSystem = new EntityWatcherSystem(this);
        entityWatcherSystem = (EntityWatcherSystem) systemManager.setSystem(entityWatcherSystem);
        systemManager.initializeAll();
        loadLevel();
        initPlayerShip();
        initEnemyShips();
        initLight();
        initCamera();

    }

    public void initGUI() {
        NiftyJmeDisplay niftyDisplay = new NiftyJmeDisplay(assetManager,
                inputManager,
                audioRenderer,
                guiViewPort);
        nifty = niftyDisplay.getNifty();


        // attach the nifty display to the gui view port as a processor
        guiViewPort.addProcessor(niftyDisplay);

    }

    void initLight() {
        Vector3f direction = new Vector3f(-0.1F, -0.7F, -1.0F).normalizeLocal();
        DirectionalLight dl = new DirectionalLight();
        dl.setDirection(direction);
        dl.setColor(new ColorRGBA(1.0F, 1.0F, 1.0F, 1.0F));
        this.rootNode.addLight(dl);
        /** A white, spot light source. */
        PointLight lamp = new PointLight();
        lamp.setPosition(Vector3f.ZERO);
        lamp.setColor(ColorRGBA.Orange);
        rootNode.addLight(lamp);
        /** A white ambient light source. */
        AmbientLight ambient = new AmbientLight();
        ambient.setColor(ColorRGBA.Pink);
        rootNode.addLight(ambient);
    }

    void initCamera() {
        flyCam.setMoveSpeed(100f);
        cam.setLocation(new Vector3f(400, 400, 900f).mult(0.1f));
        cam.lookAt(new Vector3f(400f, 0, 560f).mult(0.1f), Vector3f.UNIT_Y);
        //cam.setFrustum(0, 1500, 0, 800, 0, 640);

        flyCam.setDragToRotate(true);
        inputManager.setCursorVisible(true);
    }

    private void initEnemyShips() {
        Random r = new Random();
        for (int i = 0; 10 > i; i++) {
            Entity e = EntityFactory.createEnemyShip(world);

            e.getComponent(Transform.class).setLocation(r.nextInt(this.getCamera().getWidth()), r.nextInt(400) + 50);
            e.getComponent(Velocity.class).setVelocity(0.05f);
            e.getComponent(Velocity.class).setAngle(r.nextBoolean() ? 0 : 180);

            e.refresh();
        }
    }

    private void initPlayerShip() {
        Entity e = world.createEntity();
        e.setGroup("SHIPS");
        e.addComponent(new Transform(this.getCamera().getWidth() / 2, this.getCamera().getHeight() - 40));
        e.addComponent(new SpatialForm("PlayerShip"));
        e.addComponent(new Health(30));
        e.addComponent(new Player());

        e.refresh();
    }

    public Canvas createAndStartCanvas(int width, int height) {
        AppSettings settings = new AppSettings(true);
        settings.setWidth(width);
        settings.setHeight(height);

        setPauseOnLostFocus(true);
        setSettings(settings);
        createCanvas();
        startCanvas(true);

        JmeCanvasContext context = (JmeCanvasContext) getContext();
        Canvas canvas = context.getCanvas();
        canvas.setSize(settings.getWidth(), settings.getHeight());

        return canvas;
    }

    public void initInput() {
    }

    public void updateWorld(int delta) {
        world.loopStart();
        //System.out.println(" Delta " + delta);
        world.setDelta(delta);

        controlSystem.process();
        movementSystem.process();
        enemyShooterSystem.process();
        enemyShipMovementSystem.process();
        collisionSystem.process();
        enemySpawnSystem.process();
        expirationSystem.process();

        renderSystem.process();
        healthBarRenderSystem.process();
        hudRenderSystem.process();
        entityWatcherSystem.process();
    }

    public static void main(String[] args) {
        StarWarrior game = new StarWarrior();
        game.start();
    }

    public void simpleInitApp() {
        initWorld();
    }

    public void simpleUpdate(float tpf) {
        super.simpleUpdate(tpf);
        updateWorld(Math.round(tpf * 500));
        //System.out.println(" Cam :" + cam.getLocation());
    }

    void loadLevel() {
        Spatial model = assetManager.loadModel("Models/naves.j3o");
        Spatial sky = ((Node) model).getChild("Sky");
        //sky.setQueueBucket(Bucket.Sky);
        rootNode.attachChild(sky);

    }

    public EntityWatcherSystem getEntityWatcherSystem() {
        return entityWatcherSystem;
    }
}
