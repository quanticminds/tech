package com.gamadu.starwarrior.systems;

import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.EntityProcessingSystem;
import com.artemis.utils.Bag;

import com.gamadu.starwarrior.components.SpatialForm;
import com.gamadu.starwarrior.components.Transform;
import com.gamadu.starwarrior.spatials.EnemyShip;
import com.gamadu.starwarrior.spatials.Explosion;
import com.gamadu.starwarrior.spatials.Missile;
import com.gamadu.starwarrior.spatials.PlayerShip;
import com.gamadu.starwarrior.spatials.AbstractEntitySpatial;
import com.jme3.app.SimpleApplication;

public class RenderSystem extends EntityProcessingSystem {

    private Bag<AbstractEntitySpatial> spatials;
    private ComponentMapper<SpatialForm> spatialFormMapper;
    private ComponentMapper<Transform> transformMapper;
    private SimpleApplication container;

    public RenderSystem(SimpleApplication container) {
        super(Transform.class, SpatialForm.class);
        this.container = container;

        spatials = new Bag<AbstractEntitySpatial>();
    }

    @Override
    public void initialize() {
        spatialFormMapper = new ComponentMapper<SpatialForm>(SpatialForm.class, world);
        transformMapper = new ComponentMapper<Transform>(Transform.class, world);
    }

    @Override
    protected void process(Entity e) {
        AbstractEntitySpatial spatial = spatials.get(e.getId());
        Transform transform = transformMapper.get(e);

        if (transform.getX() >= 0 && transform.getY() >= 0 && transform.getX() < container.getCamera().getWidth() && transform.getY() < container.getCamera().getHeight() && spatial != null) {
            spatial.render();
        } else {
            //spatial.remove(container);
        }
    }

    @Override
    protected void added(Entity e) {
        AbstractEntitySpatial spatial = createSpatial(e);
        if (spatial != null) {
            spatial.initalize(container);
            spatials.set(e.getId(), spatial);
        }
    }

    @Override
    protected void removed(Entity e) {
        AbstractEntitySpatial spatial = spatials.get(e.getId());
        if (spatial != null) {
            spatial.remove(container);
            spatials.set(e.getId(), null);
        }
    }

    private AbstractEntitySpatial createSpatial(Entity e) {
        SpatialForm spatialForm = spatialFormMapper.get(e);
        String spatialFormFile = spatialForm.getSpatialFormFile();

        if ("PlayerShip".equalsIgnoreCase(spatialFormFile)) {
            return new PlayerShip(world, e);
        } else if ("Missile".equalsIgnoreCase(spatialFormFile)) {
            return new Missile(world, e);
        } else if ("EnemyShip".equalsIgnoreCase(spatialFormFile)) {
            return new EnemyShip(world, e);
        } else if ("BulletExplosion".equalsIgnoreCase(spatialFormFile)) {
            return new Explosion(world, e, 10);
        } else if ("ShipExplosion".equalsIgnoreCase(spatialFormFile)) {
            return new Explosion(world, e, 30);
        }

        return null;
    }
}
