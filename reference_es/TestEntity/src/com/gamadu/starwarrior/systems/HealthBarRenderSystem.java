package com.gamadu.starwarrior.systems;

import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.EntityProcessingSystem;

import com.gamadu.starwarrior.components.Health;
import com.gamadu.starwarrior.components.Transform;
import com.jme3.app.SimpleApplication;
import de.lessvoid.nifty.Nifty;

public class HealthBarRenderSystem extends EntityProcessingSystem {

    private SimpleApplication container;
    private ComponentMapper<Health> healthMapper;
    private ComponentMapper<Transform> transformMapper;
    Nifty nifty;

    public HealthBarRenderSystem(SimpleApplication container, Nifty nifty) {
        super(Health.class, Transform.class);
        this.container = container;
        this.nifty = nifty;
    }

    @Override
    public void initialize() {
        healthMapper = new ComponentMapper<Health>(Health.class, world);
        transformMapper = new ComponentMapper<Transform>(Transform.class, world);
    }

    @Override
    protected void process(Entity e) {
        Health health = healthMapper.get(e);
        Transform transform = transformMapper.get(e);

        //g.drawString(health.getHealthPercentage() + "%", transform.getX()-10, transform.getY()-30);
    }
}
