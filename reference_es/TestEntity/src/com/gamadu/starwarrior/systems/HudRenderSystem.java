package com.gamadu.starwarrior.systems;


import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.EntityProcessingSystem;

import com.gamadu.starwarrior.components.Health;
import com.gamadu.starwarrior.components.Player;
import com.jme3.app.SimpleApplication;
import de.lessvoid.nifty.Nifty;

public class HudRenderSystem extends EntityProcessingSystem {

    private SimpleApplication container;
    private Nifty nifty;
    private ComponentMapper<Health> healthMapper;

    public HudRenderSystem(SimpleApplication container,Nifty nifty) {
        super(Health.class, Player.class);
        this.container = container;
        this.nifty = nifty;
    }

    @Override
    public void initialize() {
        healthMapper = new ComponentMapper<Health>(Health.class, world);
        // finf the ui
    }

    @Override
    protected void process(Entity e) {
        Health health = healthMapper.get(e);

        // set the health
        
    }
}
