package com.gamadu.starwarrior.systems;

import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.EntityProcessingSystem;
import com.artemis.utils.TrigLUT;

import com.gamadu.starwarrior.components.Transform;
import com.gamadu.starwarrior.components.Velocity;
import com.jme3.app.SimpleApplication;

public class MovementSystem extends EntityProcessingSystem {

    private SimpleApplication container;
    private ComponentMapper<Velocity> velocityMapper;
    private ComponentMapper<Transform> transformMapper;

    public MovementSystem(SimpleApplication container) {
        super(Transform.class, Velocity.class);
        this.container = container;
    }

    @Override
    public void initialize() {
        velocityMapper = new ComponentMapper<Velocity>(Velocity.class, world);
        transformMapper = new ComponentMapper<Transform>(Transform.class, world);
    }

    @Override
    protected void process(Entity e) {
        Velocity velocity = velocityMapper.get(e);
        float v = velocity.getVelocity();

        Transform transform = transformMapper.get(e);

        float r = velocity.getAngleAsRadians();

        float xn = transform.getX() + (TrigLUT.cos(r) * v * world.getDelta());
        float yn = transform.getY() + (TrigLUT.sin(r) * v * world.getDelta());

        transform.setLocation(xn, yn);
    }
}
