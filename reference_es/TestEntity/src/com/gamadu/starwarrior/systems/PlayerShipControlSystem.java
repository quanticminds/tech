package com.gamadu.starwarrior.systems;

import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.EntityProcessingSystem;
import com.gamadu.starwarrior.EntityFactory;

import com.gamadu.starwarrior.components.Player;
import com.gamadu.starwarrior.components.Transform;
import com.gamadu.starwarrior.components.Velocity;
import com.jme3.app.SimpleApplication;
import com.jme3.input.InputManager;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;

public class PlayerShipControlSystem extends EntityProcessingSystem implements ActionListener {

    private SimpleApplication container;
    private boolean moveRight;
    private boolean moveLeft;
    private boolean shoot;
    private ComponentMapper<Transform> transformMapper;

    public PlayerShipControlSystem(SimpleApplication container) {
        super(Transform.class, Player.class);
        this.container = container;
    }

    @Override
    public void initialize() {
        transformMapper = new ComponentMapper<Transform>(Transform.class, world);
        InputManager inputManager = container.getInputManager();
        // Test multiple inputs per mapping
        inputManager.addMapping("KEY_H", new KeyTrigger(KeyInput.KEY_H));
        inputManager.addMapping("KEY_K", new KeyTrigger(KeyInput.KEY_K));
        inputManager.addMapping("KEY_SPACE", new KeyTrigger(KeyInput.KEY_SPACE));
        // Test multiple listeners per mapping
        inputManager.addListener(this, "KEY_H", "KEY_K", "KEY_SPACE");
    }

    @Override
    protected void process(Entity e) {
        Transform transform = transformMapper.get(e);

        if (moveLeft) {
            transform.addX(world.getDelta() * -0.3f);
            System.out.println("moveLeft " + transform.getX() + " " + transform.getY());
        }
        if (moveRight) {
            transform.addX(world.getDelta() * 0.3f);
            System.out.println("moveRight " + transform.getX() + " " + transform.getY());
        }

        if (shoot) {
            Entity missile = EntityFactory.createMissile(world);
            missile.getComponent(Transform.class).setLocation(transform.getX(), transform.getY() - 20);
            missile.getComponent(Velocity.class).setVelocity(-0.5f);
            missile.getComponent(Velocity.class).setAngle(90);
            missile.refresh();

            shoot = false;
        }
    }

    public void onAction(String name, boolean isPressed, float tpf) {
        if (isPressed) {
            if (name.equals("KEY_H")) {
                moveLeft = true;
                moveRight = false;
            } else if (name.equals("KEY_K")) {
                moveRight = true;
                moveLeft = false;
            } else if (name.equals("KEY_SPACE")) {
                shoot = true;
            }
        } else {
            if (name.equals("KEY_H")) {
                moveLeft = false;
            } else if (name.equals("KEY_K")) {
                moveRight = false;
            } else if (name.equals("KEY_SPACE")) {
                shoot = false;
            }
        }
    }
}
