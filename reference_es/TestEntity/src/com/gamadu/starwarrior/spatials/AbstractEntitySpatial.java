package com.gamadu.starwarrior.spatials;

import com.artemis.Entity;
import com.artemis.World;
import com.jme3.app.SimpleApplication;

public abstract class AbstractEntitySpatial {

    protected World world;
    protected Entity owner;
    float zoomFactor = 0.15f;

    public AbstractEntitySpatial(World world, Entity owner) {
        this.world = world;
        this.owner = owner;
    }

    public abstract void initalize(SimpleApplication app);

    public abstract void render();

    public abstract void remove(SimpleApplication container);
}
