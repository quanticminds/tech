package com.gamadu.starwarrior.spatials;

import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.World;
import com.gamadu.starwarrior.components.Expires;
import com.gamadu.starwarrior.components.Transform;
import com.jme3.app.SimpleApplication;
import com.jme3.effect.ParticleEmitter;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;

public class Explosion extends AbstractEntitySpatial {

    private Transform transform;
    private Expires expires;
    private int initialLifeTime;
    private ColorRGBA color;
    private int radius;
    Node rootNode;
    Node ex;
    private ParticleEmitter particles;

    public Explosion(World world, Entity owner, int radius) {
        super(world, owner);
        this.radius = radius;
    }

    @Override
    public void initalize(SimpleApplication app) {
        ComponentMapper<Transform> transformMapper = new ComponentMapper<Transform>(Transform.class, world);
        transform = transformMapper.get(owner);

        ComponentMapper<Expires> expiresMapper = new ComponentMapper<Expires>(Expires.class, world);
        expires = expiresMapper.get(owner);
        initialLifeTime = expires.getLifeTime();

        color = new ColorRGBA(ColorRGBA.Yellow);

        Spatial model = app.getAssetManager().loadModel("Models/naves.j3o");
        ex = (Node) ((Node) model).getChild("ExplosionAll");
        this.rootNode = app.getRootNode();
        particles = (ParticleEmitter) ex.getChild("Explosion");
        
        //Quaternion rot = new Quaternion();
        //ex.setLocalRotation(rot.fromAxes(Vector3f.UNIT_X.negate(), Vector3f.UNIT_Z.negate(), Vector3f.UNIT_Y.negate()));
        //mi.setLocalScale(0.01f);
        rootNode.attachChild(ex);
    }

    @Override
    public void remove(SimpleApplication container) {
        if (ex != null) {
            System.out.println("Remove explosion !");
            particles.killAllParticles();
            ex.removeFromParent();
            ex = null;
        }
    }

    public void render() {

        //color.a = (float) expires.getLifeTime() / (float) initialLifeTime;
        if (ex != null) {
            ex.setLocalTranslation(transform.getX() * zoomFactor, 0, transform.getY() * zoomFactor);
        }
    }
}
