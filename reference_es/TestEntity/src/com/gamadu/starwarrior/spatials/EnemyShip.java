package com.gamadu.starwarrior.spatials;

import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.World;
import com.artemis.utils.FastMath;
import com.gamadu.starwarrior.components.Transform;
import com.jme3.app.SimpleApplication;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;

public class EnemyShip extends AbstractEntitySpatial {

    private Transform transform;
    private Spatial ship;
    Node rootNode;

    public EnemyShip(World world, Entity owner) {
        super(world, owner);
    }

    @Override
    public void initalize(SimpleApplication app) {
        ComponentMapper<Transform> transformMapper = new ComponentMapper<Transform>(Transform.class, world);
        transform = transformMapper.get(owner);

        Spatial model = app.getAssetManager().loadModel("Models/naves.j3o");
        ship = ((Node) model).getChild("wsp");
        this.rootNode = app.getRootNode();
        //ship.setLocalScale(0.1f);
        rootNode.attachChild(ship);
        Quaternion rot = new Quaternion();
        ship.setLocalRotation(rot.fromAxes(Vector3f.UNIT_X.negate(), Vector3f.UNIT_Z.negate(), Vector3f.UNIT_Y.negate()));
    }

    public void render() {
        if (ship != null) {
            ship.setLocalTranslation(transform.getX() * zoomFactor, 0, transform.getY() * zoomFactor);
        }

    }

    @Override
    public void remove(SimpleApplication container) {
        if (ship != null) {
            ship.removeFromParent();
        }
    }
}
