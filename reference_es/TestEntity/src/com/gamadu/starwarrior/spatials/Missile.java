package com.gamadu.starwarrior.spatials;

import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.World;
import com.gamadu.starwarrior.components.Transform;
import com.jme3.app.SimpleApplication;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;

public class Missile extends AbstractEntitySpatial {

    private Transform transform;
    Node rootNode;
    private Spatial mi;

    public Missile(World world, Entity owner) {
        super(world, owner);
    }

    @Override
    public void initalize(SimpleApplication app) {
        ComponentMapper<Transform> transformMapper = new ComponentMapper<Transform>(Transform.class, world);
        transform = transformMapper.get(owner);
        //mi = app.getAssetManager().loadModel("Models/Missle.j3o");
        /*
        Box box = new Box(0.1f, 0.1f, 0.1f);
        mi = new Geometry("missile", box);
        Material solid = app.getAssetManager().loadMaterial("")
         * 
         */
        Spatial model = app.getAssetManager().loadModel("Models/naves.j3o");
        mi = ((Node) model).getChild("missil");
        this.rootNode = app.getRootNode();
        Quaternion rot = new Quaternion();
        mi.setLocalRotation(rot.fromAxes(Vector3f.UNIT_X.negate(),Vector3f.UNIT_Z.negate(),Vector3f.UNIT_Y.negate()));
        //mi.setLocalScale(0.01f);
        rootNode.attachChild(mi);
    }

    @Override
    public void remove(SimpleApplication container) {
        if (mi != null) {
            mi.removeFromParent();
            System.out.println("Remove a missle!");
            mi=null;
        }
    }

    @Override
    public void render() {
        if (mi!=null){
            mi.setLocalTranslation(transform.getX()*zoomFactor, 0, transform.getY()*zoomFactor);
        }
    }
}
