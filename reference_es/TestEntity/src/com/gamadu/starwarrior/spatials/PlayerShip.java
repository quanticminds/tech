package com.gamadu.starwarrior.spatials;

import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.World;
import com.gamadu.starwarrior.components.Transform;
import com.jme3.app.SimpleApplication;
import com.jme3.light.PointLight;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;

public class PlayerShip extends AbstractEntitySpatial {

    private Transform transform;
    Node rootNode;
    private Spatial ship;

    public PlayerShip(World world, Entity owner) {
        super(world, owner);
    }

    @Override
    public void initalize(SimpleApplication app) {
        ComponentMapper<Transform> transformMapper = new ComponentMapper<Transform>(Transform.class, world);
        transform = transformMapper.get(owner);

        Spatial model = app.getAssetManager().loadModel("Models/naves.j3o");
        ship = ((Node) model).getChild("kst");
        this.rootNode = app.getRootNode();
        //ship.setLocalScale(0.1f);
                /** A white, spot light source. */
        PointLight lamp = new PointLight();
        lamp.setPosition(Vector3f.ZERO);
        lamp.setColor(ColorRGBA.Pink);
        ship.addLight(lamp);
        rootNode.attachChild(ship);
    }

    @Override
    public void render() {

        ship.setLocalTranslation(transform.getX()*zoomFactor, 0, transform.getY()*zoomFactor);
        //System.out.println(" " + transform.getX() + " " + transform.getY());
    }

    @Override
    public void remove(SimpleApplication container) {
        if (ship != null) {
            ship.removeFromParent();
        }
    }
}