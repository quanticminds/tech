/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity.editor.es;

import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.gamadu.starwarrior.components.SpatialForm;
import entity.editor.ui.control.EntityWatcherSystem;

/**
 *
 * @author hungcuong
 */
public class EntityInfoNode {

    int name;
    Entity owner;
    String type;
    private final ComponentMapper<SpatialForm> spatialFormMapper;
    private final EntityWatcherSystem entityWatcherSystem;

    public EntityInfoNode(Entity e, EntityWatcherSystem entityWatcherSystem) {
        this.name = e.getId();
        this.owner = e;
        this.entityWatcherSystem = entityWatcherSystem;
        this.spatialFormMapper = entityWatcherSystem.getSpatialFormMapper();
        SpatialForm sp = spatialFormMapper.get(e);
        this.type = sp.getSpatialFormFile();
    }

    @Override
    public String toString() {
        return this.name + " (" + this.type + ")";
    }

    public int getName() {
        return name;
    }

    public void setName(int name) {
        this.name = name;
    }

    public Entity getOwner() {
        return owner;
    }

    public void setOwner(Entity owner) {
        this.owner = owner;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
