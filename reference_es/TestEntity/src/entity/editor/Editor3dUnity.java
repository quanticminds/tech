package entity.editor;

import com.gamadu.starwarrior.StarWarrior;
import entity.editor.ui.HierarchyTreeComponent;
import entity.editor.ui.InspectorComponent;
import entity.editor.ui.LogWindow;
import entity.editor.ui.MonitorPanel;
import entity.editor.ui.RuntimeMemoryMonitorSource;
import info.clearthought.layout.TableLayout;
import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.net.URL;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import org.noos.xing.mydoggy.*;
import org.noos.xing.mydoggy.event.ContentManagerUIEvent;
import org.noos.xing.mydoggy.plaf.MyDoggyToolWindowManager;
import org.noos.xing.mydoggy.plaf.ui.content.MyDoggyMultiSplitContentManagerUI;

public class Editor3dUnity {

    StarWarrior app3d;
    private JFrame frame;
    private ToolWindowManager toolWindowManager;
    private JPanel dockingFrame;
    Logger logger = Logger.getLogger(Editor3dUnity.class.getName());
    InspectorComponent inspector;
    private HierarchyTreeComponent hierarchyTree;

    protected void run() {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                setUp();
                start();
            }
        });
    }

    protected void start() {
        logger.info("Set frame visible");
        frame.setVisible(true);
    }

    void setupLAF() {
        try {
            UIManager.setLookAndFeel("org.pushingpixels.substance.api.skin.SubstanceGraphiteLookAndFeel");
            logger.info("Look and feel");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Substance Graphite failed to initialize");
        }
    }

    protected void setUp() {
        // Model

        // Controller

        // View

        initComponents();
        //createLayout();
        initToolWindowManager();
        initComponentsLayout();

    }

    protected void initComponentsLayout() {
        // Activate "Debug" Tool
        ToolWindow debugTool = toolWindowManager.getToolWindow("Debug");
        //debugTool.setActive(true);

        // Aggregate "Run" tool
        ToolWindow runTool = toolWindowManager.getToolWindow("3DView");
        runTool.aggregate(AggregationPosition.TOP);

        // Aggregate "Properties" tool
        ToolWindow propertiesTool = toolWindowManager.getToolWindow("Properties");
        propertiesTool.aggregate(AggregationPosition.RIGHT);

        // Aggregate "Hierarchy" tool
        ToolWindow hierarchyTool = toolWindowManager.getToolWindow("Hierarchy");
        hierarchyTool.aggregate(AggregationPosition.BOTTOM);

        // Aggregate "Asset" tool
        ToolWindow assetTool = toolWindowManager.getToolWindow("Asset");
        assetTool.aggregate(AggregationPosition.LEFT);

    }

    protected void addButtons(JToolBar toolBar) {
        JButton button = null;

        //first button
        button = makeNavigationButton("Play", "Play",
                "Play",
                "Play");
        toolBar.add(button);

        //second button
        button = makeNavigationButton("Stop", "Stop",
                "Stop",
                "Stop");
        toolBar.add(button);

    }

    ImageIcon getResize(ImageIcon icon, int width, int height) {
        Image img = icon.getImage();
        Image newimg = img.getScaledInstance(width, height, java.awt.Image.SCALE_SMOOTH);
        ImageIcon newIcon = new ImageIcon(newimg);
        return newIcon;
    }

    protected JButton makeNavigationButton(String imageName,
            String actionCommand,
            String toolTipText,
            String altText) {
        //Look for the image.
        String imgLocation = "ui/images/"
                + imageName
                + ".png";
        URL imageURL = Editor3dUnity.class.getResource(imgLocation);

        //Create and initialize the button.
        JButton button = new JButton();
        button.setActionCommand(actionCommand);
        button.setToolTipText(toolTipText);
        //button.addActionListener(this);

        if (imageURL != null) {                      //image found
            ImageIcon icon = new ImageIcon(imageURL, altText);

            button.setIcon(getResize(icon, 30, 30));
        } else {                                     //no image found
            button.setText(altText);
            System.err.println("Resource not found: " + imgLocation);
        }

        return button;
    }

    protected void initComponents() {
        // Init the frame
        this.frame = new JFrame("3D Unity clone...");
        this.frame.setSize(1024, 768);
        this.frame.setLocation(100, 100);
        this.frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        // Create a simple JMenuBar
        /*
         JMenuBar menuBar = new JMenuBar();
         JMenu fileMenu = new JMenu("File");
         JMenuItem exitMenuItem = new JMenuItem("Exit");
         exitMenuItem.addActionListener(new ActionListener() {
        
         public void actionPerformed(ActionEvent e) {
         frame.setVisible(false);
         frame.dispose();
         }
         });
         fileMenu.add(exitMenuItem);
         menuBar.add(fileMenu);
         */
        // Menu
        entity.editor.ui.GroovyMenu menu = new entity.editor.ui.GroovyMenu(frame);
        JMenuBar menuBar = menu.createMenuBar();
        this.frame.setJMenuBar(menuBar);


        JToolBar toolBar = new JToolBar("Still draggable");
        addButtons(toolBar);


        // Docking panel
        JPanel mainFramePanel = new JPanel();
        dockingFrame = new JPanel();
        mainFramePanel.setLayout(new BorderLayout());
        mainFramePanel.add(toolBar, BorderLayout.NORTH);
        dockingFrame.setLayout(new TableLayout(new double[][]{{0, -1, 0}, {0, -1, 0}}));
        mainFramePanel.add(dockingFrame, BorderLayout.CENTER);

        // Set a layout manager. I love TableLayout. It's powerful.
        this.frame.getContentPane().add(mainFramePanel);
        this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


    }

    protected void initToolWindowManager() {
        // Create a new instance of MyDoggyToolWindowManager passing the frame.
        MyDoggyToolWindowManager myDoggyToolWindowManager = new MyDoggyToolWindowManager();
        this.toolWindowManager = myDoggyToolWindowManager;

        // Register a Tool.
        toolWindowManager.registerToolWindow("Debug", // Id
                "Debug Tool", // Title
                null, // Icon
                new MonitorPanel(new RuntimeMemoryMonitorSource()), // Component
                ToolWindowAnchor.BOTTOM);       // Anchor

        //setupDebugTool();

        // Register another Tool.
        toolWindowManager.registerToolWindow("3DView", // Id
                "3DView", // Title
                null, // Icon
                create3DCanvas(), // Component
                ToolWindowAnchor.TOP);     // Anchor

        // Register another Tool.
        toolWindowManager.registerToolWindow("Properties", // Id
                "Properties Tool", // Title
                null, // Icon
                createEntityInspector(), // Component
                ToolWindowAnchor.RIGHT);
        // Anchor

        // Register another Tool.
        toolWindowManager.registerToolWindow("Hierarchy", // Id
                "Hierarchy Tool", // Title
                null, // Icon
                createHierarchyTree(), // Component
                ToolWindowAnchor.LEFT);            // Anchor
        // Register another Tool.
        toolWindowManager.registerToolWindow("Asset", // Id
                "Asset Tool", // Title
                null, // Icon
                getAssetTree(), // Component
                ToolWindowAnchor.LEFT);            // Anchor
        // Made all tools available
        for (ToolWindow window : toolWindowManager.getToolWindows()) {
            window.setAvailable(true);
        }

        initContentManager();

        // Add myDoggyToolWindowManager to the frame. MyDoggyToolWindowManager is an extension of a JPanel
        this.dockingFrame.add(myDoggyToolWindowManager, "1,1,");
    }

    Canvas create3DCanvas() {
        app3d = new StarWarrior();
        
        return app3d.createAndStartCanvas(640, 480);
    }

    JScrollPane getAssetTree() {
        DefaultMutableTreeNode top =
                new DefaultMutableTreeNode("Asset");
        //createNodes(top);
        JTree tree = new JTree(top);

        JScrollPane treeView = new JScrollPane(tree);
        return treeView;
    }

    JComponent createEntityInspector() {
        this.inspector = new InspectorComponent();
        return inspector;

    }

    protected void setupDebugTool() {
        ToolWindow debugTool = toolWindowManager.getToolWindow("Debug");

        // RepresentativeAnchorDescriptor
        RepresentativeAnchorDescriptor representativeAnchorDescriptor = debugTool.getRepresentativeAnchorDescriptor();
        representativeAnchorDescriptor.setPreviewEnabled(true);
        representativeAnchorDescriptor.setPreviewDelay(1500);
        representativeAnchorDescriptor.setPreviewTransparentRatio(0.4f);

        // DockedTypeDescriptor
        DockedTypeDescriptor dockedTypeDescriptor = (DockedTypeDescriptor) debugTool.getTypeDescriptor(ToolWindowType.DOCKED);
        dockedTypeDescriptor.setDockLength(300);
        dockedTypeDescriptor.setPopupMenuEnabled(true);
        JMenu toolsMenu = dockedTypeDescriptor.getToolsMenu();
        toolsMenu.add(new AbstractAction("Hello World!!!") {
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(frame, "Hello World!!!");
            }
        });
        dockedTypeDescriptor.setToolWindowActionHandler(new ToolWindowActionHandler() {
            public void onHideButtonClick(ToolWindow toolWindow) {
                JOptionPane.showMessageDialog(frame, "Hiding...");
                toolWindow.setVisible(false);
            }
        });

        dockedTypeDescriptor.setAnimating(true);

        // SlidingTypeDescriptor
        SlidingTypeDescriptor slidingTypeDescriptor = (SlidingTypeDescriptor) debugTool.getTypeDescriptor(ToolWindowType.SLIDING);
        slidingTypeDescriptor.setEnabled(false);
        slidingTypeDescriptor.setTransparentMode(true);
        slidingTypeDescriptor.setTransparentRatio(0.8f);
        slidingTypeDescriptor.setTransparentDelay(0);
        slidingTypeDescriptor.setAnimating(true);

        // FloatingTypeDescriptor
        FloatingTypeDescriptor floatingTypeDescriptor = (FloatingTypeDescriptor) debugTool.getTypeDescriptor(ToolWindowType.FLOATING);
        floatingTypeDescriptor.setEnabled(true);
        floatingTypeDescriptor.setLocation(150, 200);
        floatingTypeDescriptor.setSize(320, 200);
        floatingTypeDescriptor.setModal(false);
        floatingTypeDescriptor.setTransparentMode(true);
        floatingTypeDescriptor.setTransparentRatio(0.2f);
        floatingTypeDescriptor.setTransparentDelay(1000);
        floatingTypeDescriptor.setAnimating(true);

        // Setup Tabs
        //initTabs();
    }

    protected void initDebugTabs() {
        ToolWindow debugTool = toolWindowManager.getToolWindow("Debug");
        ToolWindowTab profilingTab = debugTool.addToolWindowTab("Profiling", new LogWindow(this.getClass().getName()));
        profilingTab.setCloseable(true);
    }

    protected void initContentManager() {
        JTree treeContent = new JTree();

        ContentManager contentManager = toolWindowManager.getContentManager();
        Content content = contentManager.addContent("Tree Key",
                "Tree Title",
                null, // An icon
                treeContent);
        content.setToolTipText("Tree tip");

        setupContentManagerUI();

        contentManager.setEnabled(false);

    }

    protected void setupContentManagerUI() {
        ContentManager contentManager = toolWindowManager.getContentManager();
        MultiSplitContentManagerUI contentManagerUI = new MyDoggyMultiSplitContentManagerUI();
        contentManager.setContentManagerUI(contentManagerUI);

        contentManagerUI.setShowAlwaysTab(true);
        contentManagerUI.setTabPlacement(TabbedContentManagerUI.TabPlacement.BOTTOM);
        contentManagerUI.addContentManagerUIListener(new ContentManagerUIListener() {
            public boolean contentUIRemoving(ContentManagerUIEvent event) {
                return JOptionPane.showConfirmDialog(frame, "Are you sure?") == JOptionPane.OK_OPTION;
            }

            public void contentUIDetached(ContentManagerUIEvent event) {
                JOptionPane.showMessageDialog(frame, "Hello World!!!");
            }
        });

        TabbedContentUI contentUI = contentManagerUI.getContentUI(toolWindowManager.getContentManager().getContent(0));

        contentUI.setCloseable(true);
        contentUI.setDetachable(true);
        contentUI.setTransparentMode(true);
        contentUI.setTransparentRatio(0.7f);
        contentUI.setTransparentDelay(1000);

        // Now Register two other contents...
        contentManager.addContent("Tree Key 2", "Tree Title 2", null, new JTree(), null,
                new MultiSplitConstraint(contentManager.getContent(0), 0));

        contentManager.addContent("Tree Key 3", "Tree Title 3", null, new JTree(), null,
                new MultiSplitConstraint(AggregationPosition.RIGHT));
    }

    private JPanel createHierarchyTree() {
        hierarchyTree = new HierarchyTreeComponent(app3d, inspector);
        hierarchyTree.setupComponents();

        return hierarchyTree;
    }
    
    public static void main(String[] args) {
        Editor3dUnity test = new Editor3dUnity();
        try {
            test.run();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
