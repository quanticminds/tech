/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity.editor;

import com.jme3.app.SimpleApplication;
import com.jme3.math.Vector3f;
import com.jme3.system.AppSettings;

import com.jme3.system.JmeCanvasContext;
import java.awt.Canvas;
import java.util.ArrayList;

/**
 *
 * @author cuong.nguyenmanh2
 */
public class JME3DCanvas extends SimpleApplication {

    public static void main(String[] args) {
        JME3DCanvas app = new JME3DCanvas();
        app.start();
    }

    public Canvas createAndStartCanvas(int width, int height) {
        AppSettings settings = new AppSettings(true);
        settings.setWidth(width);
        settings.setHeight(height);
        setPauseOnLostFocus(false);
        //setPauseOnLostFocus(true);
        setSettings(settings);
        createCanvas();
        startCanvas(true);

        JmeCanvasContext context = (JmeCanvasContext) getContext();
        Canvas canvas = context.getCanvas();
        canvas.setSize(settings.getWidth(), settings.getHeight());

        return canvas;
    }

    public void initInput() {
        flyCam.setDragToRotate(true);
        inputManager.setCursorVisible(true);
        // Test multiple inputs per mapping
        

        // Test multiple listeners per mapping
        //inputManager.addListener(actionListener, "changeTexture");
    }

    @Override
    public void simpleInitApp() {
        flyCam.setMoveSpeed(40f);
        initInput();
    }
}
