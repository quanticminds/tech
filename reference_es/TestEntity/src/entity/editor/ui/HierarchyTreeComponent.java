/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity.editor.ui;

import com.artemis.Entity;
import com.gamadu.starwarrior.StarWarrior;
import entity.editor.es.EntityInfoNode;
import entity.editor.ui.control.EntityWatcherSystem;
import java.awt.BorderLayout;
import java.util.concurrent.Callable;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeSelectionModel;

/**
 *
 * @author hungcuong
 */
public class HierarchyTreeComponent extends JPanel implements TreeSelectionListener {

    JTree tree = new JTree();
    private InspectorComponent inspector;
    private EntityWatcherSystem entityWatcherSystem;
    private StarWarrior app3d;

    public HierarchyTreeComponent(final StarWarrior app3d, InspectorComponent inspector) {
        //super();
        this.inspector = inspector;
        this.app3d = app3d;
    }

    public void setupComponents() {

        app3d.enqueue(new Callable<Object>() {
            public Object call() throws Exception {
                entityWatcherSystem = app3d.getEntityWatcherSystem();
                TreeModel model = entityWatcherSystem.getJTreeModel();
                tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
                tree.setModel(model);
                tree.addTreeSelectionListener(HierarchyTreeComponent.this);
                return null;
            }
        });
        setLayout(new BorderLayout());
        add(new JScrollPane(tree), BorderLayout.CENTER);
        //System.out.println(" Added tree!");
    }

    public void valueChanged(TreeSelectionEvent e) {
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
        if (node != null) {
            EntityInfoNode infoNode = (EntityInfoNode) node.getUserObject();
            Entity entity = infoNode.getOwner();

            inspector.setArtComponent(entity);
        }
    }
}
