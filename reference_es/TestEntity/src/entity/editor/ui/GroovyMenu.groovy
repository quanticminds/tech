package entity.editor.ui

import javax.swing.*
import groovy.swing.SwingBuilder
import java.awt.FlowLayout as FL
import javax.swing.BoxLayout as BXL

public class GroovyMenu{
    JFrame frame;
    def swing = new SwingBuilder()
    GroovyMenu(JFrame frame){
        this.frame = frame;
    }
    
    public JMenuBar createMenuBar(){
        
        return swing.menuBar {
            
            menu(text:'File') {
                menuItem() {
                    action(name:'New', closure:{ println("clicked on the new menu item!") })
                }
                menuItem() {
                    action(name:'Open', closure:{ println("clicked on the open menu item!") })
                }
                separator()
                menuItem() {
                    action(name:'Save', enabled:false, closure:{ println("clicked on the Save menu item!") })
                }
                menuItem() {
                    action(name:'Exit', closure:{ 
                            System.exit(0)
                            frame.dispose()
                        })
                }
            }
            menu(text:'Demos') {
                menuItem() {
                    action(name:'Simple TableModel Demo', closure:{  })
                }
                menuItem() {
                    action(name:'MVC Demo', closure:{  })
                }
                menuItem() {
                    action(name:'TableLayout Demo', closure:{  })
                }
            }
            menu(text:'Help') {
                menuItem() {
                    action(name:'About', closure:{ showAbout() })
                }
            }
            hglue()
            textField(columns:15)
            button(text:"Search")
        }
            
        
    }
    def showAbout(){
        def dial = swing.dialog(title:'Dialog 1',id:'myDialog',modal:true) { 
            panel() {
                boxLayout(axis:BXL.Y_AXIS)
                label(preferredSize:[400,200],text:"""
                This is a Unity clone. You know!
                    """)
                button('OK',preferredSize:[80,24],
                    actionPerformed:{
                        

                        dispose()
                    })
            }
        }
        dial.pack()
        dial.show()
    }
    def showConfig(){
       
        def dial = swing.dialog(title:'Dialog 1',id:'myDialog',modal:true) { //-- 3 --//
            panel() {
                boxLayout(axis:BXL.Y_AXIS)
                panel(alignmentX:0f) {
                    flowLayout(alignment:FL.LEFT)
                    label('Name')
                    textField(id:'name',columns:10) //-- 4 --//
                }
                panel(alignmentX:0f) {
                    flowLayout(alignment:FL.LEFT)
                    checkBox(id:'developper',text:'Developper')
                }
                panel(alignmentX:0f) {
                    flowLayout(alignment:FL.LEFT)
                    label('Gender:')
                    myGroup = buttonGroup()
                    radioButton(id:'genderMale', text:"male",
                        buttonGroup:myGroup, selected:true)
                    vstrut(height:12)
                    radioButton(id:'genderFemale', text:"female",
                        buttonGroup:myGroup)
                }
                panel(alignmentX:0f) {
                    flowLayout(alignment:FL.LEFT)
                    label('Country')
                    comboBox(id:'country',items:['luna','calypso'])
                }
                panel(alignmentX:0f) {
                    flowLayout(alignment:FL.LEFT)
                    button('OK',preferredSize:[80,24],
                        actionPerformed:{
                            vars.dialogResult = 'OK' //-- 5 --//
                            dispose()
                        })
                    button('Cancel',preferredSize:[80,24],
                        actionPerformed:{
                            vars.dialogResult = 'cancel'
                            dispose()
                        })
                }
            }
        }
        dial.pack()
        dial.show()
    }
}