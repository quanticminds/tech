/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity.editor.ui.control;

import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.EntityProcessingSystem;
import com.gamadu.starwarrior.components.SpatialForm;
import com.jme3.app.SimpleApplication;
import entity.editor.es.EntityInfoNode;
import java.util.HashMap;
import javax.swing.SwingUtilities;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;

/**
 *
 * @author hungcuong
 */
public class EntityWatcherSystem extends EntityProcessingSystem {

    private SimpleApplication container;
    private ComponentMapper<SpatialForm> spatialFormMapper;
    DefaultMutableTreeNode top = new DefaultMutableTreeNode("Hierarchy");
    DefaultTreeModel treeModel = new DefaultTreeModel(top);
    private HashMap<Entity, MutableTreeNode> map = new HashMap<Entity, MutableTreeNode>(1000);
    private boolean changed = false;

    public EntityWatcherSystem(SimpleApplication container) {
        super(SpatialForm.class);
        this.container = container;
    }

    @Override
    public void initialize() {
        spatialFormMapper = new ComponentMapper<SpatialForm>(SpatialForm.class, world);
    }

    @Override
    protected void begin() {
        super.begin();
        //top.removeAllChildren();
    }

    @Override
    protected void process(Entity e) {
        //System.out.println("Processed " + sp.getSpatialFormFile());
        //top.add(new DefaultMutableTreeNode(" " + sp.getSpatialFormFile()));
    }

    @Override
    protected void added(final Entity e) {
        super.added(e);
        EntityInfoNode info = new EntityInfoNode(e, this);
        final DefaultMutableTreeNode treeNode = new DefaultMutableTreeNode(info);

        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                map.put(e, treeNode);
                top.add(treeNode);
                changed = true;
            }
        });
    }

    @Override
    protected void removed(final Entity e) {
        super.removed(e);
        if (changed) {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    top.remove(map.get(e));
                    changed = true;
                }
            });
        }
    }

    @Override
    protected void end() {
        super.end();
        if (changed) {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    treeModel.nodeStructureChanged(top);
                    System.out.println("size: " + top.getChildCount());
                }
            });
            changed = false;
        }
    }

    public DefaultTreeModel getJTreeModel() {
        return treeModel;
    }

    public ComponentMapper<SpatialForm> getSpatialFormMapper() {
        return spatialFormMapper;
    }
}
