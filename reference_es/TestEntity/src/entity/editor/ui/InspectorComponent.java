/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity.editor.ui;

import com.artemis.Component;
import com.artemis.Entity;
import com.artemis.utils.ImmutableBag;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import org.jdesktop.swingx.JXTaskPane;
import org.jdesktop.swingx.JXTaskPaneContainer;
import org.metawidget.swing.SwingMetawidget;

/**
 *
 * @author hungcuong
 */
public class InspectorComponent extends JPanel {

    JXTaskPaneContainer inspectorPanel;

    public InspectorComponent() {
        super();
        inspectorPanel = new JXTaskPaneContainer();
        //BoxLayout boxLayout = new BoxLayout(inspectorPanel, BoxLayout.Y_AXIS);
        setLayout(new BorderLayout());
        add(new JScrollPane(inspectorPanel), BorderLayout.CENTER);
    }

    public void setArtComponent(final Entity anEntity) {

        inspectorPanel.removeAll();

        ImmutableBag<Component> bag = anEntity.getComponents();
        for (int i = 0; i < bag.size(); i++) {
            Component comp = bag.get(i);
            SwingMetawidget metawidget = new SwingMetawidget();
            metawidget.setToInspect(comp);
            JXTaskPane taskPanel = new JXTaskPane(comp.getClass().getSimpleName());
            taskPanel.add(metawidget);
            inspectorPanel.add(taskPanel);
        }
        this.validateTree();
        inspectorPanel.repaint();
        this.repaint();


    }
}
