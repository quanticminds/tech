/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package entity.ui
import org.mnode.ousia.*
import javax.swing.*;
import org.noos.xing.mydoggy.ToolWindowAnchor

def ousia = new OusiaBuilder()
ousia.edt {
    frame(title: 'MyDoggyTest', pack: true, show: true, defaultCloseOperation: JFrame.EXIT_ON_CLOSE) {

        toolWindowManager(id: 'windowManager') {
            windowManager.registerToolWindow "Test", "Test Tool", null, new JButton('Click Me'), ToolWindowAnchor.BOTTOM
        }
        windowManager.getToolWindow("Test").available = true
        windowManager.getToolWindow("Test").active = true 
    }
}