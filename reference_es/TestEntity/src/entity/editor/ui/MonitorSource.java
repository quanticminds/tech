package entity.editor.ui;

/**
 * @author Angelo De Caro
 */
public interface MonitorSource {

    float getTotal();

    float getUsed();

}
