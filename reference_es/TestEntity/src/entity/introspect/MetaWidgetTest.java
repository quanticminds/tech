/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity.introspect;

import com.gamadu.starwarrior.components.Transform;
import javax.swing.*;

import org.metawidget.swing.*;

/**
 *
 * @author cuong.nguyenmanh2
 */
public class MetaWidgetTest {

    public static void main(String[] args) {
        Transform person = new Transform();

        SwingMetawidget metawidget = new SwingMetawidget();
        metawidget.setToInspect(person);

        JFrame frame = new JFrame("Metawidget Tutorial");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(metawidget);
        frame.setSize(400, 250);
        frame.setVisible(true);
    }
}
