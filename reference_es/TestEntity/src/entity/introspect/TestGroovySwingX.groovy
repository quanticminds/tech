/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package entity.introspect

import groovy.swing.SwingXBuilder
import groovy.swing.SwingBuilder
import javax.swing.*
import java.awt.*

def swing = new SwingXBuilder()
def frame = swing.frame(title:'Frame', defaultCloseOperation:JFrame.EXIT_ON_CLOSE, pack:true, show:true) {
    /*
    multiSplitPane() {
        
    split() {
    leaf(name:"left")
    divider()
    leaf(name:"right")
    }
         
    button(text:"Left Button", constraints:"left")
    button(text:"Right Button", constraints:"right")
    }
     */
    panel(){
        boxLayout(axis:BoxLayout.Y_AXIS)
   
        taskPane(title:"A"){
            label(text:"Property1")
            label(text:"Property2")
        }
        taskPane(title:"B"){
            label(text:"Property1")
            label(text:"Property2")
        }
    }
    
}.show()
