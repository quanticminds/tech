/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package example.appstates;

import com.jme3.app.Application;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.asset.AssetManager;
import com.jme3.scene.Node;
import com.jmonkey.entitysystem.Entity;
import com.jmonkey.entitysystem.EntitySet;
import com.jmonkey.entitysystem.EntitySystem;
import example.Main;
import example.components.InSceneComponent;
import example.components.MovementComponent;
import example.components.PositionComponent;
import example.components.VisualRepComponent;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Karsten
 */
public class EntityDisplayAppState extends AbstractAppState {
    
    private EntitySet entitySet;
    private Map<Entity,EntityControl> controls;
    
    private AssetManager assetManager;
    
    private Node rootNode;
    
    public EntityDisplayAppState(Node node)
    {
        rootNode=node;
    }
    
    @Override
    public void initialize(AppStateManager stateManager, Application app)
    {
        super.initialize(stateManager,app);
        
        EntitySystem entitySystem = ((Main)app).getEntitySystem();
        this.assetManager=app.getAssetManager();
        
        controls = new HashMap<Entity,EntityControl>();
        
        entitySet = entitySystem.getEntitySet(InSceneComponent.class,
                MovementComponent.class, PositionComponent.class, VisualRepComponent.class);
        
    }
    
    @Override
    public void update(float tpf) {

        if(entitySet.applyChanges())
        {
            add(entitySet.getAddedEntities());
            changed(entitySet.getChangedEntities());
            removed(entitySet.getRemovedEntities());
        }
        
    }
    
    private void add(Set<Entity> set)
    {
        Iterator<Entity> iterator = set.iterator();
        
        while(iterator.hasNext())
        {
            Entity entity = iterator.next();
            EntityControl ec = new EntityControl(entity,assetManager);
            controls.put(entity,ec);
            rootNode.attachChild(ec.getSpatial());
        }   
    }
    
    private void changed(Set<Entity> set)
    {
        Iterator<Entity> iterator = set.iterator();
        
        while(iterator.hasNext())
        {
            controls.get(iterator.next()).updateEntity();
        }    
    }
    
    private void removed(Set<Entity> set)
    {
        Iterator<Entity> iterator = set.iterator();
        
        while(iterator.hasNext())
        {
            Entity entity = iterator.next();
            EntityControl ec = controls.get(entity);
            ec.remove();
            rootNode.detachChild(ec.getSpatial());
            controls.remove(entity);
            
        }       
    }
    
    
}
