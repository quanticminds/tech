/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package example.appstates;

import example.Utils;
import com.jme3.app.Application;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jmonkey.entitysystem.Entity;
import com.jmonkey.entitysystem.EntitySet;
import com.jmonkey.entitysystem.EntitySystem;
import example.Main;
import example.components.EnemyComponent;
import example.components.MovementComponent;
import example.components.PlayerComponent;
import example.components.PositionComponent;
import java.util.Iterator;

/**
 *
 * @author Karsten
 */
public class EnemyAppState extends AbstractAppState {
    
    private EntitySet playerSet;
    private EntitySet enemySet;
    
    
    
    public void initialize(AppStateManager stateManager, Application app) {
        EntitySystem entitySystem = ((Main)app).getEntitySystem();
        
        playerSet = entitySystem.getEntitySet(PlayerComponent.class,PositionComponent.class);
        enemySet = entitySystem.getEntitySet(EnemyComponent.class,PositionComponent.class,MovementComponent.class);
    }
    
    
   @Override
   public void update(float tpf) {
       
       if(playerSet.applyChanges())
       {
           if(playerSet.isEmpty())
               return;
           
           Entity player = playerSet.getIterator().next();
           
           enemySet.applyChanges();
           Iterator<Entity> iterator = enemySet.getIterator();
           
           while(iterator.hasNext())
           {
               Entity enemy = iterator.next();
               
               Vector3f playerPos = player.getComponent(PositionComponent.class).getLocation();
               Vector3f enemyPos = enemy.getComponent(PositionComponent.class).getLocation();
               
               float rotation = Utils.getAngle(playerPos,enemyPos);
               Vector3f movement = playerPos.subtract(enemyPos).normalize();//.mult(2);
               Quaternion q = new Quaternion();
               q = q.fromAngles(0, rotation, 0);
               
               enemy.setComponent(new PositionComponent(enemyPos,q));
               enemy.setComponent(new MovementComponent(movement));
           }
           
       }
       
   }
    
}
