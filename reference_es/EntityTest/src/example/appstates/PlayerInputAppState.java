/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package example.appstates;

import example.EntityFactory;
import example.Utils;
import com.jme3.app.Application;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.input.InputManager;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.input.controls.Trigger;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jmonkey.entitysystem.Entity;
import com.jmonkey.entitysystem.EntitySet;
import com.jmonkey.entitysystem.EntitySystem;
import example.Main;
import example.components.MovementComponent;
import example.components.PlayerComponent;
import example.components.PositionComponent;
import java.util.Iterator;

/**
 *
 * @author Karsten
 */
public class PlayerInputAppState extends AbstractAppState implements ActionListener {

    private EntitySet entitySet;
    private InputManager inputManager;
    private Camera camera;

    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        EntitySystem entitySystem = ((Main)app).getEntitySystem();
        
        entitySet = entitySystem.getEntitySet(PlayerComponent.class, MovementComponent.class, PositionComponent.class);


        this.inputManager  = app.getInputManager();
        inputManager.addMapping("w", new KeyTrigger(KeyInput.KEY_W));
        inputManager.addMapping("a", new KeyTrigger(KeyInput.KEY_A));
        inputManager.addMapping("s", new KeyTrigger(KeyInput.KEY_S));
        inputManager.addMapping("d", new KeyTrigger(KeyInput.KEY_D));

        inputManager.addMapping("shoot", new MouseButtonTrigger(0));

        inputManager.addListener(this, new String[]{"w", "a", "s", "d", "shoot"});

        this.camera = app.getCamera();
    }

    @Override
    public void update(float tpf) {
        entitySet.applyChanges();

        Vector3f origin = getMousePosition();

        Iterator<Entity> iterator = entitySet.getIterator();
        while (iterator.hasNext()) {
            Entity entity = iterator.next();
            PositionComponent pc = entity.getComponent(PositionComponent.class);

            Quaternion q = new Quaternion();

            float rotation = Utils.getAngle(origin,pc.getLocation());

            q = q.fromAngles(0, rotation, 0);

            pc = new PositionComponent(pc.getLocation(), q);
            entity.setComponent(pc);
        }

    }

    public void onAction(String name, boolean isPressed, float tpf) {

        Iterator<Entity> iterator = entitySet.getIterator();
        while (iterator.hasNext()) {
            Entity entity = iterator.next();

            if (name.equals("w")) {
                if (isPressed) {
                    changeEntityMovement(entity, new Vector3f(0, 0, 1.5f));
                } else {
                    changeEntityMovement(entity, new Vector3f(0, 0, -1.5f));
                }
            } else if (name.equals("s")) {
                if (isPressed) {
                    changeEntityMovement(entity, new Vector3f(0, 0, -1.5f));
                } else {
                    changeEntityMovement(entity, new Vector3f(0, 0, 1.5f));
                }
            } else if (name.equals("a")) {
                if (isPressed) {
                    changeEntityMovement(entity, new Vector3f(1.5f, 0, 0));
                } else {
                    changeEntityMovement(entity, new Vector3f(-1.5f, 0, 0));
                }
            } else if (name.equals("d")) {
                if (isPressed) {
                    changeEntityMovement(entity, new Vector3f(-1.5f, 0, 0));
                } else {
                    changeEntityMovement(entity, new Vector3f(1.5f, 0, 0));
                }
            }
            if (name.equals("shoot")) {
                if (isPressed) {
                    
                    Vector3f position = entity.getComponent(PositionComponent.class).getLocation();
                    Vector3f mouse = getMousePosition();
                    
                    Vector3f movement = mouse.subtract(position).normalize().mult(3);
                    Quaternion q = new Quaternion();
                    float rotation = Utils.getAngle(mouse,position);
                    q = q.fromAngles(0, rotation, 0);
                    
                    EntityFactory.createNewBullet(position, movement,q);
                }
            }
        }


    }

    private void changeEntityMovement(Entity entity, Vector3f addVector) {
        MovementComponent mc = entity.getComponent(MovementComponent.class);
        entity.setComponent(new MovementComponent(mc.getMovement().add(addVector)));
    }
    
    
    private Vector3f getMousePosition()
    {
        Vector3f origin = camera.getWorldCoordinates(inputManager.getCursorPosition(), 0.0F);
        Vector3f direction = camera.getWorldCoordinates(inputManager.getCursorPosition(), 0.3F);
        direction.subtractLocal(origin).normalizeLocal();

        float f = -origin.y / direction.y;
        origin.y = 0;
        origin.multLocal(f);
        
        return origin;
    }

}
