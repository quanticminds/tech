/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package example.appstates;

import com.jme3.app.Application;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jmonkey.entitysystem.Entity;
import com.jmonkey.entitysystem.EntitySet;
import com.jmonkey.entitysystem.EntitySystem;
import example.Main;
import example.components.ExpiresComponent;
import java.util.Iterator;


/**
 *
 * @author Karsten
 */
public class ExpiresAppState extends AbstractAppState {
    
     private EntitySet entitySet;
     
    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        EntitySystem entitySystem = ((Main)app).getEntitySystem();
        entitySet = entitySystem.getEntitySet(ExpiresComponent.class);
    }

    @Override
    public void update(float tpf) {
        entitySet.applyChanges();

        Iterator<Entity> iterator = entitySet.getIterator();

        while (iterator.hasNext()) {
            Entity entity = iterator.next(); 
            
            float time = entity.getComponent(ExpiresComponent.class).getTime()-tpf;
            
            if(time <= 0)
            {
                entity.destroy();
            }else{
                entity.setComponent(new ExpiresComponent(time));
            }
        }
    }
}
