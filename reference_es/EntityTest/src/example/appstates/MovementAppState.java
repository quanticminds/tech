/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package example.appstates;

import com.jme3.app.Application;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jmonkey.entitysystem.Entity;
import com.jmonkey.entitysystem.EntitySet;
import com.jmonkey.entitysystem.EntitySystem;
import example.Main;
import example.components.MovementComponent;
import example.components.PositionComponent;
import java.util.Iterator;

/**
 *
 * @author Karsten
 */
public class MovementAppState extends AbstractAppState {

    private EntitySet entitySet;

    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        EntitySystem entitySystem = ((Main)app).getEntitySystem();
        entitySet = entitySystem.getEntitySet(MovementComponent.class, PositionComponent.class);
    }

    @Override
    public void update(float tpf) {

        entitySet.applyChanges();

        Iterator<Entity> iterator = entitySet.getIterator();

        while (iterator.hasNext()) {
            Entity entity = iterator.next();

            MovementComponent mc = entity.getComponent(MovementComponent.class);
            PositionComponent pc = entity.getComponent(PositionComponent.class);
            
            if(mc.getMovement().x != 0 || mc.getMovement().y != 0 || mc.getMovement().z != 0)
            {
                
                Vector3f vec3f = pc.getLocation().add(mc.getMovement().mult(tpf*10));

                entity.setComponent(new PositionComponent(vec3f, pc.getRotation()));
            }
        }
    }

}