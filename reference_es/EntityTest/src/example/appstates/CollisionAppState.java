/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package example.appstates;

import com.jme3.app.Application;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.math.Vector3f;
import com.jmonkey.entitysystem.Entity;
import com.jmonkey.entitysystem.EntitySet;
import com.jmonkey.entitysystem.EntitySystem;
import example.Main;
import example.components.BulletComponent;
import example.components.PositionComponent;
import example.components.CollisionComponent;
import example.components.EnemyComponent;
import example.components.PlayerComponent;
import java.util.Iterator;

/**
 *
 * @author Karsten
 */
public class CollisionAppState extends AbstractAppState {
    
    
    private EntitySet bulletSet;
    private EntitySet playerSet;
    private EntitySet enemySet;
    
    
    
    public void initialize(AppStateManager stateManager, Application app) {
        EntitySystem entitySystem = ((Main)app).getEntitySystem();
        
        bulletSet = entitySystem.getEntitySet(BulletComponent.class,PositionComponent.class,CollisionComponent.class);
        playerSet = entitySystem.getEntitySet(PlayerComponent.class,PositionComponent.class,CollisionComponent.class);
        enemySet = entitySystem.getEntitySet(EnemyComponent.class,PositionComponent.class,CollisionComponent.class);
    }
    
    
   @Override
    public void update(float tpf) {

       playerSet.applyChanges();
       enemySet.applyChanges();
       
       //Collision player enemy
       Iterator<Entity> playerIterator = playerSet.getIterator();
       while(playerIterator.hasNext())
       {
           Entity player = playerIterator.next();
           
           Iterator<Entity>  enemyIterator = enemySet.getIterator();
           
           while(enemyIterator.hasNext())
           {
               if(checkCollistion(player, enemyIterator.next()))
               {
                   player.destroy();
                   break;
               }
           }
       }
       
       bulletSet.applyChanges();
       
       Iterator<Entity> bulletIterator = bulletSet.getIterator();
       while(bulletIterator.hasNext())
       {
           Entity bullet = bulletIterator.next();
           
           Iterator<Entity>  enemyIterator = enemySet.getIterator();
           
           while(enemyIterator.hasNext())
           {
               Entity enemy = enemyIterator.next();
               
               if(checkCollistion(bullet, enemy))
               {
                   enemy.destroy();
                   bullet.destroy();
                   enemySet.applyChanges();
                   break;
               }
           }

       }
    }
    
    
    public boolean checkCollistion(Entity a, Entity b)
    {
        
        Vector3f vecA = a.getComponent(PositionComponent.class).getLocation();
        Vector3f vecB = b.getComponent(PositionComponent.class).getLocation();
        
        float distance = a.getComponent(CollisionComponent.class).getRadius();
        distance += b.getComponent(CollisionComponent.class).getRadius();
        
        return (vecA.distance(vecB) < distance);
    }
    
}
