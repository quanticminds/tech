package example;



import com.jme3.app.SimpleApplication;
import com.jme3.light.DirectionalLight;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jmonkey.entitysystem.DefaultEntityData;
import com.jmonkey.entitysystem.EntitySystem;
import example.appstates.EntityDisplayAppState;
import example.appstates.MovementAppState;
import example.appstates.PlayerInputAppState;
import example.appstates.CollisionAppState;
import example.appstates.EnemyAppState;
import example.appstates.ExpiresAppState;


public class Main extends SimpleApplication {
    
    private long lastSpawn;
    
    private EntitySystem entitySystem;
    
    public static void main(String[] args) {
        Main app = new Main();
        app.start();
    }

    @Override
    public void simpleInitApp() {
        
       entitySystem = new EntitySystem(new DefaultEntityData());
       
       EntityFactory.init(entitySystem);

       //Init the Game Systems
       stateManager.attach(new EntityDisplayAppState(rootNode));
       stateManager.attach(new MovementAppState());
       stateManager.attach(new PlayerInputAppState());
       stateManager.attach(new ExpiresAppState());
       stateManager.attach(new CollisionAppState());
       stateManager.attach(new EnemyAppState());
       
       //Creating the player entities
       EntityFactory.createNewPlayer(Vector3f.ZERO);
       /*
       for(int x=-5;x<5;x++)
       {
            for(int z=-5;z<5;z++)
            {
                EntityFactory.createNewPlayer(new Vector3f(x*10,0,z*10));
            }
       }*/
       
        // You must add a light to make the model visible
       DirectionalLight sun = new DirectionalLight();
       sun.setDirection(new Vector3f(-0.1f, -0.7f, -1.0f));
       rootNode.addLight(sun);
       
       viewPort.setBackgroundColor(ColorRGBA.DarkGray);
       
       //Setting the camera
       this.cam.setLocation(new Vector3f(0, 96, 0));
       this.cam.lookAt(Vector3f.ZERO, Vector3f.UNIT_Y);
       
       flyCam.setEnabled(false);
       
       lastSpawn = System.currentTimeMillis();
    }
    
    
    
    @Override
    public void simpleUpdate(float tpf)
    {
        if(System.currentTimeMillis()-lastSpawn > 2000)
        {
            Vector3f position;
            int random = (int)(Math.random()*4);
            if(random == 0)
            {
                position = new Vector3f(-80,0,(float)(Math.random()*80-40));
            }else if(random == 1)
            {
                position = new Vector3f(80,0,(float)(Math.random()*80-40));
            }else if(random == 2)
            {
                position = new Vector3f(-60,0,(float)(Math.random()*160-80));
            }else{
                position = new Vector3f(60,0,(float)(Math.random()*160-80));
            }
            
            EntityFactory.createNewEnemy(position);
            lastSpawn=System.currentTimeMillis();
        }
       
    }
    
    public EntitySystem getEntitySystem()
    {
        return entitySystem;
    }


}
