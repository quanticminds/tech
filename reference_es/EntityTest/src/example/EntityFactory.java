/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package example;

import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jmonkey.entitysystem.Component;
import com.jmonkey.entitysystem.EntitySystem;
import example.components.BulletComponent;
import example.components.CollisionComponent;
import example.components.EnemyComponent;
import example.components.ExpiresComponent;
import example.components.InSceneComponent;
import example.components.MovementComponent;
import example.components.PlayerComponent;
import example.components.PositionComponent;
import example.components.VisualRepComponent;

/**
 *
 * @author Karsten
 */
public class EntityFactory {
    
    private static EntitySystem entitySystem;
    
    public static void init(EntitySystem eS)
    {
        entitySystem=eS;
    }
    
    public static void createNewPlayer(Vector3f v3f)
    {
       Component[] components = new Component[6];
       components[0] = new VisualRepComponent("Models/HoverTank/Tank2.mesh.xml"); 
       
       components[1] = new PositionComponent(v3f,new Quaternion());
       components[2] = new MovementComponent(Vector3f.ZERO);
       components[3] = new InSceneComponent(true);
       components[4] = new PlayerComponent();
       components[5] = new CollisionComponent(5);
       
       entitySystem.newEntity(components);
    }
    
    public static void createNewBullet(Vector3f position, Vector3f movement, Quaternion quat)
    {
       Component[] components = new Component[7];
       components[0] = new VisualRepComponent("Models/SpaceCraft/Rocket.mesh.xml"); 
       
       components[1] = new PositionComponent(position,quat);
       components[2] = new MovementComponent(movement);
       components[3] = new InSceneComponent(true);
       components[4] = new BulletComponent();
       components[5] = new ExpiresComponent(3);
       components[6] = new CollisionComponent(1f);
       
       entitySystem.newEntity(components);  
       
    }
    
    
    public static void createNewEnemy(Vector3f position)
    {
        
       Component[] components = new Component[6];
       components[0] = new VisualRepComponent("Models/Oto/Oto.mesh.xml"); 
       
       components[1] = new PositionComponent(position,new Quaternion());
       components[2] = new MovementComponent(Vector3f.ZERO);
       components[3] = new InSceneComponent(true);
       components[4] = new EnemyComponent();
       components[5] = new CollisionComponent(2);
       
       entitySystem.newEntity(components);  
       
    }
    
}
