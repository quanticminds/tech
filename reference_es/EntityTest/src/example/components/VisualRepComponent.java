/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package example.components;

import com.jmonkey.entitysystem.Component;

/**
 *
 * @author Karsten
 */
public class VisualRepComponent extends Component {
    
    private String assetName;
    
    public VisualRepComponent(String assetName)
    {
        this.assetName=assetName;
    }
    
    public String getAssetName()
    {
        return assetName;
    }
}
