/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package example.components;

import com.jmonkey.entitysystem.Component;

/**
 *
 * @author Karsten
 */
public class InSceneComponent extends Component {
    
    private boolean value;
    
    public InSceneComponent(boolean value)
    {
        this.value=value;
    }
       
    public boolean getValue()
    {
        return value;
    }
    
}
