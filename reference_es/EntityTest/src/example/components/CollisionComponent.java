/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package example.components;

import com.jmonkey.entitysystem.Component;

/**
 *
 * @author Karsten
 */
public class CollisionComponent extends Component {
    
    private float radius;
    
    public CollisionComponent(float radius)
    {
        this.radius=radius;
    }
    
    public float getRadius()
    {
        return radius;
    }
    
}
