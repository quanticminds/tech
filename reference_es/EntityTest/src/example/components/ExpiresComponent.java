/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package example.components;

import com.jmonkey.entitysystem.Component;

/**
 *
 * @author Karsten
 */
public class ExpiresComponent extends Component {
    
    float time;
    
    public ExpiresComponent(float time)
    {
        this.time=time;
    }
    
    public float getTime()
    {
        return time;
    }
}
