/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package example;

import com.jme3.math.Vector3f;

/**
 *
 * @author Karsten
 */
public class Utils {
    
    public static float getAngle(Vector3f a, Vector3f b)
    {
        double dy = a.z - b.z;
        double dx = a.x - b.x;
        return (float) (Math.atan2(dx, dy));
    }
    
}
