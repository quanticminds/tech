/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jme3.powermonkey;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author normenhansen
 */
public final class Entities {

    private static long idx = 0;
    //TODO: DB
    private static ConcurrentHashMap<Long, ConcurrentHashMap<Class, Object>> components = new ConcurrentHashMap<Long, ConcurrentHashMap<Class, Object>>();

    public static Entity getNewEntity() {
        return new Entity(idx++);
    }

    public static <T extends Object> T getComponent(long entityId, Class<T> controlType) {
        ConcurrentHashMap<Class, Object> entityMap = components.get(entityId);
        if (entityMap == null) {
            return null;
        } else {
            return (T) entityMap.get(controlType);
        }
    }

    public static void setComponent(long entityId, Object component) {
        ConcurrentHashMap<Class, Object> entityMap = components.get(entityId);
        if (entityMap == null) {
            entityMap = new ConcurrentHashMap<Class, Object>();
            components.put(entityId, entityMap);
        }
        entityMap.put(component.getClass(), component);
    }

    public static void clearComponent(long entityId, Class componentClass) {
        ConcurrentHashMap<Class, Object> entityMap = components.get(entityId);
        if (entityMap == null) {
            return;
        }
        entityMap.remove(componentClass);
    }

    public static List<Entity> getEntities(Class component) {
        LinkedList<Entity> list = new LinkedList<Entity>();
        for (Iterator<Entry<Long, ConcurrentHashMap<Class, Object>>> it = components.entrySet().iterator(); it.hasNext();) {
            Entry<Long, ConcurrentHashMap<Class, Object>> entry = it.next();
            if (entry.getValue().containsKey(component)) {
                list.add(new Entity(entry.getKey()));
            }
        }
        return list;
    }

    public static List<Entity> getEntities(Object component) {
        LinkedList<Entity> list = new LinkedList<Entity>();
        for (Iterator<Entry<Long, ConcurrentHashMap<Class, Object>>> it = components.entrySet().iterator(); it.hasNext();) {
            Entry<Long, ConcurrentHashMap<Class, Object>> entry = it.next();
            if (entry.getValue().containsKey(component.getClass())) {
                Object curComponent = entry.getValue().get(component.getClass());
                if (curComponent.equals(component)) {
                    list.add(new Entity(entry.getKey()));
                }
            }
        }
        return list;
    }
}
