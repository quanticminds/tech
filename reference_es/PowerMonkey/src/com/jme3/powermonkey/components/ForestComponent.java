/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jme3.powermonkey.components;

/**
 *
 * @author normenhansen
 */
public final class ForestComponent {
    private int size;
    private int density;
    private ForestType type;

    public ForestComponent(int size, int density, ForestType type) {
        this.size = size;
        this.density = density;
        this.type = type;
    }

    public int getSize() {
        return size;
    }

    public int getDensity() {
        return density;
    }

    public ForestType getType() {
        return type;
    }
    
    public static enum ForestType{
        Leaf,
        Needle,
        Mixed
    }
    
}
