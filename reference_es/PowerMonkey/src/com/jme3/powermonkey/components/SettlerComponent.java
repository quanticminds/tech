/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jme3.powermonkey.components;

import com.jme3.powermonkey.Entity;

/**
 *
 * @author normenhansen
 */
public final class SettlerComponent {

    private Entity home;

    public SettlerComponent(Entity home) {
        this.home = home;
    }

    public Entity getSettlement() {
        return home;
    }

    @Override
    public int hashCode() {
        return home.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof SettlerComponent) {
            SettlerComponent comp = (SettlerComponent) o;
            return home.equals(comp.getSettlement());
        }
        return super.equals(o);
    }
}
