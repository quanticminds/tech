/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jme3.powermonkey.components;

/**
 *
 * @author normenhansen
 */
public final class NurseryComponent {

    private long lastBirth;

    public NurseryComponent(long lastBirth) {
        this.lastBirth = lastBirth;
    }

    public long getLastBirth() {
        return lastBirth;
    }
}
