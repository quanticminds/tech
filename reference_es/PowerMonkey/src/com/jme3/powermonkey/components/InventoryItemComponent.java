/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jme3.powermonkey.components;

import com.jme3.powermonkey.Entity;

/**
 *
 * @author normenhansen
 */
public final class InventoryItemComponent {

    private Entity owner;

    public InventoryItemComponent(Entity owner) {
        this.owner = owner;
    }

    public Entity getOwner() {
        return owner;
    }

    @Override
    public int hashCode() {
        return owner.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof InventoryItemComponent) {
            InventoryItemComponent comp = (InventoryItemComponent) o;
            return owner.equals(comp.getOwner());
        }
        return super.equals(o);
    }
}
