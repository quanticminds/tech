/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jme3.powermonkey.components;

/**
 *
 * @author normenhansen
 */
public final class VehicleComponent {

    private int speed;
    private boolean swimming;

    public VehicleComponent(int speed, boolean swimming) {
        this.speed = speed;
        this.swimming = swimming;
    }

    public int getSpeed() {
        return speed;
    }

    public boolean isSwimming() {
        return swimming;
    }
}
