/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jme3.powermonkey.components;

/**
 *
 * @author normenhansen
 */
public final class VisualRepComponent {

    private String assetName;

    public VisualRepComponent(String assetName) {
        this.assetName = assetName;
    }

    public String getAssetName() {
        return assetName;
    }

}
