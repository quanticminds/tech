/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jme3.powermonkey.components;

/**
 *
 * @author normenhansen
 */
public final class ActionComponent {

    private Action action;

    public ActionComponent(Action action) {
        this.action = action;
    }

    public Action getAction() {
        return action;
    }

    public static enum Action {

        Attack,
        Passive,
        Invent,
        Gather
    }
}
