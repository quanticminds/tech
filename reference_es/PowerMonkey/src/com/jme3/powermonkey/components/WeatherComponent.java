/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jme3.powermonkey.components;

/**
 *
 * @author normenhansen
 */
public final class WeatherComponent {

    private Weather weather;

    public WeatherComponent(Weather weather) {
        this.weather = weather;
    }

    public Weather getWeather() {
        return weather;
    }

    public static enum Weather {

        Sunny,
        Raining,
        Snowing,
        Wind
    }
}
