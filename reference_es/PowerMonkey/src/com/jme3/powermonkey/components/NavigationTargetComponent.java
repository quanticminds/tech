/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jme3.powermonkey.components;

import com.jme3.math.Vector3f;

/**
 *
 * @author normenhansen
 */
public class NavigationTargetComponent {

    private Vector3f target;

    public NavigationTargetComponent(Vector3f target) {
        this.target = target;
    }

    public Vector3f getTarget() {
        return target;
    }
}
