/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jme3.powermonkey.components;

import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;

/**
 *
 * @author normenhansen
 */
public final class PositionComponent {

    private Vector3f location;
    private Quaternion rotation;
    private long lastUpdate;

    public PositionComponent(Vector3f location, Quaternion rotation, long lastUpdate) {
        this.location = location.clone();
        this.rotation = rotation.clone();
        this.lastUpdate = lastUpdate;
    }

    public Vector3f getLocation() {
        return location;
    }

    public Quaternion getRotation() {
        return rotation;
    }

    public long getLastUpdate() {
        return lastUpdate;
    }
}