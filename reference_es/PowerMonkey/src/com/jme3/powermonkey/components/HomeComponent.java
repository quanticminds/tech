/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jme3.powermonkey.components;

import com.jme3.powermonkey.Entity;

/**
 *
 * @author normenhansen
 */
public final class HomeComponent {

    private Entity home;

    public HomeComponent(Entity home) {
        this.home = home;
    }

    public Entity getHome() {
        return home;
    }

    @Override
    public int hashCode() {
        return home.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof HomeComponent) {
            HomeComponent comp = (HomeComponent) o;
            return home.equals(comp.getHome());
        }
        return super.equals(o);
    }
}
