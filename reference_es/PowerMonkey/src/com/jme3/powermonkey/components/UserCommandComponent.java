/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jme3.powermonkey.components;

import com.jme3.powermonkey.components.ActionComponent.Action;
import com.jme3.powermonkey.components.StanceComponent.Stance;

/**
 *
 * @author normenhansen
 */
public final class UserCommandComponent {

    private int selectedCommander;
    private Stance selectedStance;
    private Action selectedAction;

    public UserCommandComponent(int selectedCommander, Stance selectedStance, Action selectedAction) {
        this.selectedCommander = selectedCommander;
        this.selectedStance = selectedStance;
        this.selectedAction = selectedAction;
    }

    public int getSelectedCommander() {
        return selectedCommander;
    }

    public Stance getSelectedStance() {
        return selectedStance;
    }

    public Action getSelectedAction() {
        return selectedAction;
    }
}
