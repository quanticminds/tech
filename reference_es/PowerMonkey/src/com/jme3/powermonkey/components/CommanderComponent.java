/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jme3.powermonkey.components;

/**
 *
 * @author normenhansen
 */
public final class CommanderComponent {

    private boolean secondary;

    public CommanderComponent(boolean secondary) {
        this.secondary = secondary;
    }

    public boolean isSecondary() {
        return secondary;
    }
}
