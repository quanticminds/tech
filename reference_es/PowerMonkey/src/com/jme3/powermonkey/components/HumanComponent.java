/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jme3.powermonkey.components;

/**
 *
 * @author normenhansen
 */
public final class HumanComponent {

    private int age;
    private int moral;
    private int hunger;
    private long lastUpdate;

    public HumanComponent(int age, int moral, int hunger, long lastUpdate) {
        this.age = age;
        this.moral = moral;
        this.hunger = hunger;
        this.lastUpdate = lastUpdate;
    }

    public int getAge() {
        return age;
    }

    public int getMoral() {
        return moral;
    }

    public int getHunger() {
        return hunger;
    }

    public long getLastUpdate() {
        return lastUpdate;
    }
}
