/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jme3.powermonkey.components;

/**
 *
 * @author normenhansen
 */
public final class WoodComponent {

    private int woodValue;

    public WoodComponent(int woodValue) {
        this.woodValue = woodValue;
    }

    public int getWoodValue() {
        return woodValue;
    }
}
