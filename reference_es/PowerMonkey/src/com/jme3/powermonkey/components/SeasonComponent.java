/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jme3.powermonkey.components;

/**
 *
 * @author normenhansen
 */
public final class SeasonComponent {

    private Season season;

    public SeasonComponent(Season season) {
        this.season = season;
    }

    public Season getSeason() {
        return season;
    }

    public static enum Season {

        Spring,
        Summer,
        Autumn,
        Winter
    }
}
