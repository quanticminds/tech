/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jme3.powermonkey.components;

/**
 *
 * @author normenhansen
 */
public final class StanceComponent {

    private Stance stance;

    public StanceComponent(Stance stance) {
        this.stance = stance;
    }

    public Stance getStance() {
        return stance;
    }

    public static enum Stance {

        Passive,
        Neutral,
        Aggressive
    }
}
