/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jme3.powermonkey.components;

/**
 *
 * @author normenhansen
 */
public class TimeFactorComponent {

    private float timeFactor;

    public TimeFactorComponent(float timeFactor) {
        this.timeFactor = timeFactor;
    }

    public float getTimeFactor() {
        return timeFactor;
    }
}
