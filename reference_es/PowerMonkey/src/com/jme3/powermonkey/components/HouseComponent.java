/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jme3.powermonkey.components;

/**
 *
 * @author normenhansen
 */
public class HouseComponent {

    private int size;

    public HouseComponent(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }
}
