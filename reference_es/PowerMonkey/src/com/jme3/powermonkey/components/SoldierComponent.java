/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jme3.powermonkey.components;

import com.jme3.powermonkey.Entity;

/**
 *
 * @author normenhansen
 */
public final class SoldierComponent {

    private Entity commander;

    public SoldierComponent(Entity commander) {
        this.commander = commander;
    }

    public Entity getCommander() {
        return commander;
    }

    @Override
    public int hashCode() {
        return commander.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof SoldierComponent) {
            SoldierComponent comp = (SoldierComponent) o;
            return commander.equals(comp.getCommander());
        }
        return super.equals(o);
    }
}
