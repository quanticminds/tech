/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jme3.powermonkey.components;

import com.jme3.powermonkey.Entity;

/**
 *
 * @author normenhansen
 */
public final class WorkshopComponent {

    private Entity settlement;

    public WorkshopComponent(Entity settlement) {
        this.settlement = settlement;
    }

    public Entity getSettlement() {
        return settlement;
    }

    @Override
    public int hashCode() {
        return settlement.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof WorkshopComponent) {
            WorkshopComponent comp = (WorkshopComponent) o;
            return settlement.equals(comp.getSettlement());
        }
        return super.equals(o);
    }
}
