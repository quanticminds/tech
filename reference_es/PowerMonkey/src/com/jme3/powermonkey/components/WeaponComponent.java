/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jme3.powermonkey.components;

/**
 *
 * @author normenhansen
 */
public final class WeaponComponent {

    private int attackValue;
    private float attackSpeed;
    private float range;
    private float rangedSpeed;

    public WeaponComponent(int attackValue, float attackSpeed, float range, float rangedSpeed) {
        this.attackValue = attackValue;
        this.attackSpeed = attackSpeed;
        this.range = range;
        this.rangedSpeed = rangedSpeed;
    }

    public int getAttackValue() {
        return attackValue;
    }

    public float getAttackSpeed() {
        return attackSpeed;
    }

    public float getRange() {
        return range;
    }

    public float getRangedSpeed() {
        return rangedSpeed;
    }
}
