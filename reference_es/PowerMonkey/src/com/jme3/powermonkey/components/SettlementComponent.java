/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jme3.powermonkey.components;

/**
 *
 * @author normenhansen
 */
public final class SettlementComponent {

    private int size;

    public SettlementComponent(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }
}
