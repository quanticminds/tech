/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jme3.powermonkey.components;

import com.jme3.powermonkey.Entity;

/**
 *
 * @author normenhansen
 */
public final class TargetComponent {

    private Entity targetEntity;

    public TargetComponent(Entity targetEntity) {
        this.targetEntity = targetEntity;
    }

    public Entity getTargetEntity() {
        return targetEntity;
    }

    @Override
    public int hashCode() {
        return targetEntity.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof TargetComponent) {
            TargetComponent comp = (TargetComponent) o;
            return targetEntity.equals(comp.getTargetEntity());
        }
        return super.equals(o);
    }
}
