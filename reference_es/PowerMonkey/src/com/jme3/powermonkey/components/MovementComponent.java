/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jme3.powermonkey.components;

import com.jme3.math.Vector3f;

/**
 *
 * @author normenhansen
 */
public final class MovementComponent {

    private Vector3f movement;
    private Vector3f rotation;

    public MovementComponent(Vector3f movement, Vector3f rotation) {
        this.movement = movement;
        this.rotation = rotation;
    }

    public Vector3f getMovement() {
        return movement;
    }

    public Vector3f getRotation() {
        return rotation;
    }
}
