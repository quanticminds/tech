/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jme3.powermonkey.components;

import jme3tools.navmesh.NavMeshPathfinder;

/**
 *
 * @author normenhansen
 */
public class PathfinderComponent {

    private NavMeshPathfinder pathfinder;

    public PathfinderComponent(NavMeshPathfinder pathfinder) {
        this.pathfinder = pathfinder;
    }

    public NavMeshPathfinder getPathfinder() {
        return pathfinder;
    }
}
