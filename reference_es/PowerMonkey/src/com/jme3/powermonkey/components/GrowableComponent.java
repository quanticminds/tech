/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jme3.powermonkey.components;

/**
 *
 * @author normenhansen
 */
public final class GrowableComponent {

    private float growSpeed;
    private float ripeness;
    private long lastUpdate;

    public GrowableComponent(float growSpeed, float ripeness, long lastUpdate) {
        this.growSpeed = growSpeed;
        this.ripeness = ripeness;
        this.lastUpdate = lastUpdate;
    }

    public float getGrowSpeed() {
        return growSpeed;
    }

    public float getRipeness() {
        return ripeness;
    }

    public long getLastUpdate() {
        return lastUpdate;
    }
}
