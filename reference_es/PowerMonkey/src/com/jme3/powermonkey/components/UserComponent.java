/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jme3.powermonkey.components;

/**
 *
 * @author normenhansen
 */
public final class UserComponent {

    private String name;

    public UserComponent(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
