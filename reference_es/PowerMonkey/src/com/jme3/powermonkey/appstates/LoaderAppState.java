/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jme3.powermonkey.appstates;

import com.jme3.app.Application;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.asset.AssetManager;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.powermonkey.Entities;
import com.jme3.powermonkey.Entity;
import com.jme3.powermonkey.EntityLists;
import com.jme3.powermonkey.ToolBox;
import com.jme3.powermonkey.components.CommanderComponent;
import com.jme3.powermonkey.components.ConqueredComponent;
import com.jme3.powermonkey.components.ForestComponent;
import com.jme3.powermonkey.components.GeneralInfoComponent;
import com.jme3.powermonkey.components.HomeComponent;
import com.jme3.powermonkey.components.HumanComponent;
import com.jme3.powermonkey.components.InSceneComponent;
import com.jme3.powermonkey.components.KeepComponent;
import com.jme3.powermonkey.components.MovementComponent;
import com.jme3.powermonkey.components.HouseComponent;
import com.jme3.powermonkey.components.NavigationTargetComponent;
import com.jme3.powermonkey.components.PositionComponent;
import com.jme3.powermonkey.components.SettlementComponent;
import com.jme3.powermonkey.components.SettlerComponent;
import com.jme3.powermonkey.components.SoldierComponent;
import com.jme3.powermonkey.components.TimeFactorComponent;
import com.jme3.powermonkey.components.UserComponent;
import com.jme3.powermonkey.components.VisualRepComponent;
import com.jme3.powermonkey.components.WeatherComponent;
import com.jme3.powermonkey.components.WeatherComponent.Weather;
import com.jme3.powermonkey.components.WorkshopComponent;
import com.jme3.powermonkey.controls.EntityControl;
import com.jme3.powermonkey.timebased.HumanAISystem;
import com.jme3.powermonkey.timebased.NavigationSystem;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.SceneGraphVisitorAdapter;
import com.jme3.terrain.geomipmap.TerrainLodControl;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.logging.Logger;
import jme3tools.navmesh.NavMesh;

/**
 *
 * @author normenhansen
 */
public class LoaderAppState extends AbstractAppState {
    
    private AssetManager assetManager;
    private Application app;
    private static final Logger logger = Logger.getLogger(LoaderAppState.class.getName());
    private Node level;
    private Node modelsNode;
    private Node rootNode;
    private NavigationSystem navSystem;
    private HumanAISystem aiSystem = new HumanAISystem();
    //TODO:time!!
    private long time = 0;
    private static Random rnd = new Random(System.currentTimeMillis());
    
    public LoaderAppState(Node rootNode) {
        this.rootNode = rootNode;
    }
    
    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
        this.app = app;
        assetManager = app.getAssetManager();
        prepareWorldEntity();
        prepareUserEntites();
        loadLevel("Scenes/level_1.j3o");
        loadLevelData();
        attachLevel();
    }
    
    @Override
    public void update(float tpf) {
        super.update(tpf);
        //System.currentTimeMillis();
        time = (long) (time + (tpf * 1000.0f * ToolBox.getTimeFactor().getTimeFactor()));
        aiSystem.updateSystem(time);
        navSystem.updateSystem(time);
        List<Entity> noMove = EntityLists.filterComponent(Entities.getEntities(SettlerComponent.class), NavigationTargetComponent.class);
        for (Iterator<Entity> it = noMove.iterator(); it.hasNext();) {
            Entity entity = it.next();
            if(rnd.nextFloat()>0.999)
            entity.setComponent(new NavigationTargetComponent(navSystem.getLocationOnTerrain(new Vector3f((rnd.nextFloat()) * 100, 0, (rnd.nextFloat()) * -100))));
        }
        entityVisualRepCheck(tpf);
    }
    
    private void entityVisualRepCheck(float tpf) {
        if (modelsNode == null) {
            return;
        }
        List<Entity> list = EntityLists.filterComponent(Entities.getEntities(VisualRepComponent.class), InSceneComponent.class);
        for (Iterator<Entity> it = list.iterator(); it.hasNext();) {
            Entity entity = it.next();
            entity.setComponent(new InSceneComponent());
            Node model = new Node("Model");
            model.addControl(new EntityControl(entity, assetManager));
            modelsNode.attachChild(model);
        }
    }
    
    private void prepareWorldEntity() {
        Entity world = Entities.getNewEntity();
        world.setComponent(new WeatherComponent(Weather.Sunny));
        world.setComponent(new TimeFactorComponent(1));
    }
    
    private void prepareUserEntites() {
        Entity user = Entities.getNewEntity();
        user.setComponent(new UserComponent("User"));
    }
    
    private void loadLevel(String name) {
        if (level != null) {
            closeLevel();
        }
        level = (Node) assetManager.loadModel(name);
        modelsNode = new Node("Models_Node");
        level.attachChild(modelsNode);
        Geometry navMeshGeom = (Geometry) level.getChild("NavMesh");
        level.getChild("terrain-level_1").getControl(TerrainLodControl.class).setCamera(app.getCamera());
        NavMesh navMesh = null;
        if (navMeshGeom != null) {
            navMesh = new NavMesh(navMeshGeom.getMesh());
        }
        navSystem = new NavigationSystem(level.getChild("Terrain"), navMesh);
    }
    
    private void loadLevelData() {
        SceneGraphVisitorAdapter adapter = new SceneGraphVisitorAdapter() {
            
            List<Entity> users = Entities.getEntities(UserComponent.class);
            public int uid = 0;
            
            @Override
            public void visit(Node node) {
                super.visit(node);
                if ("Keep".equals(node.getName())) {
                    createKeep(node);
                } else if ("Settlement".equals(node.getName())) {
                    createSettlement(node);
                } else if ("Forest".equals(node.getName())) {
                    Entity forest = Entities.getNewEntity();
                    forest.setComponent(new GeneralInfoComponent((String) node.getUserData("Name"), (String) node.getUserData("Description")));
                    forest.setComponent(new PositionComponent(node.getWorldTranslation(), node.getWorldRotation(), 0));
                    forest.setComponent(new ForestComponent(0, 0, ForestComponent.ForestType.Mixed));
                    //TODO: trees
                } else if ("Item".equals(node.getName())) {
                    Entity item = Entities.getNewEntity();
                    item.setComponent(new GeneralInfoComponent((String) node.getUserData("Name"), (String) node.getUserData("Description")));
                    item.setComponent(new PositionComponent(node.getWorldTranslation(), node.getWorldRotation(), 0));
                }
            }
            
            private void createSettlement(Node node) {
                Entity settlement = Entities.getNewEntity();
                settlement.setComponent(new GeneralInfoComponent((String) node.getUserData("Name"), (String) node.getUserData("Description")));
                settlement.setComponent(new PositionComponent(node.getWorldTranslation(), node.getWorldRotation(), 0));
                settlement.setComponent(new SettlementComponent((Integer) node.getUserData("Size")));
                settlement.setComponent(new VisualRepComponent("Models/Pillar.j3o"));
                int houses = 0;
                switch (settlement.getComponent(SettlementComponent.class).getSize()) {
                    case 0:
                        houses = 4;
                        break;
                    case 1:
                        houses = 6;
                        break;
                    case 2:
                        houses = 8;
                        break;
                    default:
                        houses = 8;
                        break;
                }
                float val = (FastMath.PI * 2.0f) / (float) houses;
                Vector3f locationAdd = new Vector3f(0, 0, 3 * houses);
                Quaternion quat = new Quaternion().fromAngles(0, val, 0);
                for (int i = 0; i < houses - 1; i++) {
                    //compute house location/rotation
                    Vector3f location = node.getWorldTranslation().add(locationAdd);
                    Vector3f settlementLocation = settlement.getComponent(PositionComponent.class).getLocation();
                    Quaternion rotation = new Quaternion();
                    Vector3f direction = settlementLocation.subtract(location);
                    direction.setY(0).normalizeLocal();
                    rotation.lookAt(direction, Vector3f.UNIT_Y);
                    quat.multLocal(locationAdd);
                    //create house entity
                    Entity house = Entities.getNewEntity();
                    house.setComponent(new GeneralInfoComponent("A house", (String) node.getUserData("A house")));
                    house.setComponent(new HouseComponent(4));
                    house.setComponent(new PositionComponent(navSystem.getLocationOnTerrain(location), rotation, 0));
                    switch (settlement.getComponent(SettlementComponent.class).getSize()) {
                        case 0:
                            house.setComponent(new VisualRepComponent("Models/House_A.j3o"));
                            break;
                        case 1:
                            house.setComponent(new VisualRepComponent("Models/House_B.j3o"));
                            break;
                        case 2:
                            house.setComponent(new VisualRepComponent("Models/House_C.j3o"));
                            break;
                        default:
                            house.setComponent(new VisualRepComponent("Models/House_C.j3o"));
                            break;
                    }
                    for (int r = 0; r < house.getComponent(HouseComponent.class).getSize(); r++) {
                        Entity inhabitant = Entities.getNewEntity();
                        inhabitant.setComponent(new GeneralInfoComponent("A guy", "A guys bio"));
                        inhabitant.setComponent(house.getComponent(PositionComponent.class));
                        inhabitant.setComponent(new HumanComponent(20, 100, 0, 0));
                        inhabitant.setComponent(new HomeComponent(settlement));
                        inhabitant.setComponent(new SettlerComponent(settlement));
                        inhabitant.setComponent(new VisualRepComponent("Models/Soldier.j3o"));
                    }
                }
                Entity workshop = Entities.getNewEntity();
                workshop.setComponent(new GeneralInfoComponent("A workshop", (String) node.getUserData("A workshop")));
                workshop.setComponent(new WorkshopComponent(settlement));
                Vector3f location = node.getWorldTranslation().add(locationAdd);
                Vector3f settlementLocation = settlement.getComponent(PositionComponent.class).getLocation();
                Quaternion rotation = new Quaternion();
                Vector3f direction = settlementLocation.subtract(location);
                direction.setY(0).normalizeLocal();
                rotation.lookAt(direction, Vector3f.UNIT_Y);
                workshop.setComponent(new PositionComponent(navSystem.getLocationOnTerrain(location), rotation, 0));
                workshop.setComponent(new VisualRepComponent("Models/Workshop.j3o"));
            }
            
            private void createKeep(Node node) {
                Entity keep = Entities.getNewEntity();
                keep.setComponent(new GeneralInfoComponent((String) node.getUserData("Name"), (String) node.getUserData("Description")));
                keep.setComponent(new PositionComponent(node.getWorldTranslation(), node.getWorldRotation(), 0));
                keep.setComponent(new VisualRepComponent("Models/Keep.j3o"));
                keep.setComponent(new HouseComponent((Integer) node.getUserData("Population")));
                if (uid < users.size()) {
                    keep.setComponent(new KeepComponent(users.get(uid)));
                    Entity commander = Entities.getNewEntity();
                    commander.setComponent(new GeneralInfoComponent("Your commander", "Your commanders bio"));
                    commander.setComponent(new PositionComponent(node.getWorldTranslation(), node.getWorldRotation(), 0));
                    commander.setComponent(new CommanderComponent(false));
                    commander.setComponent(new ConqueredComponent(users.get(uid)));
                    commander.setComponent(new HumanComponent(20, 100, 0, 0));
                    commander.setComponent(new HomeComponent(keep));
                    commander.setComponent(new SoldierComponent(commander));
                    commander.setComponent(new VisualRepComponent("Models/Soldier.j3o"));
                    for (int i = 0; i < keep.getComponent(HouseComponent.class).getSize(); i++) {
                        Entity inhabitant = Entities.getNewEntity();
                        inhabitant.setComponent(new GeneralInfoComponent("A soldier", "A soldiers bio"));
                        inhabitant.setComponent(new PositionComponent(node.getWorldTranslation(), node.getWorldRotation(), 0));
                        inhabitant.setComponent(new HumanComponent(20, 100, 0, 0));
                        inhabitant.setComponent(new HomeComponent(keep));
                        inhabitant.setComponent(new SoldierComponent(commander));
                        inhabitant.setComponent(new VisualRepComponent("Models/Soldier.j3o"));
                    }
                    uid++;
                }
            }
        };
        level.depthFirstTraversal(adapter);
    }
    
    private void attachLevel() {
        rootNode.attachChild(level);
    }
    
    private void closeLevel() {
        rootNode.detachChild(level);
        level = null;
        modelsNode = null;
        navSystem = null;
    }
}
