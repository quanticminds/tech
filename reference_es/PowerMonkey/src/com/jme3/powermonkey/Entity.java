/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jme3.powermonkey;

/**
 * Just a helper class that is mostly initialized directly in stack
 * @author normenhansen
 */
public final class Entity {

    private long id;

    public Entity(long id) {
        this.id = id;
    }

    public <T extends Object> T getComponent(Class<T> controlType) {
        return Entities.getComponent(id, controlType);
    }

    public void setComponent(Object comp) {
        Entities.setComponent(id, comp);
    }
    
    public void clearComponent(Class componentType){
        Entities.clearComponent(id, componentType);
    }

    public long getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Entity) {
            Entity entity = (Entity) o;
            return entity.getId() == id;
        }
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return (int) id;
    }
}
