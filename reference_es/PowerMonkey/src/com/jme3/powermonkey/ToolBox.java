/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jme3.powermonkey;

import com.jme3.powermonkey.components.PositionComponent;
import com.jme3.powermonkey.components.TimeFactorComponent;
import java.util.List;

/**
 *
 * @author normenhansen
 */
public class ToolBox {

    public static boolean isInRange(Entity entityA, Entity entityB, float distance) {
        PositionComponent posA = entityA.getComponent(PositionComponent.class);
        PositionComponent posB = entityB.getComponent(PositionComponent.class);
        if (posA != null && posB != null) {
            if (posA.getLocation().subtract(posB.getLocation()).length() <= distance) {
                return true;
            }
        }
        return false;
    }

    public static TimeFactorComponent getTimeFactor() {
        List<Entity> entities = Entities.getEntities(TimeFactorComponent.class);
        if (entities.size() > 0) {
            return entities.get(0).getComponent(TimeFactorComponent.class);
        }
        return null;
    }
}
