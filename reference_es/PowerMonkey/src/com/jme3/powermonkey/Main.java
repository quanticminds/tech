package com.jme3.powermonkey;

import com.jme3.app.SimpleApplication;
import com.jme3.post.FilterPostProcessor;
import com.jme3.powermonkey.appstates.GameUIAppState;
import com.jme3.powermonkey.appstates.LoaderAppState;
import com.jme3.renderer.RenderManager;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * test
 * @author normenhansen
 */
public class Main extends SimpleApplication {

//    private static ScheduledThreadPoolExecutor executor = new ScheduledThreadPoolExecutor(1);

    public static void main(String[] args) {
        Main app = new Main();
        app.start();
    }

    @Override
    public void simpleInitApp() {
        disableLogging();
        setupUI();
        flyCam.setMoveSpeed(100);
        LoaderAppState sys = new LoaderAppState(rootNode);
        stateManager.attach(sys);
        GameUIAppState ui = new GameUIAppState();
        stateManager.attach(ui);
        FilterPostProcessor fpp = (FilterPostProcessor) assetManager.loadAsset("Filters/scenefilter_normal.j3f");
        viewPort.addProcessor(fpp);
    }

    private void disableLogging() throws SecurityException {
        Logger root = Logger.getLogger("");
        Handler[] handlers = root.getHandlers();
        for (int i = 0; i < handlers.length; i++) {
            if (handlers[i] instanceof ConsoleHandler) {
                ((ConsoleHandler) handlers[i]).setLevel(Level.WARNING);
            }
        }
    }

    private void setupUI() {
    }

    @Override
    public void simpleUpdate(float tpf) {
    }

    @Override
    public void simpleRender(RenderManager rm) {
    }

    @Override
    public void destroy() {
        super.destroy();
//        executor.shutdown();
    }
}
