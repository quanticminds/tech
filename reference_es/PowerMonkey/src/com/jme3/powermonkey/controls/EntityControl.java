/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jme3.powermonkey.controls;

import com.jme3.animation.AnimChannel;
import com.jme3.animation.AnimControl;
import com.jme3.animation.SkeletonControl;
import com.jme3.asset.AssetManager;
import com.jme3.powermonkey.Entity;
import com.jme3.powermonkey.components.MovementComponent;
import com.jme3.powermonkey.components.PositionComponent;
import com.jme3.powermonkey.components.InSceneComponent;
import com.jme3.powermonkey.components.VisualRepComponent;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.SceneGraphVisitorAdapter;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;
import com.jme3.scene.control.Control;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import jme3tools.optimize.TextureAtlas;

/**
 *
 * @author normenhansen
 */
public final class EntityControl extends AbstractControl {

    private static final Logger logger = Logger.getLogger(EntityControl.class.getName());
    private static final Map<String, Spatial> models = new ConcurrentHashMap<String, Spatial>();
    private Entity entity;
    private AssetManager manager;
    private String currentModelName;
    private Spatial currentModel;
    private long lastLocationUpdate;
    private float updateTime;
    private List<AnimControl> animControls;
    private List<AnimChannel> animChannels;

    public EntityControl(Entity entity, AssetManager manager) {
        this.entity = entity;
        this.manager = manager;
    }

    @Override
    public void setSpatial(Spatial spatial) {
        super.setSpatial(spatial);
    }

    @Override
    protected void controlUpdate(float tpf) {
        if (entity == null) {
            return;
        }
        if (!updateVisualRep()) {
            return;
        }
        if (!updateLocation(tpf)) {
            return;
        }
        if (!updateAnimation()) {
            return;
        }
    }

    private boolean updateVisualRep() {
        VisualRepComponent visRep = entity.getComponent(VisualRepComponent.class);
        if (visRep != null) {
            if (currentModelName != null && currentModelName.equals(visRep.getAssetName())) {
                return true;
            } else {
                if (currentModel != null) {
                    setAnimControls(null);
                    currentModel.removeFromParent();
                }
                currentModelName = visRep.getAssetName();
//                Spatial model = models.get(currentModelName);
//                if (model == null) {
//
//                    setAnimControls(model);
//                    model = manager.loadModel(currentModelName);
//                    try {
//                        Spatial newmodel = TextureAtlas.makeAtlasBatch(model, manager, 1024);
//                        if (newmodel != null) {
//                            model = newmodel;
//                        }
//                    } catch (Exception e) {
//                        System.out.println("error batching model " + currentModelName + ": " + e);
//                    }
//                } else {
//                    model = model.clone(false);
//                }
//                models.put(currentModelName, model);
//                currentModel = model;
                currentModel = manager.loadModel(currentModelName);
                setAnimControls(currentModel);
                ((Node) spatial).attachChild(currentModel);
            }
        } else {
            //dispose ourselves if the entity has no VisualRepComponent anymore..
            setAnimControls(null);
            spatial.removeFromParent();
            entity.clearComponent(InSceneComponent.class);
            return false;
        }
        return true;
    }

    private boolean updateLocation(float tpf) {
        PositionComponent position = entity.getComponent(PositionComponent.class);
        MovementComponent movement = entity.getComponent(MovementComponent.class);
        if (movement != null && position != null) {
            spatial.setLocalTranslation(position.getLocation());
            spatial.setLocalRotation(position.getRotation());

            if (position.getLastUpdate() == lastLocationUpdate) {
                //TODO: interpolate
            }
        } else if (position != null) {
            spatial.setLocalTranslation(position.getLocation());
            spatial.setLocalRotation(position.getRotation());
        }
        return true;
    }

    private boolean updateAnimation() {
        MovementComponent movement = entity.getComponent(MovementComponent.class);
        if (movement != null && movement.getMovement().length() > 0) {
            setAnimation("walk");
        } else {
            setAnimation("idle");
        }
        return true;
    }

    private void setAnimation(String name) {
        if (animChannels != null) {
            for (Iterator<AnimChannel> it = animChannels.iterator(); it.hasNext();) {
                AnimChannel animChannel = it.next();
                if (animChannel.getAnimationName() == null || !animChannel.getAnimationName().equals(name)) {
                    animChannel.setAnim(name);
                    logger.log(Level.INFO, "Setting anim {0}", name);
                    if (animChannel.getControl().getAnim(name) != null) {
                    }
                }
            }
        }
    }

    private void setAnimControls(Spatial spatial) {
        if (spatial == null) {
            if (animControls != null) {
                for (Iterator<AnimControl> it = animControls.iterator(); it.hasNext();) {
                    AnimControl animControl = it.next();
                    animControl.clearChannels();
                }
            }
            animControls = null;
            animChannels = null;
            return;
        }
        SceneGraphVisitorAdapter visitor = new SceneGraphVisitorAdapter() {

            @Override
            public void visit(Geometry geom) {
                super.visit(geom);
                checkForAnimControl(geom);
            }

            @Override
            public void visit(Node geom) {
                super.visit(geom);
                checkForAnimControl(geom);
            }

            private void checkForAnimControl(Spatial geom) {
                AnimControl control = geom.getControl(AnimControl.class);
                if (control == null) {
                    return;
                }
                if (animControls == null) {
                    animControls = new LinkedList<AnimControl>();
                }
                if (animChannels == null) {
                    animChannels = new LinkedList<AnimChannel>();
                }
//                geom.removeControl(geom.getControl(SkeletonControl.class));
//                geom.removeControl(control);
                animControls.add(control);
                animChannels.add(control.createChannel());
            }
        };
        spatial.depthFirstTraversal(visitor);
    }

    @Override
    protected void controlRender(RenderManager rm, ViewPort vp) {
    }

    public Control cloneForSpatial(Spatial spatial) {
        throw new UnsupportedOperationException("Not supported");
    }
}
