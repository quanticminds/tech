/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jme3.powermonkey.timebased;

import com.jme3.powermonkey.Entities;
import com.jme3.powermonkey.Entity;
import com.jme3.powermonkey.components.ActionComponent;
import com.jme3.powermonkey.components.HumanComponent;
import com.jme3.powermonkey.components.SettlerComponent;
import com.jme3.powermonkey.components.SoldierComponent;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author normenhansen
 */
public class HumanAISystem implements TimeBasedSystem {

    public static enum State {

        Dumb,
        Knowing,
        Acting,
        Reacting
    }

    public void updateSystem(long time) {
        updateNature(time);
        updateSettlerActions(time);
        updateSoldierActions(time);
        performActions(time);
    }

    private void updateNature(long time) {
        List<Entity> humans = Entities.getEntities(HumanComponent.class);
        for (Iterator<Entity> it = humans.iterator(); it.hasNext();) {
            Entity human = it.next();
            HumanComponent humanComp = human.getComponent(HumanComponent.class);
            long timeSince = time - humanComp.getLastUpdate();
            if (timeSince > 10000) {
                human.setComponent(new HumanComponent(humanComp.getAge(), humanComp.getHunger() + 1, humanComp.getMoral(), time));
            }
        }
    }

    private void updateSettlerActions(long time) {
        List<Entity> humans = Entities.getEntities(SettlerComponent.class);
        for (Iterator<Entity> it = humans.iterator(); it.hasNext();) {
            Entity settler = it.next();
            ActionComponent action = settler.getComponent(ActionComponent.class);
            if (action != null) {
            } else {
            }
        }
    }

    private void updateSoldierActions(long time) {
        List<Entity> humans = Entities.getEntities(SoldierComponent.class);
        for (Iterator<Entity> it = humans.iterator(); it.hasNext();) {
            Entity soldier = it.next();
        }
    }

    private void performActions(long time) {
        List<Entity> humans = Entities.getEntities(ActionComponent.class);
        for (Iterator<Entity> it = humans.iterator(); it.hasNext();) {
            Entity entity = it.next();
        }
    }
}
