/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jme3.powermonkey.timebased;

import com.jme3.collision.CollisionResult;
import com.jme3.collision.CollisionResults;
import com.jme3.math.Quaternion;
import com.jme3.math.Ray;
import com.jme3.math.Vector3f;
import com.jme3.powermonkey.Entities;
import com.jme3.powermonkey.Entity;
import com.jme3.powermonkey.components.MovementComponent;
import com.jme3.powermonkey.components.NavigationTargetComponent;
import com.jme3.powermonkey.components.PositionComponent;
import com.jme3.scene.Spatial;
import java.util.Iterator;
import java.util.List;
import jme3tools.navmesh.NavMesh;
import jme3tools.navmesh.NavMeshPathfinder;

/**
 *
 * @author normenhansen
 */
public class NavigationSystem implements TimeBasedSystem {

    private Spatial terrain;
    private NavMesh navMesh;

    public NavigationSystem(Spatial terrain, NavMesh navMesh) {
        this.terrain = terrain;
        this.navMesh = navMesh;
    }

    public void updateSystem(long time) {
        updateMovement(time);
        updatePositions(time);
    }

    public void updateMovement(long time) {
        List<Entity> humans = Entities.getEntities(NavigationTargetComponent.class);
        for (Iterator<Entity> it = humans.iterator(); it.hasNext();) {
            Entity navigator = it.next();
            NavigationTargetComponent navTarget = navigator.getComponent(NavigationTargetComponent.class);
            PositionComponent position = navigator.getComponent(PositionComponent.class);
            if (position == null) {
                navigator.clearComponent(NavigationTargetComponent.class);
                continue;
            }
            Vector3f target = navTarget.getTarget();
            Vector3f targetVector = target.subtract(position.getLocation());
            if (targetVector.length() < 1) {
                navigator.clearComponent(MovementComponent.class);
                navigator.clearComponent(NavigationTargetComponent.class);
                continue;
            }
            float speed = 10;
            targetVector.normalizeLocal().multLocal(speed);
            navigator.setComponent(new MovementComponent(targetVector, Vector3f.ZERO));
        }
    }

    public void updatePositions(long time) {
        List<Entity> humans = Entities.getEntities(PositionComponent.class);
        for (Iterator<Entity> it = humans.iterator(); it.hasNext();) {
            Entity entity = it.next();
            PositionComponent position = entity.getComponent(PositionComponent.class);
            MovementComponent movement = entity.getComponent(MovementComponent.class);
            if(movement==null){
                entity.setComponent(new PositionComponent(position.getLocation(), position.getRotation(), time));
                continue;
            }
            long deltaTime = time - position.getLastUpdate();
            float tpf = (float) ((double) deltaTime / 1000.0d);
            Vector3f newPosition = getLocationOnTerrain(position.getLocation().add(movement.getMovement().mult(tpf)));
            Quaternion newRotation = new Quaternion(position.getRotation());
            newRotation.lookAt(movement.getMovement(), Vector3f.UNIT_Y);
            entity.setComponent(new PositionComponent(newPosition, newRotation, time));
        }
    }

    public Vector3f getLocationOnTerrain(Vector3f location) {
        CollisionResults results = new CollisionResults();
        Ray ray = new Ray(new Vector3f(location.x, 1000.0f, location.z), Vector3f.UNIT_Y.negate());
        terrain.collideWith(ray, results);
        CollisionResult result = results.getClosestCollision();
        if (result != null) {
            return result.getContactPoint();
        }
        return location;
    }

    public NavMeshPathfinder getNavMeshPathfinder() {
        if (navMesh != null) {
            return new NavMeshPathfinder(navMesh);
        }
        return null;
    }
}
