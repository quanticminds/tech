/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jme3.powermonkey.timebased;

/**
 *
 * @author normenhansen
 */
public interface TimeBasedSystem {

    public abstract void updateSystem(long time);
}
