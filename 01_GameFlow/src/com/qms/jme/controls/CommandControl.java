/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qms.jme.controls;

import com.jme3.export.InputCapsule;
import com.jme3.export.JmeExporter;
import com.jme3.export.JmeImporter;
import com.jme3.export.OutputCapsule;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;
import com.jme3.scene.control.Control;
import java.io.IOException;
import java.util.LinkedList;
import com.qms.jme.samples.commands.Command;

/**
 *
 * @author adriano
 */
public class CommandControl extends AbstractControl {
    //Any local variables should be encapsulated by getters/setters so they
    //appear in the SDK properties window and can be edited.
    //Right-click a local variable to encapsulate it with getters and setters.
    LinkedList<Command> commands;
    
    
    public void addCommand(Command command){}
    public void removeCommand(Command command){}
    public void clearCommands(){}
    
    
    @Override
    protected void controlUpdate(float tpf) {
//        for (Iterator<Command> it = commands.iterator(); it.hasNext();) {
//            Command command = it.next();
//            //do command and remove if returned true, else stop processing
//            Command.State commandState = command.doCommand(tpf);
//            switch (commandState) {
//                case Finished:
//                    command.setRunning(false);
//                    it.remove();
//                    break;
//                case Blocking:
//                    return;
//                case Continuing:
//                    break;
//            }
//        }
    }
    
    @Override
    protected void controlRender(RenderManager rm, ViewPort vp) {
        //Only needed for rendering-related operations,
        //not called when spatial is culled.
    }
    
    public Control cloneForSpatial(Spatial spatial) {
        CommandControl control = new CommandControl();
        //TODO: copy parameters to new Control
        control.setSpatial(spatial);
        return control;
    }
    
    @Override
    public void read(JmeImporter im) throws IOException {
        super.read(im);
        InputCapsule in = im.getCapsule(this);
        //TODO: load properties of this Control, e.g.
        //this.value = in.readFloat("name", defaultValue);
    }
    
    @Override
    public void write(JmeExporter ex) throws IOException {
        super.write(ex);
        OutputCapsule out = ex.getCapsule(this);
        //TODO: save properties of this Control, e.g.
        //out.write(this.value, "name", defaultValue);
    }
}
