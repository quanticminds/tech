/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qms.jme.controls;

import com.jme3.collision.CollisionResults;
import com.jme3.export.JmeExporter;
import com.jme3.export.JmeImporter;
import com.jme3.input.InputManager;
import com.jme3.input.KeyInput;
import com.jme3.input.MouseInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.AnalogListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.controls.MouseAxisTrigger;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.material.Material;
import com.jme3.math.Ray;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;
import com.jme3.scene.control.Control;
import java.io.IOException;

/**
 *
 * @author adriano
 */
public class UserInputControl extends AbstractControl implements ActionListener {

    private InputManager inputManager;
//    private ManualControl manualControl = null;
    private float moveX = 0;
    private float moveY = 0;
    private float moveZ = 0;
    private float steerX = 0;
    private float steerY = 0;
    private Camera camera;
    Node worldRootNode;

    public UserInputControl(InputManager inputManager, Camera camera,
            Node worldRootNode) {
        this.inputManager = inputManager;
        this.camera = camera;
        this.worldRootNode = worldRootNode;

        prepareInputManager();
    }

    private void prepareInputManager() {
//        inputManager.addMapping("pick target",
//                new MouseButtonTrigger(MouseInput.BUTTON_LEFT));
//        inputManager.addMapping("UserInput_Left_Arrow_Key", new KeyTrigger(KeyInput.KEY_LEFT));
//        inputManager.addMapping("UserInput_Right_Arrow_Key", new KeyTrigger(KeyInput.KEY_RIGHT));
//        inputManager.addMapping("UserInput_Space_Key", new KeyTrigger(KeyInput.KEY_SPACE));
//        inputManager.addMapping("UserInput_Enter_Key", new KeyTrigger(KeyInput.KEY_RETURN));
//        inputManager.addMapping("UserInput_Left_Mouse", new MouseButtonTrigger(MouseInput.BUTTON_LEFT));
//        inputManager.addMapping("UserInput_Mouse_Axis_X_Left", new MouseAxisTrigger(MouseInput.AXIS_X, true));
//        inputManager.addMapping("UserInput_Mouse_Axis_X_Right", new MouseAxisTrigger(MouseInput.AXIS_X, false));
//        inputManager.addMapping("UserInput_Mouse_Axis_Y_Up", new MouseAxisTrigger(MouseInput.AXIS_Y, true));
//        inputManager.addMapping("UserInput_Mouse_Axis_Y_Down", new MouseAxisTrigger(MouseInput.AXIS_Y, false));
//        inputManager.addListener(this,
//                "pick target" 
        //                "UserInput_Left_Key",
        //                "UserInput_Right_Key",
        //                "UserInput_Up_Key",
        //                "UserInput_Down_Key",
        //                "UserInput_Left_Arrow_Key",
        //                "UserInput_Right_Arrow_Key",
        //                "UserInput_Space_Key",
        //                "UserInput_Enter_Key" //,
        //                "UserInput_Left_Mouse",
        //                "UserInput_Mouse_Axis_X_Left",
        //                "UserInput_Mouse_Axis_X_Right",
        //                "UserInput_Mouse_Axis_Y_Up",
        //                "UserInput_Mouse_Axis_Y_Down"
//                );
    }

    public void onAction(String name, boolean isPressed, float tpf) {
    }

    public void onAnalog(String name, float value, float tpf) {
        if (name.equals("pick target")) {
            CollisionResults collisions = new CollisionResults();
            Vector2f cursorPosition = this.inputManager.getCursorPosition();
            Vector3f cursorWorldPosition =
                    this.camera.getWorldCoordinates(cursorPosition, 0f);
            Vector3f cursorDirection =
                    this.camera.getWorldCoordinates(cursorPosition, 1f)
                    .subtract(cursorWorldPosition).normalizeLocal();
            Ray clickableArea = new Ray(cursorWorldPosition, cursorDirection);
            this.worldRootNode.collideWith(clickableArea, collisions);

            if (collisions.size() > 0) {
                collisions.getClosestCollision()
                        .getGeometry();
            }
        }
    }

    @Override
    protected void controlUpdate(float tpf) {
        //TODO: add code that controls Spatial,
        //e.g. spatial.rotate(tpf,tpf,tpf);
    }

    @Override
    protected void controlRender(RenderManager rm, ViewPort vp) {
        //Only needed for rendering-related operations,
        //not called when spatial is culled.
    }

    public Control cloneForSpatial(Spatial spatial) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void read(JmeImporter im) throws IOException {
        super.read(im);
//        InputCapsule in = im.getCapsule(this);
        //TODO: load properties of this Control, e.g.
        //this.value = in.readFloat("name", defaultValue);
    }

    @Override
    public void write(JmeExporter ex) throws IOException {
        super.write(ex);
//        OutputCapsule out = ex.getCapsule(this);
        //TODO: save properties of this Control, e.g.
        //out.write(this.value, "name", defaultValue);
    }
}
