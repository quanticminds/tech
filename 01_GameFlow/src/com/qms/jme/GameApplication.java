/*
 * GameApplication.java
 *
 * Version:  0.1.0
 *
 * Authors:  
 *          Adriano Ribeiro <adriano.ribeiro at quanticmidns.com>
 *
 *********************
 *
 * Copyright (c) 2011, 2012  Quantic Minds Software Ltda.
 * All Rights Reserved.
 *
 *********************
 */
package com.qms.jme;

import com.jme3.app.SimpleApplication;
import com.jme3.font.BitmapText;
import com.jme3.input.FlyByCamera;
import com.jme3.niftygui.NiftyJmeDisplay;
import de.lessvoid.nifty.Nifty;

/**
 *
 * <code>GameApplication</code> provê o ambiente de execução para o jogo,
 * inicializa o sistema HUD, cria o gerenciador do mundo do jogo.
 * 
 * @author Adriano Ribeiro
 * 
 */
public class GameApplication extends SimpleApplication {

    private NiftyJmeDisplay niftyDisplay;
    private Nifty hudManager;
    private FlyByCamera camera;

    public Nifty getHUDManager(){
        return this.hudManager;
    }
    
  private void initCrossHairs() {
    guiNode.detachAllChildren();
    guiFont = assetManager.loadFont("Interface/Fonts/Default.fnt");
    BitmapText ch = new BitmapText(guiFont, false);
    ch.setSize(guiFont.getCharSet().getRenderedSize() * 2);
    ch.setText("+"); // crosshairs
    ch.setLocalTranslation( // center
      settings.getWidth() / 2 - guiFont.getCharSet().getRenderedSize() / 3 * 2,
      settings.getHeight() / 2 + ch.getLineHeight() / 2, 0);
    guiNode.attachChild(ch);
  }
    
    private void startHudManager() {

        this.niftyDisplay = new NiftyJmeDisplay(getAssetManager(),
                getInputManager(),
                getAudioRenderer(),
                getGuiViewPort());
              
        this.hudManager = niftyDisplay.getNifty();

        // all HUD game Screens
        this.hudManager.fromXmlWithoutStartScreen("Interface/hud.xml");

        // attach the hudManager display to the gui view port as a processor
        getGuiViewPort().addProcessor(niftyDisplay);

        // disable the fly cam
//        flyCam.setEnabled(false);
        
//        flyCam.setDragToRotate(true);
        getInputManager().setCursorVisible(true);
    }

    @Override
    public void simpleInitApp() {

        //start Nifty system to handle user interface
        startHudManager();
        
        /** A centred plus sign to help the player aim. */
        initCrossHairs();
    }
}
