/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qms.jme;

import com.jme3.app.SimpleApplication;
import com.jme3.asset.AssetManager;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.qms.jme.controls.MousePickUserInputControl;
import com.qms.jme.controls.UserInputControl;

/**
 *
 * @author adriano
 */
public class WorldManager{

    
    boolean isPaused;
    private GameApplication gameApp;
    private AssetManager assetManager;
    private Node rootNode;
    private Node mapNode;
    private Node playerNode;
//    private NavMesh
   
    public WorldManager(GameApplication app){
        this.gameApp = app;
        this.rootNode = this.gameApp.getRootNode();
        this.assetManager = this.gameApp.getAssetManager();
    }
    
    
    public void initialize() {
        
        this.isPaused = false;
    }

    
    public void update(float tpf) {
        
    }

    public void cleanup() {
        this.mapNode.detachAllChildren();
        this.rootNode.detachAllChildren();
    }    
    
    
    public boolean loadMap(String map){
       
        this.mapNode = (Node)this.assetManager.loadModel(map);
        
        return true;
    }
    
    public boolean loadModel(String model){
       
//        this.playerNode = (Node)this.assetManager.loadModel(model);
        
        return true;
    }
    public int attachMap(){
        return this.rootNode.attachChild(this.mapNode);
    }
    
    public int detachMap(){
        return this.rootNode.detachChild(this.mapNode);
    }
    
    // TODO: i need set where that player shoud appear
    // TODO: i need identify the player
    // TODO: i need link player with Spatial that is represent
    
    /**
     * Called when new player come to game.
     *
     */
    public void addPlayer(){
        this.playerNode = 
                (Node)this.assetManager.loadModel("Models/Oto/Oto.j3o");
        
        MousePickUserInputControl uic = 
                new MousePickUserInputControl(this.gameApp.getInputManager(),
                this.gameApp.getCamera(),
                this.mapNode);
        
        this.playerNode.addControl(uic);
        
        this.playerNode.setLocalTranslation(new Vector3f(-30f,-9.8f,0f));
        this.playerNode.setLocalScale(0.5f);
        
//        this.playerACT = this.playerNode.getControl(AnimControl.class);
//        this.playerACT.addListener(this);
//        this.playerACH = this.playerACT.createChannel();
//        this.playerACH.setAnim("stand");
//        
//        CapsuleCollisionShape capsuleShape = new CapsuleCollisionShape(1.5f, 2f, 1);
//        this.playerCCT = new CharacterControl(capsuleShape, 0.5f);
//        this.playerCCT.setJumpSpeed(20);
//        this.playerCCT.setFallSpeed(30);
//        this.playerCCT.setGravity(30);
//        this.playerCCT.setPhysicsLocation(new Vector3f(-30f,-9.8f,0f));
//        
//        this.playerNode.addControl(this.playerCCT);
//        
//        this.rootNode.attachChild(this.playerNode);
//        this.physicsState.getPhysicsSpace().add(this.playerCCT);
//
//        chaseCam = new ChaseCamera(this.simpleApp.getCamera(), 
//                this.playerNode, inputManager);
    }

}
