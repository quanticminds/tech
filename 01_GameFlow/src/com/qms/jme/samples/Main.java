/*
 * Main.java
 *
 * Version:  0.1.0
 *
 * Authors:  
 *          Adriano Ribeiro <adriano.ribeiro at quanticmidns.com>
 *
 *********************
 *
 * Copyright (c) 2011, 2012  Quantic Minds Software Ltda.
 * All Rights Reserved.
 *
 *********************
 */
package com.qms.jme.samples;

import com.jme3.renderer.RenderManager;
import com.jme3.system.AppSettings;
import com.qms.jme.GameApplication;
import com.qms.jme.WorldManager;
import com.qms.jme.samples.screens.InGameScreen;
import java.util.concurrent.Callable;

/**
 * <code>AppSettings</code> provides a store of configuration to be used by the
 * application. <p> By default only the {@link JmeContext context} uses the
 * configuration, however the user may set and retrieve the settings as well.
 * The settings can be stored either in the Java preferences (using {@link #save(java.lang.String)
 * } or a .properties file (using {@link #save(java.io.OutputStream) }.
 *
 * @author Kirill Vainer
 */
public class Main extends GameApplication {

    public static void main(String[] args) {
        Main app = new Main();

        AppSettings setting = new AppSettings(true);
        setting.setResolution(1280, 800);

        app.setSettings(setting);
        app.setPauseOnLostFocus(false);
        app.setShowSettings(false);
        app.start();
    }

    @Override
    public void simpleInitApp() {
        super.simpleInitApp();

//        enqueue(new Callable<Void>() {
//            public Void call() throws Exception {
//                hudManager.gotoScreen("loading");
//                return null;
//            }
//        });
        getStateManager().attach(new InGameScreen(this));
        
        enqueue(new Callable<Void>() {
            public Void call() throws Exception {
                getStateManager().getState(InGameScreen.class).newGame();
                return null;
            }
        });
    }

    @Override
    public void simpleUpdate(float tpf) {
        //TODO: add update code
    }
}
