/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qms.jme.samples.commands;

/**
 *
 * @author adriano
 */
public interface Command {
    
    enum State{
        Success,
        Running,
        Waiting,
        Failed        
    }
    
    
    public String getName();
    
    public State executeCommand(float tpf);
    
    public Command initialize();
    
    public int getPriority();
    
    public void setPriority(int priority);
    
    public boolean isRunning();
    
    public void setRunning(boolean running);
    
    public void cancel();
    
    
}
