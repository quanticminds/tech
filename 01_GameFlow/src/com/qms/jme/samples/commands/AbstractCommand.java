/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qms.jme.samples.commands;

import com.jme3.scene.Spatial;
import com.qms.jme.controls.CommandControl;

/**
 *
 * @author adriano
 */
public abstract class AbstractCommand implements Command{

    String name; 
    int priority = -1;
    boolean isrunning = false;
    
    protected Spatial entity;
    
    public abstract State executeCommand(float tpf);
    
    public String getName() {
        return this.name;
    }

    public Command initialize() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public int getPriority() {
        return this.priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public boolean isRunning() {
        return this.isrunning;
    }

    public void setRunning(boolean running) {
        this.isrunning = running;
    }

    public void cancel() {
        entity.getControl(CommandControl.class).removeCommand(this);
    }
    
}
