/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qms.jme.samples.screens;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;
import java.beans.Encoder;
import com.qms.jme.WorldManager;

/**
 *
 * @author adriano
 */
public class LoadingScreen extends AbstractAppState implements ScreenController{


    private Nifty hudManager;
    private AppStateManager stateManager;
    private SimpleApplication simpleApp;
       
    public LoadingScreen(Nifty hudManager){
        this.hudManager = hudManager;
    }
    
    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);        

        this.simpleApp = (SimpleApplication)app;
        this.stateManager = this.simpleApp.getStateManager();
        this.hudManager.registerScreenController(this);
    }

    @Override
    public void update(float tpf) {

    }

    @Override
    public void stateAttached(AppStateManager stateManager) {
        super.stateAttached(stateManager);
        this.hudManager.gotoScreen("loading");
    }
    
    public void bind(Nifty nifty, Screen screen) {
        System.out.println("current screen binded: " + screen.getScreenId());
    }

    public void onStartScreen() {
        System.out.println("screen started");
    }

    public void onEndScreen() {
        System.out.println("screen ended");
    }

}
