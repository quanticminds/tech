/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qms.jme.samples.screens;

import com.jme3.app.Application;
import com.jme3.app.state.AppStateManager;
import com.qms.jme.GameApplication;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.screen.Screen;
import com.qms.jme.WorldManager;
import com.qms.jme.screens.GameScreen;

/**
 *
 * @author adriano
 */
public class InGameScreen extends GameScreen {
    private WorldManager world;
    
    public InGameScreen(GameApplication app){
        this.world = new WorldManager(app);
        this.world.initialize();
    }
    
    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
    }

    
    @Override
    public void update(float tpf) {
        
    }

    @Override
    public void cleanup() {
        super.cleanup();
    }

    public void bind(Nifty nifty, Screen screen) {
        System.out.println("current screen binded: " + screen.getScreenId());
    }

    public void onStartScreen() {
        System.out.println("screen started");
    }

    public void onEndScreen() {
        System.out.println("screen ended");
    }

    
    public synchronized boolean newGame(){
        boolean gameStarted = false;
        
        this.world.loadMap("Scenes/town/city.j3o");
        
        this.world.attachMap();
        
        this.world.addPlayer();
//        this.worldManager.addPlayer("Scenes/town/city.j3o");

//        this.hudManager.gotoScreen("ingame");
        
        return gameStarted;
    }
    
    
    public synchronized boolean saveGame(){
        return false;
    }
}
