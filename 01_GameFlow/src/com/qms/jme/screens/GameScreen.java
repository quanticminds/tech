/*
 * GameApp.java
 *
 * Version:  0.1.0
 *
 * Authors:  
 *          Adriano Ribeiro <adriano.ribeiro at quanticmidns.com>
 *
 *********************
 *
 * Copyright (c) 2011, 2012  Quantic Minds Software Ltda.
 * All Rights Reserved.
 *
 *********************
 */

package com.qms.jme.screens;

import com.jme3.app.Application;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.qms.jme.GameApplication;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.screen.ScreenController;

/**
 *
 * @author adriano
 */
public abstract class GameScreen extends AbstractAppState implements ScreenController{

    protected Nifty hudManager;
    protected GameApplication gameApp;
    protected AppStateManager stateManager;

           
    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);        

        this.gameApp = (GameApplication)app;
        this.hudManager = this.gameApp.getHUDManager();
        this.hudManager.registerScreenController(this);
        this.stateManager = this.gameApp.getStateManager();
    }
}
