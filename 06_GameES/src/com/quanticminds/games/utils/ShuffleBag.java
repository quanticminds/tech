/* 
 * The MIT License
 *
 * Copyright 2013 adriano.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.quanticminds.games.utils;

import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 *
 * @author adriano
 * http://gamedev.tutsplus.com/tutorials/implementation/shuffle-bags-making-random-feel-more-random/
 * 
 */
public class ShuffleBag<E> {
    private CopyOnWriteArrayList<E> elements;
    private E curElement;
    private int curIndex = -1;
    private Random random;
    
    public ShuffleBag(){
        this.elements = new CopyOnWriteArrayList();
    }
        
    
    public void add(E element){
        this.add(element, 1);
    }
    
    public void add(E element, int frequency){
        for(int i = 0; i < frequency; i++){
            this.elements.add(element);
        }
        
        this.curIndex = this.elements.size()-1;
    }
    
    public E getNext(){
        
        if(curIndex < 1){
            this.curIndex = this.elements.size() - 1;
            this.curElement = this.elements.get(0);
            return this.curElement;
        }
        
        int idx = this.random.nextInt(curIndex);
        this.curElement = this.elements.get(idx);
        this.elements.set(idx, this.elements.get(this.curIndex));
        this.elements.set(curIndex,this.curElement);
        this.curIndex--;
        
        return this.curElement;        
    }
}
