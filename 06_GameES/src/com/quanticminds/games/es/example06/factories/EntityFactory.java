/* 
 * The MIT License
 *
 * Copyright 2013 adriano.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.quanticminds.games.es.example06.factories;

import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.quanticminds.games.es.example06.components.Input;
import com.quanticminds.games.es.example06.components.PartyMember;
import com.quanticminds.games.es.example06.components.Position;
import com.quanticminds.games.es.example06.components.Velocity;
import com.quanticminds.games.es.example06.components.Visual;
import com.quanticminds.games.example06.Constants;
import com.simsilica.es.EntityData;
import com.simsilica.es.EntityId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 *
 * @author adriano
 */
public class EntityFactory {
    private static Logger _log = LoggerFactory.getLogger( EntityFactory.class );
    private EntityData entityData;

    public EntityFactory(EntityData entityData) {
        this.entityData = entityData;
    }
        
    
    /**
     *
     * @param db
     * @param name
     * @param position
     * @param facing
     * @return
     */
    public EntityId createPartyMember(String name, 
                    Vector3f position, Quaternion facing,
                    boolean leader) {
        EntityId eid = entityData.createEntity();
        entityData.setComponents(eid, 
                new PartyMember(leader),
                new Visual(Constants.Models.SinbadMesh,name, 
                            Constants.ModelTypes.Asset, false),
//                new Input(Vector3f.ZERO, 0f, Constants.Input.Idle),
                new Velocity(1.5f, 1.5f, 2f, 1.0f, 0f, Vector3f.UNIT_Z),
                new Position(position, facing, false));
        
        _log.info("party member entity created {}", eid);
        
        return eid;
    }

}
