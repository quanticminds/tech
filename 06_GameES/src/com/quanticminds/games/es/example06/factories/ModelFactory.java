/* 
 * The MIT License
 *
 * Copyright 2013 adriano.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.quanticminds.games.es.example06.factories;

import com.jme3.app.SimpleApplication;
import com.jme3.asset.AssetManager;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.collision.shapes.CapsuleCollisionShape;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.scene.Spatial;
import com.quanticminds.games.es.example06.components.PartyMember;
import com.quanticminds.games.es.example06.components.Position;
import com.quanticminds.games.es.example06.components.Visual;
import com.simsilica.es.Entity;


/**
 *
 * @author adriano
 */
public class ModelFactory {

    private SimpleApplication _simpleApp;
    private AssetManager _assets;

    public ModelFactory(SimpleApplication simpleApp){
        _simpleApp = simpleApp;
        _assets = simpleApp.getAssetManager();
    }
    
    /**
     *
     * @param db
     * @param name
     * @param position
     * @param rotation
     * @return
     */
    public Spatial createPlayer(Entity entity) {

        PartyMember pm = entity.get(PartyMember.class);
        Visual vs = entity.get(Visual.class);
        Position ps = entity.get(Position.class);
        
        Spatial spatial = _assets.loadModel(vs.getAsset());
        spatial.setName(vs.getName());
        spatial.setLocalRotation(ps.getFacing());
        spatial.setLocalTranslation(ps.getLocation());
//        RigidBodyControl rb = new RigidBodyControl(new CapsuleCollisionShape(2f, 5.5f), 1f);
//        RigidBodyControl rb = new RigidBodyControl(1f);
//        spatial.addControl(rb);
//        rb.setKinematic(true);
//        _simpleApp.getStateManager().getState(BulletAppState.class)
//                .getPhysicsSpace().add(rb);
        
        return spatial;
    }
}
