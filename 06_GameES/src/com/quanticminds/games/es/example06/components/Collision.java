/* 
 * The MIT License
 *
 * Copyright 2013 adriano.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.quanticminds.games.es.example06.components;

import com.simsilica.es.PersistentComponent;

/**
 *
 * @author adriano
 */
public class Collision implements PersistentComponent{
   
    //capsule shape
    int collisionGroup;
    int[] collisionWith;
    float height;
    float radius;
    float mass;
    float friction;
   

    /**
     *
     * @param collisionGroup
     * @param collisionWith
     * @param height
     * @param radius
     * @param mass
     * @param friction
     */
    public Collision(int collisionGroup, int[] collisionWith, float height, 
                            float radius, float mass, float friction) {
        this.collisionGroup = collisionGroup;
        this.collisionWith = collisionWith;
        this.height = height;
        this.radius = radius;
        this.mass = mass;
        this.friction = friction;
    }

    /**
     *
     * @return
     */
    public int getCollisionGroup() {
        return collisionGroup;
    }

    /**
     *
     * @return
     */
    public int[] getCollisionWith() {
        return collisionWith;
    }

    /**
     *
     * @return
     */
    public float getHeight() {
        return height;
    }

    /**
     *
     * @return
     */
    public float getRadius() {
        return radius;
    }
    
    /**
     *
     * @return
     */
    public float getMass() {
        return mass;
    }

    /**
     *
     * @return
     */
    public float getFriction() {
        return friction;
    }
}
