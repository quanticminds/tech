/* 
 * The MIT License
 *
 * Copyright 2013 adriano.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.quanticminds.games.es.example06.components;

import com.jme3.math.Vector3f;
import com.simsilica.es.PersistentComponent;

/**
 *
 * @author adriano.ribeiro
 */
public class Velocity implements PersistentComponent{

    float strength = 0f;
    
    float speed = 1.5f; // worldUnits/second
    float maxSpeed = 1.5f; // worldUnits/second
    float maxTurnForce = 2f; // max steering force per second (perpendicular to velocity)
                            // if speed is 1 and turn force is 1, then it will turn 45 degrees in a second
    float mass = 1.0f; // the higher, the slower it turns

    Vector3f initialVelocity;
    
    public Velocity(float speed, float maxSpeed, float maxTurnForce, 
                    float mass, float strength, Vector3f initialVelocity){
        this.mass = mass;
        this.maxSpeed = maxSpeed;
        this.maxTurnForce = maxTurnForce;
        this.speed = speed;
        this.strength = strength;
        this.initialVelocity = initialVelocity;
    }

    public float getMass() {
        return mass;
    }

    public float getMaxSpeed() {
        return maxSpeed;
    }

    public float getMaxTurnForce() {
        return maxTurnForce;
    }

    public float getSpeed() {
        return speed;
    }

    public float getStrength() {
        return strength;
    }
}
