/* 
 * The MIT License
 *
 * Copyright 2013 adriano.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.quanticminds.games.es.example06.appstates;

//import com.jme3.ai.bt.Behaviour;
//import com.jme3.ai.bt.MultiBehaviourController;
import com.jme3.app.Application;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.simsilica.es.EntityData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author adriano
 */
public class CommandQueueAppState extends AbstractAppState {
    static Logger log = LoggerFactory.getLogger(CommandQueueAppState.class);
    
//    private Behaviour _commandRoot;
            
    
//    public MultiBehaviourController getCommandQueue() {
//        return (MultiBehaviourController)this._commandRoot.getController();
//    }
    
    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
        EntityData ed = app.getStateManager()
                .getState(EntityDataAppState.class).getEntityData();

        //i need pass root world data context here, all childreen can 
        // get and run queries on EntityData to get just and EntitySet 
        // of their interest
//        _commandRoot = new Parallel(null);
    }

    @Override
    public void update(float tpf) {
        super.update(tpf);
        
//        _commandRoot.doExecute();
    }

    
    
    @Override
    public void cleanup() {
        super.cleanup();
//        this._commandRoot.getController().behaviourFinish();
//        this._commandRoot = null; // cannot be reused
    }
}
