/* 
 * The MIT License
 *
 * Copyright 2013 adriano.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.quanticminds.games.es.example06.appstates;

import com.jme3.ai.navmesh.NavMesh;
import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.scene.Node;
import com.quanticminds.games.example06.managers.GameLogicManager;


/**
 *
 * @author adriano
 */
public class LoadingAppState extends AbstractAppState{
   
    boolean _finished = false;
    Node _worldRootNode;
    NavMesh _navMesh;
    SimpleApplication _simpleApp;
    GameLogicManager _gameLogic;

            
    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
        _simpleApp = (SimpleApplication)app;

//        _simpleApp.enqueue(new Callable<Void>(){
//            public Void call() throws Exception{
//        BulletAppState physics = new BulletAppState();
//        physics.setDebugEnabled(true);
//        getStateManager().attach(physics);
//        _simpleApp.getStateManager().attach(new BulletAppState());

        _simpleApp.getStateManager().attach(new WorldMapAppState());
        _simpleApp.getStateManager().attach(new EntityDisplayAppState());
        _simpleApp.getStateManager().attach(new PlayerControlAppState());
        
//                return null;
//            }
//        });
        
        setEnabled(false);
            
//        _simpleApp.enqueue(new Callable<Void>(){
//            public Void call() throws Exception{
//
////                Node node = (Node)_simpleApp.getAssetManager()
////                    .loadModel(Constants.Models.SinbadFile);
////                node.setLocalTranslation(new Vector3f(-20, -7, 10));
////                
////                _gameLogic.getWorldRootNode().attachChild(node);
//                
//                _gameLogic.getEntityfactory()
//                        .createPartyMember("Player 01", 
//                                        new Vector3f(-20, -7.2f, 10)
//                                        , Quaternion.ZERO
//                                        ,true);
//                
//                return null;
//            }
//        });
    }
    
    @Override
    public void update(float tpf) {
    }
}
