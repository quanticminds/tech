/* 
 * The MIT License
 *
 * Copyright 2013 adriano.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.quanticminds.games.es.example06.appstates;

import com.quanticminds.games.es.example06.systems.*;
import com.jme3.animation.AnimControl;
import com.jme3.animation.LoopMode;
import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.font.BitmapFont;
import com.jme3.font.BitmapText;
import com.jme3.math.Vector3f;
import com.jme3.scene.Spatial;
import com.quanticminds.games.es.example06.components.Input;
import com.quanticminds.games.es.example06.components.Position;
import com.quanticminds.games.es.example06.components.Visual;
import com.quanticminds.games.es.example06.factories.ModelFactory;
import com.quanticminds.games.example06.Constants;
import com.simsilica.es.Entity;
import com.simsilica.es.EntityId;
import com.simsilica.es.EntitySet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 *
 * @author adriano
 */
public class EntityDisplayAppState extends AbstractAppState{
    
    private static Logger _log = LoggerFactory.getLogger( EntityDisplayAppState.class );
    private EntityDataAppState _entityData;
    private WorldMapAppState _world;
    private EntitySet _entitySet;
    private ModelFactory _modelsfactory;
    private Map<EntityId, Spatial> _spatials = new HashMap<EntityId, Spatial>();
    private Map<EntityId, AnimControl> _animations = new HashMap<EntityId, AnimControl>();
    private Map<EntityId, String> _animationState = new HashMap<EntityId, String>();
    private SimpleApplication _simpleApp;
          
    
    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
        _simpleApp = (SimpleApplication)app;
       _entityData = _simpleApp.getStateManager()
               .getState(EntityDataAppState.class);
        _world = _simpleApp.getStateManager()
               .getState(WorldMapAppState.class);
        _entitySet = _entityData.getEntityData()
                .getEntities(Position.class, Visual.class);
        _modelsfactory = new ModelFactory(_simpleApp);
        
        initCrossHairs();
        
        _log.info("Display system started.");
    }
    
    @Override
    public void cleanup() {
        super.cleanup();
    
        _spatials.clear();
        _entitySet.release();
    }

    @Override
    public void update(float tpf) {
        if( _entitySet.applyChanges() ) {
            removeModels(_entitySet.getRemovedEntities());
            addedModels(_entitySet.getAddedEntities());
            updateModels(_entitySet.getChangedEntities());
            
//            systemUpdate(tpf);
        }
    }
    
    protected void systemUpdate(float tpf) {
//        _log.info("system udpate.");
    }
    
    protected void removeModels(Set<Entity> set){
        for( Entity e : set ) {
            Spatial s = _spatials.remove(e.getId());
            if( s == null ) {
                _log.error("Model not found for removed entity:" + e);
                continue;
            }

            s.removeFromParent();
            Visual vc = e.get(Visual.class);
            _log.info("Model removed {}.", vc.getName());
        }        
    }
    
    protected void addedModels(Set<Entity> set){
        for( Entity e : set ) {
            // See if we already have one
            if(_spatials.containsKey(e.getId())) {
                _log.error("Model already exists for added entity:" + e);
                continue;
            }

            final Spatial s = _modelsfactory.createPlayer(e);
            _spatials.put(e.getId(), s);
            updateModelSpatial(e, s);
            Visual vc = e.get(Visual.class);
            e.set(new Visual(vc.getAsset(), vc.getName(), vc.getType(), true));
            _log.info("Model added {}.",vc.getName());                    

            _simpleApp.enqueue(new Callable<Void>(){
                public Void call() throws Exception{

                    _world.getWorldRootNode().attachChild(s);

                    return null;
                }
            });
        }        
    }
    
    protected void updateModels(Set<Entity> set){
        for( Entity e : set ) {
            Spatial s = _spatials.get(e.getId());
            if( s == null ) {
                _log.error("Model not found for updated entity: " + e);
                continue;
            }

            updateModelSpatial(e, s);
        }        
    }    
    
    private void updateModelSpatial( Entity e, Spatial s ) {
        Position p = e.get(Position.class);
        
        AnimControl ac = _animations.get(e.getId());

        if(ac == null){
            ac = s.getControl(AnimControl.class);
            _animations.put(e.getId(), ac);
        }

        if(ac.getNumChannels() == 0){
            ac.createChannel();
            ac.createChannel();
        }
        
        String animation = _animationState.get(e.getId());
          
        if((animation != null) && (p.getLocation().distance(s.getLocalTranslation()) > 0) 
                && !animation.equals(Constants.Input.Run)){
            ac.getChannel(0).setAnim("RunTop",0.15f);
            ac.getChannel(0).setLoopMode(LoopMode.Loop);
            ac.getChannel(1).setAnim("RunBase",0.15f);
            ac.getChannel(1).setLoopMode(LoopMode.Loop);
            _animationState.put(e.getId(),Constants.Input.Run);
//            _log.info("model is now {} position {}  location {}", animation, p.getLocation(), s.getLocalTranslation());
            
        }else if((animation == null) || !animation.equals(Constants.Input.Idle)){
            ac.getChannel(0).setAnim("IdleTop",0.15f);
            ac.getChannel(0).setLoopMode(LoopMode.Loop);            
            ac.getChannel(1).setAnim("IdleBase",0.15f);
            ac.getChannel(1).setLoopMode(LoopMode.Loop);
            _animationState.put(e.getId(),Constants.Input.Idle);
//            _log.info("model is now {}", animation);
        }
        
//        _log.info("model is now {}", _animationState.get(e.getId()));

        
        s.setLocalTranslation(p.getLocation());
        s.setLocalRotation(p.getFacing());
//       _log.info("model position global {} position local {} facing to {}. ", s.getWorldTranslation(), p.getLocation(), p.getFacing());
    }
    
      /** A centred plus sign to help the player aim. */
  protected void initCrossHairs() {
    _simpleApp.enqueue(new Callable<Void>(){
            public Void call() throws Exception{
                BitmapFont guiFont = _simpleApp.getAssetManager().loadFont("Interface/Fonts/Default.fnt");
                BitmapText ch = new BitmapText(guiFont, false);
                ch.setSize(guiFont.getCharSet().getRenderedSize() * 2);
                ch.setText("+"); // crosshairs
                ch.setLocalTranslation( // center
                 _simpleApp.getCamera().getWidth()/ 2 - ch.getLineWidth()/2, _simpleApp.getCamera().getHeight() / 2 + ch.getLineHeight()/2, 0);
                _simpleApp.getGuiNode().attachChild(ch);
                return null;
            }
        });
  }
}
