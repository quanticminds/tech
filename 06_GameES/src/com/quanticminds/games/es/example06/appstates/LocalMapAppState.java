/* 
 * The MIT License
 *
 * Copyright 2013 adriano.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.quanticminds.games.es.example06.appstates;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.AnalogListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.scene.Node;
import com.quanticminds.games.example06.Constants;
import com.quanticminds.games.example06.managers.GameLogicManager;
import java.util.concurrent.Callable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 *
 * @author adriano
 */
public class LocalMapAppState extends AbstractAppState 
                                implements AnalogListener {
       
    private GameLogicManager _gameLogic;
    private SimpleApplication _simpleApp;
    private Node _worldRootNode;
    private static Logger _log = 
            LoggerFactory.getLogger( LocalMapAppState.class );
    
    public LocalMapAppState(GameLogicManager gameLogic){
        _gameLogic = gameLogic;
    }
            
    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
        
        _simpleApp = (SimpleApplication)app;
        _worldRootNode = _simpleApp.getRootNode();
    }
    
    @Override
    public void update(float tpf) {

        
    }

    public void onAnalog(String name, float value, float tpf) {
        
        if(name.equals("Menu")){
//            this._logicManager.showMenu();
        }
    }
    
}
