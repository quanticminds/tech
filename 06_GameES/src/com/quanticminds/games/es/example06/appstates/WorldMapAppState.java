/* 
 * The MIT License
 *
 * Copyright 2013 adriano.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.quanticminds.games.es.example06.appstates;

import com.jme3.ai.navmesh.NavMesh;
import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.quanticminds.games.example06.Constants;
import com.quanticminds.games.example06.managers.GameLogicManager;
import java.util.concurrent.Callable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 *
 * @author adriano
 */
public class WorldMapAppState extends AbstractAppState {
    private static Logger _log = LoggerFactory.getLogger( WorldMapAppState.class );
    
//    BulletAppState _physicsState;
    Node _worldRootNode;
    NavMesh _navMesh;
    SimpleApplication _simpleApp;
    GameLogicManager _gameLogic;
    EntityDataAppState _entityData;
    
    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
        _simpleApp = (SimpleApplication)app;

        _entityData = _simpleApp.getStateManager().getState(EntityDataAppState.class);
        
        _worldRootNode = (Node)_simpleApp.getAssetManager()
                .loadModel(Constants.Maps.TownFile);
//            _gameLogic.setWorldRootNode(_worldRootNode);
//            _gameLogic.setFloorNode(_navMesh);
            _simpleApp.enqueue(new Callable<Void>(){
                    public Void call() throws Exception{

                    _simpleApp.getRootNode().attachChild(_worldRootNode);
                    _simpleApp.getFlyByCamera().setMoveSpeed(60);
                    
                    return null;
                }
            });
                    
            _simpleApp.enqueue(new Callable<Void>(){
                    public Void call() throws Exception{

                     _entityData.getEntityFactory()
                        .createPartyMember("Player 01", 
                                        new Vector3f(-20, -7.2f, 10)
                                        , Quaternion.ZERO
                                        ,true);
                        return null;
                    }
                });
    
        _log.info("world map started.");
    }
    
    @Override
    public void update(float tpf) {
                
//        _log.info("running {}", tpf);
    }
    
    @Override
    public void cleanup() {
        _worldRootNode.removeFromParent();
    }
    
    public Node getWorldRootNode(){
        return _worldRootNode;
    }
}
