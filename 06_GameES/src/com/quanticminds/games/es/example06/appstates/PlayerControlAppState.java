/* 
 * The MIT License
 *
 * Copyright 2013 adriano.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.quanticminds.games.es.example06.appstates;

import com.quanticminds.games.es.example06.systems.*;
import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.collision.CollisionResult;
import com.jme3.collision.CollisionResults;
import com.jme3.math.Ray;
import com.jme3.math.Vector3f;
import com.quanticminds.games.es.example06.components.Input;
import com.quanticminds.games.es.example06.components.PartyMember;
import com.quanticminds.games.example06.Constants;
import com.quanticminds.games.example06.LeaderFunctions;
import com.quanticminds.games.example06.managers.GameLogicManager;
import com.simsilica.es.Entity;
import com.simsilica.es.EntitySet;
import com.simsilica.es.Filters;
import com.simsilica.lemur.GuiGlobals;
import com.simsilica.lemur.input.AnalogFunctionListener;
import com.simsilica.lemur.input.FunctionId;
import com.simsilica.lemur.input.InputMapper;
import com.simsilica.lemur.input.InputState;
import com.simsilica.lemur.input.StateFunctionListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author adriano
 */
public class PlayerControlAppState extends AbstractAppState implements AnalogFunctionListener, StateFunctionListener  {
    private final Logger _log = LoggerFactory.getLogger(PlayerInputSystem.class);
    private GameLogicManager _gameLogic;
    private EntitySet _entitySet;
    private Vector3f targetPosition = Vector3f.ZERO;
    private Vector3f viewDirection = new Vector3f(1, 0, 0);
    private SimpleApplication _simpleApp;


    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
        _simpleApp = (SimpleApplication)app;
        InputMapper inputMapper = GuiGlobals.getInstance().getInputMapper();
        inputMapper.addAnalogListener(this, LeaderFunctions.F_GOTO);
//        inputMapper.addStateListener(this, LeaderFunctions.F_GOTO);
        
        _simpleApp.getCamera().setLocation(new Vector3f(40f,15f,60f));
        
        _simpleApp.getCamera().lookAt(new Vector3f(-20, -7.2f, 10), Vector3f.UNIT_Y);


            
        _log.info("started");
    }

    @Override
    public void update(float tpf) {

        if(isEnabled()){
//            _log.info("running {}", tpf);
        }
    }

    @Override
    public void cleanup() {
        InputMapper inputMapper = GuiGlobals.getInstance().getInputMapper();
        inputMapper.removeAnalogListener(this,LeaderFunctions.F_GOTO);
    }

    @Override
    public void stateAttached(AppStateManager stateManager) {
        InputMapper inputMapper = GuiGlobals.getInstance().getInputMapper();
        inputMapper.activateGroup(LeaderFunctions.GROUP);
    }

    @Override
    public void stateDetached(AppStateManager stateManager) {
        InputMapper inputMapper = GuiGlobals.getInstance().getInputMapper();
        inputMapper.deactivateGroup(LeaderFunctions.GROUP);
    }
    
    public void valueActive(FunctionId func, double value, double tpf) {
        
    }

    public void valueChanged(FunctionId func, InputState value, double tpf) {
        if(func.equals(LeaderFunctions.F_GOTO)){
            // 1. Reset results list.
            CollisionResults results = new CollisionResults();
            // 2. Aim the ray from cam loc to cam direction.
            Ray ray = new Ray(_simpleApp.getCamera().getLocation(), 
                                _simpleApp.getCamera().getDirection());
            // 3. Collect intersections between Ray and Shootables in results list.
            _gameLogic.getWorldRootNode().collideWith(ray, results);
            // 4. Print the results
//            _log.info("----- Collisions? {} -----", results.size());
//            for (int i = 0; i < results.size(); i++) {
//              // For each hit, we know distance, impact point, name of geometry.
//              float dist = results.getCollision(i).getDistance();
//              Vector3f pt = results.getCollision(i).getContactPoint();
//              String hit = results.getCollision(i).getGeometry().getName();
//              _log.info("* Collision # {}", i);
//              _log.info("  You shot {} at {}, {} wu away.", hit, pt, dist);
//            }
            // 5. Use the results (we mark the hit object)
            if (results.size() > 0) {
              // The closest collision point is what was truly hit:
                CollisionResult closest = results.getClosestCollision();
                targetPosition.set(closest.getContactPoint());
            }            
            
            _entitySet = _gameLogic.getEntityData()
                    .getEntities(Filters.fieldEquals(PartyMember.class, "leader",true));

            for(Entity e : _entitySet){
                if (!e.get(PartyMember.class).isLeader()) {
                    continue;
                }

                e.set(new Input(targetPosition, 0.18f, Constants.Input.Run));
            }
            
            _log.info("move player");
        }
    }
}
