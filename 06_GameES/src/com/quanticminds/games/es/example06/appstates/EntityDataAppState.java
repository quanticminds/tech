/* 
 * The MIT License
 *
 * Copyright 2013 adriano.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.quanticminds.games.es.example06.appstates;

import com.jme3.app.Application;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.quanticminds.games.es.example06.factories.EntityFactory;
import com.simsilica.es.EntityData;
import com.simsilica.es.base.DefaultEntityData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author adriano
 */
public class EntityDataAppState extends AbstractAppState {
    final Logger _log = LoggerFactory.getLogger(EntityDataAppState.class);
    
    private EntityData _entityData;
    private EntityFactory _entityFactory;

    public EntityDataAppState() {
        this(new DefaultEntityData());
    }

    public EntityDataAppState(EntityData entityData) {
        this._entityData = entityData;
    }

    public EntityData getEntityData() {
        return this._entityData;
    }
    
    public EntityFactory getEntityFactory(){
        return _entityFactory;
    }
    
    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
        
        _entityFactory = new EntityFactory(_entityData);
                
        _log.info("starting");
    }
    
    @Override
    public void cleanup() {
        super.cleanup();
        _entityData.close();
    }
}
