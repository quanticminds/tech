/* 
 * The MIT License
 *
 * Copyright 2013 adriano.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.quanticminds.games.es.example06.systems;

import com.jme3.app.Application;
import com.jme3.app.state.AppStateManager;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.util.TempVars;
import com.quanticminds.games.es.example06.components.Input;
import com.quanticminds.games.es.example06.components.Position;
import com.quanticminds.games.es.example06.components.Velocity;
import com.quanticminds.games.example06.managers.GameLogicManager;
import com.simsilica.es.Entity;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author adriano
 */
public class MovementSystem extends GameLogicBaseSystem {

    private final Logger _log = LoggerFactory.getLogger(MovementSystem.class);
    private TempVars _tempVars;
    public MovementSystem(GameLogicManager gameLogic) {
        super(gameLogic);
    }

    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
        _entitySet = _gameLogic.getEntityData()
                .getEntities(Position.class, Velocity.class, Input.class);
        _log.info("started");
    }

    @Override
    public void cleanup() {
        super.cleanup();

        _entitySet.release();
    }

//    @Override
//    protected void systemUpdate(float tpf) {
//        _log.info("move system tick");
//    }

    
    @Override
    protected void addedModels(Set<Entity> set) {
        for (Entity e : set) {
            updateVelocity(e);
        }
//        _log.info("entity added");
    }

    @Override
    protected void updateModels(Set<Entity> set) {

        for (Entity e : set) {
            updateVelocity(e);
        }
//        _log.info("entity updated");
    }
    
    @Override
    protected void removeModels(Set<Entity> set) {
//        _log.info("entity removed");
    }
    
    /**
     * Take the steering influence and apply the vehicle's mass, max speed,
     * speed, and maxTurnForce to determine the new velocity.
     */
    private void updateVelocity(Entity e) {
        Vector3f velocity = Vector3f.UNIT_Z;

        Input ic = e.get(Input.class);
        Velocity vc = e.get(Velocity.class);
        Position pc = e.get(Position.class);

        Vector3f steeringForce = truncate(ic.getSteering(), vc.getMaxTurnForce() * ic.getStrength());
        steeringForce.divideLocal(vc.getMass());
        velocity = truncate(velocity.addLocal(steeringForce), vc.getMaxSpeed());        
        Vector3f pos = pc.getLocation().add(velocity.mult(ic.getStrength()));
            
            Quaternion rotTo = pc.getFacing().clone();
            rotTo.lookAt(velocity.normalize(), Vector3f.UNIT_Y);
        
            _log.info("steering: {} force: {} ", ic.getSteering(), ic.getStrength());
        
        e.set(new Position(pos, rotTo, true));
        
//        _log.info("new position: {} new facing: {}", pos.toString(), rotTo.toString());

    }

    /**
     * truncate the length of the vector to the given limit
     */
    private Vector3f truncate(Vector3f source, float limit) {
        if (source.lengthSquared() <= limit * limit) {
            return source;
        } else {
            return source.normalize().scaleAdd(limit, Vector3f.ZERO);
        }
    }
}
