/* 
 * The MIT License
 *
 * Copyright 2013 adriano.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.quanticminds.games.example06.managers;

import com.jme3.ai.navmesh.NavMesh;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.scene.Node;
import com.quanticminds.games.es.example06.factories.EntityFactory;
import com.quanticminds.games.es.example06.systems.MovementSystem;
import com.simsilica.es.EntityData;
import java.util.EnumMap;
import java.util.concurrent.Callable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author adriano
 */
public class GameLogicManager implements Runnable {
    private static Logger _log = LoggerFactory.getLogger(GameLogicManager.class );
    private enum GameState { Menu, WorldMap, Combat, LocalMap, GameOver, Loading};

    private final float DEFAULT_TPF = 0.02f;
    private float _lastFrame = 0;
    private Node _worldRootNode;
    private NavMesh _navMesh;
    private GameState _gameState = GameState.GameOver;
    private AppStateManager _stateManager;
    private SimpleApplication _simpleApp;
    private EntityData _entityData;
    private EntityFactory _entityFactory;
    private MapManager _mapManager;
    private EnumMap<GameState, AbstractAppState> _gameStates;
    
    public GameLogicManager(EntityData entityData, SimpleApplication app) {
        _simpleApp = (SimpleApplication)app;
        _stateManager = new AppStateManager(app);
        _entityData = entityData;
        
        _stateManager.attach(new MovementSystem(this));

//      add the logic AppStates to this thread
//      stateManager.attach(new MovementAppState());
//      stateManager.attach(new ExpiresAppState());
//      stateManager.attach(new CollisionAppState());
//      stateManager.attach(new EnemyAppState());
    }
    
    public void addGameSystem(AbstractAppState...systems){
        _stateManager.attachAll(systems);
    }

    public Node getWorldRootNode() {
        return _worldRootNode;
    }
    
    public void setFloorNode(NavMesh navMesh) {
        this._navMesh = navMesh;
    }
    
    public NavMesh getFloorNode() {
        return _navMesh;
    }
    
    public EntityFactory getEntityfactory(){
        return _entityFactory;
    }
  
    public EntityData getEntityData(){
        return _entityData;
    }
    
    public AppStateManager getStateManager(){
        return this._stateManager;
    }
    
    public void addSceneNode(final Node node){
        _simpleApp.enqueue(new Callable<Void>(){
            public Void call() throws Exception{

                _worldRootNode.attachChild(node);
               
                return null;
            }
        });
    }
    
    public void run() {
        
//        _log.info("entering loop {}");
        
        // Use our own tpf calculation in case frame rate is
        // running away making this tpf unstable
        float time = _simpleApp.getTimer().getTimeInSeconds();
        float delta = time - _lastFrame;
        _lastFrame = time;
        if( delta == 0 ) {
            return; // no update to perform
        }

        // Clamp frame time to no bigger than a certain amount
        // to prevent physics errors.  A little jitter for slow frames
        // is better than tunneling/ghost objects
        if( delta > 0.18f ) {
            delta = DEFAULT_TPF;
        }
        
//        _log.info("running {}", delta);
        
        _stateManager.update(delta);
    } 
}
