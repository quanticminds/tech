/* 
 * The MIT License
 *
 * Copyright 2013 adriano.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.quanticminds.games.example06;

/**
 *
 * @author adriano.ribeiro
 */
public final class Constants {
    public final class ModelTypes{
        public static final String BigSquare = "BIG_SQUARE";        
        
        public static final String TallRetangle = "TALL_RETANGLE";
        
        public static final String Circle = "CIRCLE";
        
        public static final String Asset = "ASSET";
        
    }
    
    public final class Input{
        public static final String Run = "Run";
        public static final String Idle = "Idle";
    }
    
    /**
     *
     */
    public final class Models{
        /**
         *
         */
        public static final String SinbadMesh = "Models/Sinbad/Sinbad.mesh.j3o";
        
        public static final String Sinbad = "Models/Sinbad/sinbad.j3o";
        /**
         *
         */
        public static final String OtoFile = "Models/Oto/Oto.mesh.j3o";
        

    }
    /**
     *
     */
    public final class Maps{
        /**
         *
         */
        public static final String TownFile = "Scenes/city.j3o";
        public static final String Terrain01 = "Scenes/terrain01.j3o";
    }
    /**
     *
     */
    public final class CollisionGroups{
        /**
         *
         */
        public static final int Ground = 1;
        /**
         *
         */
        public static final int Players = 2;
        /**
         *
         */
        public static final int PlayerShoot = 3;
        /**
         *
         */
        public static final int Enemies = 4;
        /**
         *
         */
        public static final int Doors = 5;
        /**
         *
         */
        public static final int Triggers = 6;
    }
    
}
