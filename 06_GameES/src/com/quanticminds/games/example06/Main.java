/* 
 * The MIT License
 *
 * Copyright 2013 adriano.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.quanticminds.games.example06;

import com.jme3.app.SimpleApplication;
import com.jme3.renderer.RenderManager;
import com.jme3.system.AppSettings;
import com.quanticminds.games.es.example06.appstates.EntityDataAppState;
import com.quanticminds.games.es.example06.appstates.LoadingAppState;
import com.quanticminds.games.example06.managers.GameLogicManager;
import com.simsilica.es.EntityData;
import com.simsilica.es.base.DefaultEntityData;
import com.simsilica.lemur.GuiGlobals;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * test
 * @author normenhansen
 */
public class Main extends SimpleApplication {

    private ScheduledExecutorService _managersExecutor;
    private GameLogicManager _gameLogic;
    EntityData _entityData;

    
    public static void main(String[] args) {
        Main app = new Main();
        AppSettings settings = new AppSettings(true);
        settings.setResolution(1368, 720);
        settings.setRenderer(AppSettings.LWJGL_OPENGL2);
        settings.setAudioRenderer(AppSettings.LWJGL_OPENAL);
        app.setSettings(settings);
        app.setShowSettings(false);
        app.start();
    }
    
    @Override
    public void simpleInitApp() {
        
        _entityData = new DefaultEntityData();
        
        // Initialize the Lemur helper instance
        GuiGlobals.initialize(this);

        // Setup default key mappings
        LeaderFunctions.initializeDefaultMappings(GuiGlobals.getInstance().getInputMapper());

        getInputManager().setCursorVisible(true);
               
        getStateManager().attach(new EntityDataAppState(_entityData));
        getStateManager().attach(new LoadingAppState());
       
        _managersExecutor = Executors.newSingleThreadScheduledExecutor();
        _managersExecutor.scheduleAtFixedRate(new GameLogicManager(_entityData, this), 0, 20, TimeUnit.MILLISECONDS);

    }

    @Override
    public void simpleUpdate(float tpf) {
        //TODO: add update code
    }

    @Override
    public void simpleRender(RenderManager rm) {
        //TODO: add render code
    }

    @Override
    public void stop() {
        super.stop();        
        _managersExecutor.shutdown();
    }
}
