/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quanticminds.games.example06;

import com.simsilica.lemur.input.Button;
import com.simsilica.lemur.input.FunctionId;
import com.simsilica.lemur.input.InputMapper;

/**
 *
 * @author adriano
 */
public class LeaderFunctions {

    public static final String GROUP = "Leader Controls";

    public static final FunctionId F_GOTO = new FunctionId(GROUP, "Goto");

    public static void initializeDefaultMappings( InputMapper inputMapper ) {
        // Default key mappings
        inputMapper.map(F_GOTO, Button.MOUSE_BUTTON1);
    }
}
