package com.quanticminds.samples.game04;

import com.jme3.app.SimpleApplication;
import com.jme3.renderer.RenderManager;
import com.jme3.system.AppSettings;
import com.simsilica.es.EntityData;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * test
 * @author normenhansen
 */
public class GameApp extends SimpleApplication {
    
    WorldAppState gameWorld;
    EntityData entityData; 
    GameManager gameManager;
    boolean loaded = false;
            
    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        GameApp app = new GameApp();
        AppSettings settings = new AppSettings(true);
        settings.setResolution(640, 480);
//        settings.setResolution(1360, 720);
        settings.setRenderer(AppSettings.LWJGL_OPENGL2);
        settings.setAudioRenderer(AppSettings.LWJGL_OPENAL);
        app.setSettings(settings);
        app.setShowSettings(false);
        app.start();
    }

    /**
     *
     */
    @Override
    public void simpleInitApp() {
        Logger.getLogger("").setLevel(Level.WARNING);
        
//        this.flyCam.setEnabled(false);    
        this.flyCam.setMoveSpeed(40);

        this.getStateManager().attach(new WorldAppState());

//       gameWorld = this.getStateManager().getState(WorldAppState.class);
//        EntityFactory.createPlayer(gameWorld.getEntityData(), "Player 01", new Vector3f(0f,-9.5f,0f), Quaternion.DIRECTION_Z);

//        for(int i =0; i < 5; i++){
//            EntityFactory.createSinbadNPC(world, String.format("NPC Sinbad %2d",i), 
//                new Vector3f((i*5) - 30f, -9.5f, 20f), Quaternion.DIRECTION_Z);
//        }
//
//        for(int i =0; i < 5; i++){
//            EntityFactory.createOtoNPC(world, String.format("NPC Oto %2d",i), 
//                new Vector3f((i*5) - 30f, -9.5f, -20f), Quaternion.DIRECTION_Z);
//        }    
        
        
        //        gameManager = new GameManager(this);   
        
//        this.enqueue(new Callable<Void>(){
//            public Void call()throws Exception{
//                gameWorld.loadLevel(Constants.Maps.TownFile);
//                gameWorld.attachLevel();
//                return null;
//            }
//        });
        
//        gameManager.startGame();
    }

    /**
     *
     * @param tpf
     */
    @Override
    public void simpleUpdate(float tpf) {
//        
//        if(!loaded){
//            this.gameWorld.loadLevel(Constants.Maps.TownFile);
//            this.gameWorld.attachLevel();
//            loaded = true;
//        }

    }

    /**
     *
     * @param rm
     */
    @Override
    public void simpleRender(RenderManager rm) {
        //TODO: add render code
    }
    
    
}
