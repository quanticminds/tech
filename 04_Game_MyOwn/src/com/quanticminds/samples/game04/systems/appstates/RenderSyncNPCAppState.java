/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quanticminds.samples.game04.systems.appstates;

import com.jme3.animation.LoopMode;
import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.asset.AssetManager;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.PhysicsSpace;
import com.jme3.bullet.control.BetterCharacterControl;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.quanticminds.samples.game04.Constants;
import com.quanticminds.samples.game04.WorldAppState;
import com.quanticminds.samples.game04.components.Model;
import com.quanticminds.samples.game04.components.Movement;
import com.quanticminds.samples.game04.components.Position;
import com.quanticminds.samples.game04.components.Rotation;
import com.quanticminds.samples.game04.controls.AnimationControl;
import com.quanticminds.tech.utils.Pair;
import com.simsilica.es.Entity;
import com.simsilica.es.EntityData;
import com.simsilica.es.EntitySet;
import com.simsilica.es.Name;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author adriano
 */
public class RenderSyncNPCAppState extends AbstractAppState {

    private SimpleApplication simpleApp;
    private WorldAppState world;
    Node worldRootNode;
    private AssetManager assetManager;
    private PhysicsSpace physicsSpace;
    private EntitySet entitySet;
    private EntityData entityData;
    private Map<Long, Spatial> spatials = new HashMap<Long, Spatial>();

    /**
     *
     * @param stateManager
     * @param app
     */
    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);

        this.simpleApp = (SimpleApplication) app;
        this.world = this.simpleApp.getStateManager().getState(WorldAppState.class);
        this.physicsSpace = this.simpleApp.getStateManager().getState(BulletAppState.class).getPhysicsSpace();
        this.worldRootNode = this.world.getWorldRootNode();
        this.assetManager = this.world.getAssetManager();
        this.entityData = this.world.getEntityData();
        this.entitySet = this.entityData.getEntities(Model.class, Position.class, Name.class, Movement.class);
    }

    @Override
    public void update(float tpf) {
        if (isEnabled()) {

            //first load after level change or game start
            if (this.spatials.isEmpty() && this.world.isLevelReady()) {
                entitiesAdded(this.entitySet);

                //after game running and entities changing over time
            } else {
                if (entitySet.applyChanges()) {
                    entitiesAdded(this.entitySet.getAddedEntities());
                    entitiesRemoved(this.entitySet.getRemovedEntities());
                    entitiesChanged(this.entitySet.getChangedEntities());
                }
            }
        }
    }

    @Override
    public void cleanup() {
        super.cleanup();
        this.spatials.clear();
    }

    private void entitiesAdded(Set<Entity> entities) {

        if (entities.isEmpty()) {
            return;
        }

        Iterator<Entity> models = entities.iterator();
        while (models.hasNext()) {
            final Entity e = models.next();
//                    this.simpleApp.enqueue(new Callable<Void>() {
//                        public Void call() throws Exception {
            Name name = e.get(Name.class);
            Model model = e.get(Model.class);
            Position position = e.get(Position.class);

            Node node = new Node(name.getName());
            node.setLocalTranslation(position.getX(), position.getY(), position.getZ());

            Spatial spatial = assetManager.loadModel(model.getFileName());

            float scale = 0f;
            Vector3f relativePosition = new Vector3f();
            float radius = 0f;
            float height = 0f;
            float mass = 0f;

            Map<String, List<Pair<String, LoopMode>>> animations = new HashMap<String, List<Pair<String, LoopMode>>>();

            if (model.getFileName().equals(Constants.Models.OtoFile)) {
                scale = .3f;
                relativePosition.set(0, 1.55f, 0);

                radius = .9f;
                height = 3f;
                mass = 8f;

                animations.put("Idle", new LinkedList<Pair<String, LoopMode>>());
                animations.get("Idle").add(new Pair<String, LoopMode>("stand", LoopMode.Loop));

                animations.put("Run", new LinkedList<Pair<String, LoopMode>>());
                animations.get("Run").add(new Pair<String, LoopMode>("Walk", LoopMode.Loop));

                AnimationControl ac = new AnimationControl(e.getId(), this.entitySet, animations);
                spatial.addControl(ac);
            }else{
                continue;
            }
            
            spatial.setLocalScale(scale);
            node.attachChild(spatial);
            spatial.setLocalTranslation(relativePosition.clone());

            BetterCharacterControl playerControl = new BetterCharacterControl(radius, height, mass);
            node.addControl(playerControl);
            this.physicsSpace.add(playerControl);


            this.worldRootNode.attachChild(node);
            this.spatials.put(e.getId().getId(), node);


//                            return null;
//                        }
//                    });
        }
    }

    private void entitiesRemoved(Set<Entity> entities) {

        if (entities.isEmpty()) {
            return;
        }

        Iterator<Entity> models = entities.iterator();
        while (models.hasNext()) {
            final Entity e = models.next();
//                    this.simpleApp.enqueue(new Callable<Void>() {
//                        public Void call() throws Exception {
            Model model = e.get(Model.class);
            
            if(!spatials.containsKey(e.getId().getId())){
                continue;
            }

            if (model.getFileName().equals(Constants.Models.SinbadFile)
                    || model.getFileName().equals(Constants.Models.OtoFile)
                    || model.getFileName().equals(Constants.Models.JaimeFile)) {
                Spatial spatial = spatials.get(e.getId().getId());
                this.physicsSpace.remove(spatial.getControl(BetterCharacterControl.class));
                spatial.removeFromParent();
            }
//                            return null;
//                        }
//                    });
        }
    }

    private void entitiesChanged(Set<Entity> entities) {

        if (entities.isEmpty()) {
            return;
        }

        Iterator<Entity> models = entities.iterator();
        while (models.hasNext()) {
            final Entity e = models.next();
//                    this.simpleApp.enqueue(new Callable<Void>() {
//                        public Void call() throws Exception {
            Model model = e.get(Model.class);
            Movement mov = e.get(Movement.class);
            
            if(!spatials.containsKey(e.getId().getId())){
                continue;
            }

            if (mov != null) {
                if (model.getFileName().equals(Constants.Models.SinbadFile)
                        || model.getFileName().equals(Constants.Models.OtoFile)
                        || model.getFileName().equals(Constants.Models.JaimeFile)) {
             
                    Spatial spatial = spatials.get(e.getId().getId());
                    BetterCharacterControl bcc = spatial.getControl(BetterCharacterControl.class);
                    bcc.setWalkDirection(mov.getWalkDirection());
                    bcc.setViewDirection(mov.getViewDirection());
                }
            }

//                            return null;
//                        }
//                    });
        }
    }
}
