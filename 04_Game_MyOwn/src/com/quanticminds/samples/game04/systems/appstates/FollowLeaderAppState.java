/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quanticminds.samples.game04.systems.appstates;

import com.jme3.ai.navmesh.NavMeshPathfinder;
import com.jme3.ai.navmesh.Path;
import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.math.Vector3f;
import com.quanticminds.samples.game04.WorldAppState;
import com.quanticminds.samples.game04.components.FollowLeader;
import com.quanticminds.samples.game04.components.Movement;
import com.quanticminds.samples.game04.components.Player;
import com.quanticminds.samples.game04.components.Position;
import com.simsilica.es.Entity;
import com.simsilica.es.EntityData;
import com.simsilica.es.EntityId;
import com.simsilica.es.EntitySet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;

/**
 * este serviço será responsável pura e simplesmente por monitor o Player ativo,
 * obter sua posição atualizada, gerar um caminho até ele para que o seguidores 
 * possam acompanhá-lo. Para isso, ele se interessa em componentes do Player, Movement e Position
 * 
 * @author adriano
 */
public class FollowLeaderAppState extends AbstractAppState {

    private SimpleApplication simpleApp;
    private WorldAppState world;
    private EntityId playerLeaderId;
    private EntitySet entitySetPlayers;
    private EntityData entityData;
    private Vector3f leaderPosition = new Vector3f();
    private Map<Long, NavMeshPathfinder> playersPaths = new HashMap<Long, NavMeshPathfinder>();

    /**
     *
     * @param stateManager
     * @param app
     */
    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);

        this.simpleApp = (SimpleApplication) app;
        this.world = this.simpleApp.getStateManager().getState(WorldAppState.class);
        this.entityData = world.getEntityData();
        this.entitySetPlayers = this.entityData.getEntities(Position.class, Player.class, Movement.class, FollowLeader.class);
    }

    @Override
    public void update(float tpf) {
        if (isEnabled()) {
            
            if (playersPaths.isEmpty()) {
                entitiesAdded(this.entitySetPlayers);
            }

            if (entitySetPlayers.applyChanges()) {
                entitiesAdded(this.entitySetPlayers.getAddedEntities());
                entitiesRemoved(this.entitySetPlayers.getRemovedEntities());
                entitiesChanged(this.entitySetPlayers.getChangedEntities());
            }
            
            Iterator<Entity> followers = this.entitySetPlayers.iterator();

            while (followers.hasNext()) {

                Entity e = followers.next();

//                long entityId = e.getKey();
//                FollowLeaderComponent flc = e.getValue();
//                Vector3f start = new Vector3f();
//                Vector3f finish = new Vector3f();
//
//                start.set(flc.getStart());
//                finish.set(flc.getFinish());
//
//                PositionComponent myps = this.world.getEntityDB()
//                        .getComponent(entityId, PositionComponent.class);
//
//                NavMeshPathfinder npf = null;
//                if (!navMeshPaths.containsKey(entityId)) {
//                    navMeshPaths.put(entityId, new NavMeshPathfinder(this.world.getNavMesh()));
//                    npf = navMeshPaths.get(entityId);
//                    
//                    start.set(myps.getLocation().clone());
//                    npf.setPosition(start);
//
//                    finish.set(playerPosition.getLocation().clone());
//                    boolean pathOk = npf.computePath(finish);
//
//                }else{
//                    npf = navMeshPaths.get(entityId);
//                }
//
//                if (!finish.equals(playerPosition.getLocation())) {
//                    start.set(myps.getLocation());
//                    finish.set(playerPosition.getLocation());
//
//                    npf.setPosition(start);
//                    npf.computePath(finish);
//                }
//
//                followers.setComponent(entityId, new FollowLeaderComponent(
//                        flc.getDistance(), start, finish));
//
//                Path.Waypoint nextPoint = npf.getNextWaypoint();
//                if (nextPoint == null) {
//                    continue;
//                }
//
//                float angleY = myps.getLocation().normalize()
//                        .angleBetween(playerPosition.getLocation().normalize());
//
//                Quaternion rotate = new Quaternion();
//                if (angleY > 0) {
//                    //rotate.fromAngles(0, angleY, 0);
//                    rotate.lookAt(nextPoint.getPosition().normalize(), Vector3f.UNIT_Y);
//                }
//
//                System.out.printf("id: %d angle: %f \n", entityId, angleY);
////                rotate.lookAt(nextPoint.getPosition().normalize(),Vector3f.UNIT_Y);
//
//                if (myps.getLocation().distance(playerPosition.getLocation())
//                        > flc.getDistance()) {
////                    walkDirection.set(myps.getLocation())
////                            .subtract(nextPoint.getPosition())
////                            .negate().mult(5f);
//                    walkDirection.set(nextPoint.getPosition());
//
//                    this.world.getEntityDB().setComponent(entityId,
//                            new MovementComponent(
//                            walkDirection,
//                            rotate));
//                } else {
//                    this.world.getEntityDB().clearComponent(entityId, MovementComponent.class);
//                }
//
//                //persist data
//                followers.applyChanges();
            }
        }
    }

    @Override
    public void cleanup() {
        super.cleanup();
    }

    private void entitiesAdded(Set<Entity> entities) {

        if (entities.isEmpty()) {
            return;
        }

        Iterator<Entity> models = entities.iterator();
        while (models.hasNext()) {
            final Entity e = models.next();

            Player pl = e.get(Player.class);

            if (pl.isLeader()) {
                if (this.playerLeaderId != e.getId()) {
                    this.playerLeaderId = e.getId();
                    Position lp = e.get(Position.class);
                    this.leaderPosition.set(lp.getLocation());
                }
                
                continue;
            }

//            this.simpleApp.enqueue(new Callable<Void>() {
//                public Void call() throws Exception {


                    Position pos = e.get(Position.class);
                    NavMeshPathfinder finder = null;
                    if (!playersPaths.containsKey(e.getId().getId())) {
                        finder = new NavMeshPathfinder(world.getNavMesh());
                        playersPaths.put(e.getId().getId(), finder);
                        finder.setPosition(pos.getLocation());
                        finder.computePath(leaderPosition);
                    }

                    Path.Waypoint point = finder.getNextWaypoint();

                    if (point != null) {

                        e.set(new FollowLeader(point.getPosition()));

                        float dot = pos.getLocation().dot(point.getPosition());

                        if (dot <= 0) {
                            e.set(new Movement(0, 0, 0, 3, 0, 3));
                        }
                    }

//                    return null;
//                }
//            });
        }
    }

    private void entitiesRemoved(Set<Entity> entities) {

        if (entities.isEmpty()) {
            return;
        }

        Iterator<Entity> models = entities.iterator();
        while (models.hasNext()) {
            final Entity e = models.next();
//                    this.simpleApp.enqueue(new Callable<Void>() {
//                        public Void call() throws Exception {
            
            if (playersPaths.containsKey(e.getId().getId())) {
                playersPaths.remove(e.getId().getId());
            }            
            
//                            return null;
//                        }
//                    });
        }
    }

    private void entitiesChanged(Set<Entity> entities) {

        if (entities.isEmpty()) {
            return;
        }

        Iterator<Entity> models = entities.iterator();
        while (models.hasNext()) {
            final Entity e = models.next();

            Player pl = e.get(Player.class);
            Position pos = e.get(Position.class);
            FollowLeader follow = e.get(FollowLeader.class);

            if (pl.isLeader()) {

                // new leader
                if (this.playerLeaderId != e.getId()) {
                    this.playerLeaderId = e.getId();
                    this.leaderPosition.set(pos.getLocation());
                }
            }

//            if (playersPaths.containsKey(e.getId().getId())) {
//                NavMeshPathfinder finder = playersPaths.get(e.getId().getId());
//                finder.setPosition(pos.getLocation());
//                finder.computePath(leaderPosition);
//            }

//            Path.Waypoint point = finder.getNextWaypoint();
        }
    }

    private void entitiesProcessing(Set<Entity> entities) {

        if (entities.isEmpty()) {
            return;
        }

        Iterator<Entity> models = entities.iterator();
        while (models.hasNext()) {
            final Entity e = models.next();

            Player pl = e.get(Player.class);

            if (!pl.isLeader()) {
            }
        }
    }

}
