/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quanticminds.samples.game04;

import com.jme3.ai.navmesh.NavMesh;
import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.asset.AssetManager;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.PhysicsSpace;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.quanticminds.samples.game04.systems.appstates.FollowLeaderAppState;
import com.quanticminds.samples.game04.systems.appstates.RenderSyncAppState;
import com.quanticminds.samples.game04.systems.appstates.RenderSyncNPCAppState;
import com.simsilica.es.EntityData;
import com.simsilica.es.sql.SqlEntityData;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author adriano.ribeiro
 */
public class WorldAppState extends AbstractAppState {

    BulletAppState physicsSpace;
    SimpleApplication app;
    Node worldRootNode = new Node("WorldRootNode");
    NavMesh navMesh;
    EntityData entityData;
    boolean levelReady = false;

    public boolean isLevelReady() {
        return levelReady;
    }
    
    public EntityData getEntityData() {
        return entityData;
    }
    
    public SimpleApplication getSimpleApp() {
        return app;
    }
    
    /**
     *
     * @return
     */
    public Node getWorldRootNode() {
        return worldRootNode;
    }

    /**
     *
     * @return
     */
    public AssetManager getAssetManager() {
        return this.app.getAssetManager();
    }
    
    public PhysicsSpace getPhysicsSpace(){
        return this.physicsSpace.getPhysicsSpace();
    }
    
  
    /**
     *
     * @return
     */
    public NavMesh getNavMesh(){
        return navMesh;
    }

    /**
     *
     * @param stateManager
     * @param app
     */
    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
        
        this.app = (SimpleApplication) app;
        this.physicsSpace = new BulletAppState();
        
        //physics app state with debug enable
        this.app.getStateManager().attach(physicsSpace);
//        this.physicsSpace.getPhysicsSpace().enableDebug(this.getAssetManager());
        this.physicsSpace.setDebugEnabled(true);
        
        try{
//        // The main entry point to my ES is EntityData… I think it better describes what we’re
//        // doing and avoids the “system” confusion
            entityData = new SqlEntityData( Constants.DataBase.GameData, 100 ); // 100 ms write-delay
        }catch(SQLException ex){
                Logger.getLogger(WorldAppState.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        this.app.getStateManager().attach(new FollowLeaderAppState());
        this.app.getStateManager().attach(new RenderSyncAppState());
        this.app.getStateManager().attach(new RenderSyncNPCAppState());
        
        loadLevel(Constants.Maps.TownFile);
        attachLevel();       
        
//        EntityFactory.createPlayer(entityData, "Player 01", new Vector3f(0f,-9.5f,0f), Quaternion.DIRECTION_Z);

//        for(int i =0; i < 5; i++){
//            EntityFactory.createSinbadNPC(entityData, String.format("NPC Sinbad %2d",i), 
//                new Vector3f((i*5) - 30f, -9.5f, 20f), Quaternion.DIRECTION_Z);
//        }
//
//        for(int i =0; i < 5; i++){
//            EntityFactory.createOtoNPC(entityData, String.format("Robo Oto %2d",i), 
//                new Vector3f((i*5) - 30f, -9.5f, -20f), Quaternion.DIRECTION_Z);
//        }    
    }

    @Override
    public void update(float tpf) {
        if(isEnabled()){

        }
    }

    @Override
    public void cleanup() {
        super.cleanup();

        this.unloadLevel();
        this.detachLevel();
        
        if(this.entityData != null){
            this.entityData.close();
        }
    }

    /**
     *
     * @param map
     */
    public void loadLevel(String map) {
        final String mapFile = map;

//        app.enqueue(new Callable<Void>(){
//            public Void call() throws Exception{
                worldRootNode = (Node)app.getAssetManager().loadModel(mapFile);
                levelReady = true;
//                return null;
//            }
//        });        
    }

    /**
     *
     */
    public void unloadLevel() {
        this.worldRootNode.detachAllChildren();
    }

    /**
     *
     */
    public void attachLevel() {
//        app.enqueue(new Callable<Void>(){
//            public Void call() throws Exception{                
                Geometry navGeo = (Geometry)worldRootNode.getChild("NavMesh");
                navMesh = new NavMesh(navGeo.getMesh());
                
                app.getRootNode().attachChild(worldRootNode);
                RigidBodyControl rb = new RigidBodyControl(0);
                rb.setCollisionGroup(Constants.CollisionGroups.Ground);
                worldRootNode.addControl(rb);
                physicsSpace.getPhysicsSpace().add(rb);
//                return null;
//            }
//        });        
    }

    /**
     *
     */
    public void detachLevel() {
        this.app.getRootNode().detachChild(this.worldRootNode);
        RigidBodyControl rb = this.worldRootNode.getControl(RigidBodyControl.class);

        if (rb != null) {
            this.physicsSpace.getPhysicsSpace().remove(rb);
        }
    }
}
