/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quanticminds.samples.game04.controls;

import com.jme3.ai.navmesh.NavMesh;
import com.jme3.ai.navmesh.NavMeshPathfinder;
import com.jme3.ai.navmesh.Path;
import com.jme3.export.InputCapsule;
import com.jme3.export.JmeExporter;
import com.jme3.export.JmeImporter;
import com.jme3.export.OutputCapsule;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;
import com.jme3.scene.control.Control;
import com.quanticminds.samples.game04.WorldAppState;
import java.io.IOException;
import java.util.logging.Logger;

/**
 * Esse controle encorpora a capacidade de locomoção iniciando na sua 
 * posição atual e terminando em um destino definido, De tempos em tempos 
 * este caminho será recalculado, seja por a entidade ficou presa, seja para 
 * se adequar a uma possível alteração do destino.
 * @author adriano
 */
public class FollowLeaderControl extends AbstractControl {

    private WorldAppState world;
    private NavMesh navMesh = null;
    private Vector3f targetPosition = new Vector3f();
    private float minDistance = 0f;
    private float maxDistance = 0f;
    private NavMeshPathfinder myPath= null;
    private static final Logger logger = Logger.getLogger(FollowLeaderControl.class.getName());


    public FollowLeaderControl(WorldAppState world) {
        this.navMesh = world.getNavMesh();        
        this.world = world;
    }
    
    public void setTargetPosition(Vector3f targetPosition){
        targetPosition.set(targetPosition);
    }
    
    public Path getPath(){
        return myPath.getPath();
    }

    @Override
    public void setSpatial(Spatial spatial) {
        super.setSpatial(spatial);
        
        
    }
      
   
    @Override
    protected void controlUpdate(float tpf) {
        //TODO: add code that controls Spatial,
        //e.g. spatial.rotate(tpf,tpf,tpf);
    }
    
    @Override
    protected void controlRender(RenderManager rm, ViewPort vp) {
        //Only needed for rendering-related operations,
        //not called when spatial is culled.
    }
    
    public Control cloneForSpatial(Spatial spatial) {
        FollowLeaderControl control = new FollowLeaderControl(world);
        //TODO: copy parameters to new Control
        control.setSpatial(spatial);
        return control;
    }
    
    @Override
    public void read(JmeImporter im) throws IOException {
        super.read(im);
        InputCapsule in = im.getCapsule(this);
        //TODO: load properties of this Control, e.g.
        //this.value = in.readFloat("name", defaultValue);
    }
    
    @Override
    public void write(JmeExporter ex) throws IOException {
        super.write(ex);
        OutputCapsule out = ex.getCapsule(this);
        //TODO: save properties of this Control, e.g.
        //out.write(this.value, "name", defaultValue);
    }
}
