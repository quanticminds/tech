/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quanticminds.tech.commands.base;

import com.quanticminds.samples.game04.WorldAppState;
import com.simsilica.es.Entity;

/**
 *
 * @author adriano.ribeiro
 */
public interface Command {
    enum State{
        Invalid,
        Sucess,
        Failure,
        Running,
        Aborted,
    }

    public String getName();

    public State update(float tpf);

    public void initialize(WorldAppState world, Entity entity);
    
    public int getPriority();

    public void setPriority(int priority);

    public boolean isRunning();

    public void setRunning(boolean running);

    public void cancel();    
}
