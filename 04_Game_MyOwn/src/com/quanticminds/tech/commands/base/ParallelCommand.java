/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quanticminds.tech.commands.base;

import java.util.Iterator;

/**
 *
 * @author adriano.ribeiro
 */
public class ParallelCommand extends CompositeCommand {

    @Override
    protected State commandUpdate(float tpf) {

        for (Iterator<Command> it = commands.iterator(); it.hasNext();) {
            Command command = it.next();
            //do command and remove if returned true, else stop processing
            Command.State state = command.update(tpf);
            switch (state) {
                case Aborted:
                case Failure:
                    commandState = state;
                    break;
            }
        }

        return commandState;
    }
}
