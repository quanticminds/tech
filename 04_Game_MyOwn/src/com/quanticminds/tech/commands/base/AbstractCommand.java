/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quanticminds.tech.commands.base;

import com.quanticminds.samples.game04.WorldAppState;
import com.simsilica.es.Entity;

/**
 *
 * @author adriano.ribeiro
 */
public abstract class AbstractCommand implements Command{

    protected State commandState = State.Invalid;
    protected WorldAppState world;
    protected Entity entity;
    String name;
    int priority;

    
    public String getName(){
        return name;
    }

    public void initialize(WorldAppState world, Entity entity){
        this.world = world;
        this.entity = entity;
    }
    
    public void terminate(State commandState){
    }    
    
    public State update(float tpf){
        
        if(commandState != State.Running){
            initialize(world, entity);
        }
        
        commandState = commandUpdate(tpf);
        
        //After my command finish.
        if(commandState != State.Running){
            terminate(commandState);
        }
        
        return commandState;
    }

    public void abort(){
        terminate(State.Aborted);
        commandState = State.Aborted;
    }
    
    public int getPriority(){
        return this.priority;
    }

    public void setPriority(int priority){
        this.priority = priority;
    }
    
    public boolean isRunning(){
        return commandState == State.Running;
    }

    public void setRunning(boolean running) {
        this.commandState = State.Running;
    }
    
    public boolean isTerminated(){
        return (commandState == State.Running) || (commandState == State.Failure);
    }

    public void cancel(){
//        world.getCommandQueue().removeCommand(this);
    }
    
    protected abstract State commandUpdate(float tpf);
}
