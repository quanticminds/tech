/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quanticminds.tech.commands.base;

import com.quanticminds.samples.game04.WorldAppState;
import com.simsilica.es.Entity;
import java.util.LinkedList;

/**
 *
 * @author adriano.ribeiro
 */
public abstract class CompositeCommand extends AbstractCommand{
    protected LinkedList<Command> commands = new LinkedList<Command>();    

    @Override
    public void initialize(WorldAppState world, Entity entity) {
        super.initialize(world, entity);
        for(int i =0; i < commands.size(); i++){
            commands.get(i).initialize(world, entity);
        }
    }  
    
    
    public void addCommand(Command command){
        for(int i =0; i < commands.size(); i++){
            Command c = commands.get(i);
            if(command.getPriority() < command.getPriority()){
                commands.add(i,command);
                return;
            }
        }
        commands.add(command);
    }
    
    public void removeCommand(Command command){
        commands.remove(command);
    }
}
