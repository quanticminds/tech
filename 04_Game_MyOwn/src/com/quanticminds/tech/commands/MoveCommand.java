/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quanticminds.tech.commands;

import com.jme3.bullet.control.BetterCharacterControl;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Spatial;
import com.quanticminds.samples.game04.WorldAppState;
import com.quanticminds.tech.commands.base.AbstractCommand;
import com.simsilica.es.Entity;

/**
 *
 * @author adriano.ribeiro
 */
public class MoveCommand extends AbstractCommand{

    protected BetterCharacterControl character;
    protected Spatial spatial;

    public MoveCommand() {
    }

    public MoveCommand(Spatial spatial) {
        this.spatial = spatial;
        
    }   

    @Override
    public void initialize(WorldAppState world, Entity entity) {
        super.initialize(world, entity);
        
        this.character = spatial.getControl(BetterCharacterControl.class);
    }
      
    
    
    @Override
    protected State commandUpdate(float tpf) {
        Vector3f modelForwardDir = spatial.getWorldRotation().mult(Vector3f.UNIT_Z);

//        walkDirection.set(0, 0, 0);
//        viewDirection.set(modelForwardDir.normalize().clone());
//
//        if (forward) {
//            walkDirection.addLocal(modelForwardDir.mult(8));
//        } else if (backward) {
//            walkDirection.addLocal(modelForwardDir.negate().multLocal(8));
//        }
//
//        if (turnLeft) {
//            Quaternion rotateL = new Quaternion().fromAngleAxis(FastMath.PI * tpf, Vector3f.UNIT_Y);
//            rotateL.multLocal(viewDirection);
//        } else if (turnRight) {
//            Quaternion rotateR = new Quaternion().fromAngleAxis(-FastMath.PI * tpf, Vector3f.UNIT_Y);
//            rotateR.multLocal(viewDirection);
//        }
        
        return this.commandState;
    }
}
