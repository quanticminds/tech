/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quanticminds.tech.commands;

import com.jme3.animation.AnimChannel;
import com.jme3.animation.AnimControl;
import com.jme3.animation.LoopMode;
import com.jme3.scene.Spatial;
import com.quanticminds.samples.game04.WorldAppState;
import com.quanticminds.tech.commands.base.AbstractCommand;
import com.simsilica.es.Entity;

/**
 *
 * @author adriano.ribeiro
 */
public class AnimateCommand extends AbstractCommand{

    private AnimControl animControl;
    private AnimChannel animChannel;
    protected String animationName;

    public AnimateCommand() {
    }

    
    public AnimateCommand(Spatial spatial, String animationName) {
        this.animControl = spatial.getControl(AnimControl.class);
        this.animationName = animationName;
    }

    @Override
    public void initialize(WorldAppState world, Entity entity) {
        super.initialize(world, entity);
        
        this.animChannel = this.animControl.createChannel();
        this.animChannel.setAnim(this.animationName);
        this.animChannel.setLoopMode(LoopMode.DontLoop);
    }
    
    

    public String getAnimationName() {
        return animationName;
    }

    public void setAnimationName(String animationName) {
        this.animationName = animationName;
    }
    
        
    @Override
    protected State commandUpdate(float tpf) {
        if(isRunning()){
            this.animChannel.setAnim(animationName);
            this.animChannel.setLoopMode(LoopMode.DontLoop);
        }
        
        return super.commandState;
    }
}
