/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quanticminds.tech.utils;

/**
 *
 * @param <T> 
 * @param <K> 
 * @author adriano.ribeiro
 */
public class Pair<T,K> {
    /**
     * first element from data pair
     */
    public T first;
    /**
     * second element from data pair.
     */
    public K second;
    
    /**
     *
     * @param first
     * @param second
     */
    public Pair(T first, K second){
        this.first = first;
        this.second = second;
    }    
}
