package com.quanticminds.samples.game04;

import com.jme3.animation.LoopMode;
import com.jme3.bullet.control.BetterCharacterControl;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.quanticminds.samples.game04.components.Model;
import com.quanticminds.samples.game04.components.Movement;
import com.quanticminds.samples.game04.components.Position;
import com.quanticminds.tech.utils.Triple;
import com.simsilica.es.EntityData;
import com.simsilica.es.EntityId;
import com.simsilica.es.Name;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author adriano
 */
public class EntityFactory {

    /**
     *
     * @param db
     * @param name
     * @param position
     * @param rotation
     * @return
     */
    public static void createPlayer(EntityData ed, String name, 
                    Vector3f position, Quaternion rotation) {

//        // Create an entity and set some stuff using raw access
//            EntityId player = ed.createEntity();//ed.createEntity();
//            ed.setComponent( player, new Name(name));
//            ed.setComponent( player, new Player(true));
//            ed.setComponent( player, new Model(Constants.Models.SinbadFile));
//            ed.setComponent( player, new Position(position.getX(),position.getY(),position.getZ()));

//        EntityId c3po = ed.createEntity();
//        ed.setComponent( c3po, new Name( “C-3PO”);
//        es.setComponent( c3po, new CreatedBy(anakin) );


//        Node node = new Node(name);
//        node.setLocalTranslation(position);
//        node.setLocalRotation(rotation);
//
//        Spatial spatial = world.getAssetManager().loadModel(Constants.Models.SinbadFile);
//        spatial.setLocalScale(.27f);
//        node.attachChild(spatial);        
//        spatial.setLocalTranslation(0, 1.4f, 0);
//        node.setUserData("SpatialName", spatial.getName());
//
//        BetterCharacterControl playerControl = new BetterCharacterControl(.7f, 3f, 8f);
//        node.addControl(playerControl);
//        world.getPhysicsSpace().add(playerControl);        
//    
//        Map<String,List<Triple<String, LoopMode, Float>>> animations =
//                new HashMap<String, List<Triple<String, LoopMode, Float>>>();
//        
//        animations.put("Idle", new LinkedList<Triple<String, LoopMode, Float>>());
//        animations.get("Idle").add(new Triple<String, LoopMode, Float>("IdleTop", LoopMode.Loop,0.15f));
//        animations.get("Idle").add(new Triple<String, LoopMode, Float>("IdleBase", LoopMode.Loop,0.15f));
//        animations.get("Idle").add(new Triple<String, LoopMode, Float>("HandsRelaxed", LoopMode.Loop,0.15f));
//
//        animations.put("Run", new LinkedList<Triple<String, LoopMode, Float>>());
//        animations.get("Run").add(new Triple<String, LoopMode, Float>("RunTop", LoopMode.Loop,0.15f));
//        animations.get("Run").add(new Triple<String, LoopMode, Float>("RunBase", LoopMode.Loop,0.15f));
//        animations.get("Run").add(new Triple<String, LoopMode, Float>("HandsClosed", LoopMode.Loop,0.15f));
//
//        AnimationControl ac = new AnimationControl(name, animations);
//        ac.playAnimation(spatial.getName(), "Idle");
//        node.addControl(ac);        
//
//        node.addControl(new PlayerControl(true, world.getSimpleApp().getInputManager()));
//                
//        world.getWorldRootNode().attachChild(node);

//        return node;
    }

    public static Node createJaime(WorldAppState world, String name, 
                    Vector3f position, Quaternion rotation) {
        
        Node node = new Node(name);
        node.setLocalTranslation(position);
        node.setLocalRotation(rotation);

        Spatial spatial = world.getAssetManager().loadModel(Constants.Models.JaimeFile);
        spatial.setLocalScale(1.50f);
        node.setUserData("SpatialName", spatial.getName());

        Map<String,List<Triple<String, LoopMode, Float>>> animations =
                new HashMap<String, List<Triple<String, LoopMode, Float>>>();
        
        animations.put("Idle", new LinkedList<Triple<String, LoopMode, Float>>());
        animations.get("Idle").add(new Triple<String, LoopMode, Float>("Idle", LoopMode.Loop,0.15f));
        
        animations.put("Walk", new LinkedList<Triple<String, LoopMode, Float>>());
        animations.get("Walk").add(new Triple<String, LoopMode, Float>("Walk", LoopMode.Loop,0.15f));
        
        animations.put("Run", new LinkedList<Triple<String, LoopMode, Float>>());
        animations.get("Run").add(new Triple<String, LoopMode, Float>(
                "Run", LoopMode.Loop,0.15f
            ));
                
//        AnimationControl ac = new AnimationControl(name, animations);
//        ac.playAnimation(spatial.getName(), "Idle");
        
        node.attachChild(spatial);        
        BetterCharacterControl playerControl = new BetterCharacterControl(0.3f, 2.5f, 8f);
        node.addControl(playerControl);
//        node.addControl(new PlayerControl(true, world.getSimpleApp().getInputManager()));
        world.getPhysicsSpace().add(playerControl);        
        world.getWorldRootNode().attachChild(node);

        return node;
    }
    
    /**
     *
     * @param db
     * @param name
     * @param position
     * @param rotation
     * @return
     */
    public static Node createSinbadNPC(EntityData ed, String name, 
                Vector3f position, Quaternion rotation) {

            EntityId player = ed.createEntity();
            ed.setComponent( player, new Name(name));
            ed.setComponent( player, new Model(Constants.Models.SinbadFile));
            ed.setComponent( player, new Position(position.getX(),position.getY(),position.getZ()));
            ed.setComponent( player, new Movement(0,0,0,0,0,0));

        Node node = new Node(name);
//        node.setLocalTranslation(position);
//        node.setLocalRotation(rotation);
//        node.setLocalScale(0.27f);
//        
//        Spatial spatial = world.getAssetManager().loadModel(Constants.Models.SinbadFile);
//        node.attachChild(spatial);
//        spatial.setLocalTranslation(0, 5.1f, 0);
//        node.setUserData("SpatialName", spatial.getName());
//        
//        BetterCharacterControl enemyControl = new BetterCharacterControl(.5f, 3f, 8f);
//        node.addControl(enemyControl);
//        world.getPhysicsSpace().add(enemyControl);
// 
//        Map<String,List<Triple<String, LoopMode, Float>>> animations =
//                new HashMap<String, List<Triple<String, LoopMode, Float>>>();
//        
//        animations.put("Idle", new LinkedList<Triple<String, LoopMode, Float>>());
//        animations.get("Idle").add(new Triple<String, LoopMode, Float>("IdleTop", LoopMode.Loop,0.15f));
//        animations.get("Idle").add(new Triple<String, LoopMode, Float>("IdleBase", LoopMode.Loop,0.15f));
//        animations.get("Idle").add(new Triple<String, LoopMode, Float>("HandsRelaxed", LoopMode.Loop,0.15f));
//
//        animations.put("Run", new LinkedList<Triple<String, LoopMode, Float>>());
//        animations.get("Run").add(new Triple<String, LoopMode, Float>("RunTop", LoopMode.Loop,0.15f));
//        animations.get("Run").add(new Triple<String, LoopMode, Float>("RunBase", LoopMode.Loop,0.15f));
//        animations.get("Run").add(new Triple<String, LoopMode, Float>("HandsClosed", LoopMode.Loop,0.15f));

//        animations.put("Jump", new LinkedList<Triple<String, LoopMode, Float>>());
//        animations.get("Jump").add(new Triple<String, LoopMode, Float>("RunTop", LoopMode.Loop,0.15f));
//        animations.get("Jump").add(new Triple<String, LoopMode, Float>("RunBase", LoopMode.Loop,0.15f));

        
//        AnimationControl ac = new AnimationControl(name, animations);
//        ac.playAnimation(spatial.getName(), "Idle");
//        spatial.addControl(ac);        
        
//        world.getWorldRootNode().attachChild(node);
                
        return node;
    }
    
    /**
     *
     * @param db
     * @param name
     * @param position
     * @param rotation
     * @return
     */
    public static Node createOtoNPC(EntityData ed, String name, 
                Vector3f position, Quaternion rotation) {

        
        
        EntityId player = ed.createEntity();//ed.createEntity();
        ed.setComponent( player, new Name(name));
        ed.setComponent( player, new Model(Constants.Models.OtoFile));
        ed.setComponent( player, new Position(position.getX(),position.getY(),position.getZ()));

        
        Node node = new Node(name);
//        node.setLocalTranslation(position);
//        node.setLocalRotation(rotation);
//        node.setLocalScale(0.3f);
//        
//        Spatial spatial = world.getAssetManager().loadModel(Constants.Models.OtoFile);
//        node.attachChild(spatial);               
//        spatial.setLocalTranslation(0, 5f, 0);
//        node.setUserData("SpatialName", spatial.getName());
//        
//        BetterCharacterControl enemyControl = new BetterCharacterControl(.9f, 3f, 8f);
//        node.addControl(enemyControl);
//        world.getPhysicsSpace().add(enemyControl);
//
//        Map<String,List<Triple<String, LoopMode, Float>>> animations =
//                new HashMap<String, List<Triple<String, LoopMode, Float>>>();
//        
//        animations.put("Idle", new LinkedList<Triple<String, LoopMode, Float>>());
//        animations.get("Idle").add(new Triple<String, LoopMode, Float>("stand", LoopMode.Loop,0.15f));
//
//        animations.put("Walk", new LinkedList<Triple<String, LoopMode, Float>>());
//        animations.get("Walk").add(new Triple<String, LoopMode, Float>("Walk", LoopMode.Loop,0.15f));
//
////        AnimationControl ac = new AnimationControl(name, animations);
////        spatial.addControl(ac);        
////        ac.playAnimation(spatial.getName(), "Idle");
//        
//        world.getWorldRootNode().attachChild(node);        

        return node;
    }
}
