/*
 * $Id: Model.java adribeiro@gmail.com $
 *
 * Copyright (c) 2012-2013 All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the distribution.
 *
 * * Neither the modelFile of 'jMonkeyEngine' nor the names of its contributors
 *   may be used to endorse or promote products derived from this software
 *   without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.quanticminds.samples.game04.components;

import com.jme3.math.Vector3f;
import com.jme3.network.serializing.Serializable;
import com.simsilica.es.EntityComponent;
import com.simsilica.es.PersistentComponent;

/**
 *  Represents the modelFile of an entity.
 *
 *  @version   $Revision: 961 $
 *  @author    Paul Speed
 */
@Serializable
public class Movement implements EntityComponent, PersistentComponent
{
    private float walkX;
    private float walkY;
    private float walkZ;

    private float viewX;
    private float viewY;
    private float viewZ;

    public Movement(){
        
    }
    
    public Movement(float walkX, float walkY, float walkZ, float viewX, float viewY, float viewZ) {
        this.walkX = walkX;
        this.walkY = walkY;
        this.walkZ = walkZ;
        this.viewX = viewX;
        this.viewY = viewY;
        this.viewZ = viewZ;
    }

    public float getViewX() {
        return viewX;
    }

    public float getViewY() {
        return viewY;
    }

    public float getViewZ() {
        return viewZ;
    }

    public float getWalkX() {
        return walkX;
    }

    public float getWalkY() {
        return walkY;
    }

    public float getWalkZ() {
        return walkZ;
    }
   
    public boolean IsMoving(){
        return walkX != 0f || walkY != 0f || walkZ != 0f;
    }
    
    public Vector3f getWalkDirection(){
        return new Vector3f(walkX, walkY, walkZ);
    }

    public Vector3f getViewDirection(){
        return new Vector3f(viewX, viewY, viewZ);
    }
    
    @Override
    public String toString()
    {
        return String.format("Movement [ walk: %f, %f, %f - view: %f, %f, %f]", walkX, walkY, walkZ, viewX, viewY, viewZ);
    }      
}

