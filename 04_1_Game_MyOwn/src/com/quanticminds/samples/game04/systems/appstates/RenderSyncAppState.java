/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quanticminds.samples.game04.systems.appstates;

import com.jme3.animation.LoopMode;
import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.asset.AssetManager;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.PhysicsSpace;
import com.jme3.bullet.control.BetterCharacterControl;
import com.jme3.input.ChaseCamera;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.quanticminds.samples.game04.Constants;
import com.quanticminds.samples.game04.WorldAppState;
import com.quanticminds.samples.game04.components.Model;
import com.quanticminds.samples.game04.components.Movement;
import com.quanticminds.samples.game04.components.Player;
import com.quanticminds.samples.game04.components.Position;
import com.quanticminds.samples.game04.controls.AnimationControl;
import com.quanticminds.samples.game04.controls.PlayerControl;
import com.quanticminds.tech.utils.Pair;
import com.simsilica.es.Entity;
import com.simsilica.es.EntityData;
import com.simsilica.es.EntitySet;
import com.simsilica.es.Name;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;

/**
 *
 * @author adriano
 */
public class RenderSyncAppState extends AbstractAppState {

    private SimpleApplication simpleApp;
    private WorldAppState world;
    Node worldRootNode;
    private AssetManager assetManager;
    private PhysicsSpace physicsSpace;
    private EntitySet entitySet;
    private EntityData entityData;
    private ChaseCamera playerCamera;
    private Map<Long, Spatial> spatials = new HashMap<Long, Spatial>();
    private Map<Long, BetterCharacterControl> physics = new HashMap<Long, BetterCharacterControl>();

    /**
     *
     * @param stateManager
     * @param app
     */
    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);

        this.simpleApp = (SimpleApplication) app;
        this.world = this.simpleApp.getStateManager().getState(WorldAppState.class);
        this.physicsSpace = this.simpleApp.getStateManager().getState(BulletAppState.class).getPhysicsSpace();
        this.worldRootNode = this.world.getWorldRootNode();
        this.assetManager = this.world.getAssetManager();
        this.entityData = this.world.getEntityData();
        this.entitySet = this.entityData.getEntities(Model.class, Position.class, Name.class, Player.class, Movement.class);
    }

    @Override
    public void update(float tpf) {
        if (isEnabled()) {

            //first load after level change or game start
            if (this.spatials.isEmpty()) {
                entitiesAdded(this.entitySet);
                //after game running and entities changing over time
            }
            
            if (entitySet.applyChanges()) {
                entitiesAdded(this.entitySet.getAddedEntities());
                entitiesRemoved(this.entitySet.getRemovedEntities());
                entitiesChanged(this.entitySet.getChangedEntities());
            }

        }
    }

    @Override
    public void cleanup() {
        super.cleanup();
    }

    private void entitiesAdded(Set<Entity> entities) {

        if (entities.isEmpty()) {
            return;
        }

        Iterator<Entity> models = entities.iterator();
        while (models.hasNext()) {
            final Entity e = models.next();

            if (spatials.containsKey(e.getId().getId())) {
                continue;
            }
            this.simpleApp.enqueue(new Callable<Void>() {
                public Void call() throws Exception {
                    Name name = e.get(Name.class);
                    Model model = e.get(Model.class);
                    Player player = e.get(Player.class);
                    Position position = e.get(Position.class);

                    Node node = new Node(name.getName());
                    node.setLocalTranslation(position.getX(), position.getY(), position.getZ());

                    Spatial spatial = assetManager.loadModel(model.getFileName());

                    float scale = 0f;
                    Vector3f relativePosition = new Vector3f();
                    float radius = 0f;
                    float height = 0f;
                    float mass = 0f;
                    Map<String, List<Pair<String, LoopMode>>> animations = new HashMap<String, List<Pair<String, LoopMode>>>();

                    if (model.getFileName().equals(Constants.Models.SinbadFile)) {
                        scale = .27f;
                        relativePosition.set(0, 1.4f, 0);

                        radius = .7f;
                        height = 3f;
                        mass = 8f;

                        animations.put("Idle", new LinkedList<Pair<String, LoopMode>>());
                        animations.get("Idle").add(new Pair<String, LoopMode>("IdleTop", LoopMode.Loop));
                        animations.get("Idle").add(new Pair<String, LoopMode>("IdleBase", LoopMode.Loop));
                        animations.get("Idle").add(new Pair<String, LoopMode>("HandsRelaxed", LoopMode.Loop));

                        animations.put("Run", new LinkedList<Pair<String, LoopMode>>());
                        animations.get("Run").add(new Pair<String, LoopMode>("RunTop", LoopMode.Loop));
                        animations.get("Run").add(new Pair<String, LoopMode>("RunBase", LoopMode.Loop));
                        animations.get("Run").add(new Pair<String, LoopMode>("HandsClosed", LoopMode.Loop));

                    } else if (model.getFileName().equals(Constants.Models.JaimeFile)) {
                        scale = 1.50f;
                        relativePosition.set(0, 0, 0);

                        radius = .3f;
                        height = 2.5f;
                        mass = 8f;

                        animations.put("Idle", new LinkedList<Pair<String, LoopMode>>());
                        animations.get("Idle").add(new Pair<String, LoopMode>("Idle", LoopMode.Loop));

//                animations.put("Walk", new LinkedList<Pair<String, LoopMode>>());
//                animations.get("Walk").add(new Pair<String, LoopMode>("Walk", LoopMode.Loop));

                        animations.put("Run", new LinkedList<Pair<String, LoopMode>>());
                        animations.get("Run").add(new Pair<String, LoopMode>("Run", LoopMode.Loop));

                    }

                    spatial.setLocalScale(scale);
                    node.attachChild(spatial);
                    spatial.setLocalTranslation(relativePosition.clone());

                    BetterCharacterControl playerControl = new BetterCharacterControl(radius, height, mass);
                    physics.put(e.getId().getId(), playerControl);
                    node.addControl(playerControl);
                    physicsSpace.add(playerControl);

                    if (player.isLeader()) {
                        playerCamera = new ChaseCamera(simpleApp.getCamera(), node, simpleApp.getInputManager());
                        playerCamera.setSmoothMotion(true);
                        playerCamera.setChasingSensitivity(5);
                        node.addControl(new PlayerControl(true, simpleApp.getInputManager(), entitySet));
                    }

                    AnimationControl ac = new AnimationControl(e.getId(), entitySet, animations);
                    spatial.addControl(ac);

                    worldRootNode.attachChild(node);
                    spatials.put(e.getId().getId(), node);


                    return null;
                }
            });
        }
    }

    private void entitiesRemoved(Set<Entity> entities) {

        if (entities.isEmpty()) {
            return;
        }

        Iterator<Entity> models = entities.iterator();
        while (models.hasNext()) {
            final Entity e = models.next();

            if (!spatials.containsKey(e.getId().getId())) {
                continue;
            }
            this.simpleApp.enqueue(new Callable<Void>() {
                public Void call() throws Exception {

                    Model model = e.get(Model.class);

                    if (model.getFileName().equals(Constants.Models.SinbadFile)
                            || model.getFileName().equals(Constants.Models.OtoFile)
                            || model.getFileName().equals(Constants.Models.JaimeFile)) {
                        Spatial spatial = spatials.get(e.getId().getId());
                        physicsSpace.remove(physics.get(e.getId().getId()));
                        spatial.removeFromParent();
                    }
                    return null;
                }
            });
        }
    }

    private void entitiesChanged(Set<Entity> entities) {

        if (entities.isEmpty()) {
            return;
        }

        Iterator<Entity> models = entities.iterator();
        while (models.hasNext()) {
            final Entity e = models.next();

            if (!spatials.containsKey(e.getId().getId())) {
                continue;
            }
            this.simpleApp.enqueue(new Callable<Void>() {
                public Void call() throws Exception {
                    Movement mov = e.get(Movement.class);

                    if (mov != null) {
                        Spatial spatial = spatials.get(e.getId().getId());
                        BetterCharacterControl bcc = physics.get(e.getId().getId());
                        if (bcc != null) {
                            bcc.setWalkDirection(mov.getWalkDirection());
                            bcc.setViewDirection(mov.getViewDirection());
                            e.set(new Position(spatial.getLocalTranslation()));
                        }
                    }

                    return null;
                }
            });
        }
    }
}
