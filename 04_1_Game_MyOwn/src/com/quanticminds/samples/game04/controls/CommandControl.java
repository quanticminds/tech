/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quanticminds.samples.game04.controls;

import com.jme3.export.InputCapsule;
import com.jme3.export.JmeExporter;
import com.jme3.export.JmeImporter;
import com.jme3.export.OutputCapsule;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;
import com.jme3.scene.control.Control;
import com.quanticminds.samples.game04.WorldAppState;
import com.quanticminds.tech.commands.base.Command;
import com.simsilica.es.Entity;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;

/**
 *
 * @author adriano.ribeiro
 */
public class CommandControl extends AbstractControl {
    protected LinkedList<Command> commands = new LinkedList<Command>();
    protected Command defaultCommand;
    protected WorldAppState world;
    protected Entity entity;
    
    public CommandControl(WorldAppState world, Entity entity) {
        this.world = world;
        this.entity = entity;
    }

    public Command initCommand(Command command){
//        command.initialize(world, entity);
        return command;
    }
    
    public void addCommand(Command command){
//        command.setRunning(true);
//        for(int i =0; i < commands.size(); i++){
//            Command c = commands.get(i);
//            if(command.getPriority() < command.getPriority()){
//                commands.add(i,command);
//                return;
//            }
//        }
        commands.add(command);
    }
    
    public void removeCommand(Command command) {
//        command.setRunning(false);
        commands.remove(command);
    }

    public void clearCommands() {
        for (Iterator<Command> it = commands.iterator(); it.hasNext();) {
            Command command = it.next();
//            command.setRunning(false);
        }
        commands.clear();
    }
    
    @Override
    protected void controlUpdate(float tpf) {
        if (!enabled) {
            return;
        }
        for (Iterator<Command> it = commands.iterator(); it.hasNext();) {
            Command command = it.next();
            //do command and remove if returned true, else stop processing
//            Command.State commandState = command.update(tpf);
//            switch (commandState) {
//                case Sucess:
//                case Failure:
//                    command.setRunning(false);
//                    it.remove();
//                    break;
//                case Aborted:
//                    return;
//                case Running:
//                    break;
//            }
        }
    }
    
    @Override
    protected void controlRender(RenderManager rm, ViewPort vp) {
        //Only needed for rendering-related operations,
        //not called when spatial is culled.
    }
    
    public Control cloneForSpatial(Spatial spatial) {
        CommandControl control = new CommandControl(world, null);
        //TODO: copy parameters to new Control
        control.setSpatial(spatial);
        return control;
    }
    
    @Override
    public void read(JmeImporter im) throws IOException {
        super.read(im);
        InputCapsule in = im.getCapsule(this);
        //TODO: load properties of this Control, e.g.
        //this.value = in.readFloat("name", defaultValue);
    }
    
    @Override
    public void write(JmeExporter ex) throws IOException {
        super.write(ex);
        OutputCapsule out = ex.getCapsule(this);
        //TODO: save properties of this Control, e.g.
        //out.write(this.value, "name", defaultValue);
    }
}
