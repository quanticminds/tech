/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quanticminds.samples.game04.controls;

import com.jme3.bullet.control.BetterCharacterControl;
import com.jme3.export.InputCapsule;
import com.jme3.export.JmeExporter;
import com.jme3.export.JmeImporter;
import com.jme3.export.OutputCapsule;
import com.jme3.input.InputManager;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.AnalogListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;
import com.jme3.scene.control.Control;
import com.quanticminds.samples.game04.components.Movement;
import com.quanticminds.samples.game04.components.Player;
import com.simsilica.es.Entity;
import com.simsilica.es.EntitySet;
import java.io.IOException;
import java.util.Iterator;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

/**
 *
 * @author adriano
 */
public class PlayerControl extends AbstractControl implements AnalogListener {

    boolean activeControl;
    private boolean forward;
    private boolean backward;
    private boolean turnLeft;
    private boolean turnRight;
    private boolean jumping;
    float lastUpdate = 0f;
    private BetterCharacterControl playerControl;
    private EntitySet entitySet;
    private InputManager inputManager;
    private Vector3f walkDirection = new Vector3f(0, 0, 0);
    private Vector3f viewDirection = new Vector3f(1, 0, 0);
    private String[] actionNames = new String[]{"Player_Forward",
        "Player_Backward", "Player_Left",
        "Player_Right", "Player_Jump"};

    public PlayerControl(boolean activeControl, InputManager inputManager, EntitySet entitySet) {
        this.activeControl = activeControl;
        this.entitySet = entitySet;
        this.inputManager = inputManager;

//        registerPlayerActionInputs();
    }

    public boolean isActiveControl() {
        return activeControl;
    }

    public void setActiveControl(boolean activeControl) {
        this.activeControl = activeControl;
    }

    @Override
    public void setSpatial(Spatial spatial) {
        super.setSpatial(spatial);

        registerPlayerActionInputs();
        this.inputManager.addListener(this, actionNames);
        
//        playerControl = spatial.getControl(BetterCharacterControl.class);
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        
        if(enabled){
            registerPlayerActionInputs();
            this.inputManager.addListener(this, actionNames);
        }else{
            this.inputManager.removeListener(this);
        }
    }
    

    @Override
    protected void controlUpdate(float tpf) {
        if (isEnabled()) {

            if (isActiveControl()) {
               
                Iterator<Entity> entity = this.entitySet.iterator();

                while(entity.hasNext()){
                    Entity e = entity.next();
                    
                    if(!e.get(Player.class).isLeader()){
                        continue;
                    }

                    Movement mov = e.get(Movement.class);

                    Vector3f modelForwardDir = getSpatial().getWorldRotation().mult(Vector3f.UNIT_Z);

                    walkDirection.set(0, 0, 0);
                    viewDirection.set(modelForwardDir.normalize().clone());

                    if (forward) {
                        walkDirection.addLocal(modelForwardDir.mult(8));
                    } else if (backward) {
                        walkDirection.addLocal(modelForwardDir.negate().multLocal(8));
                    }

                    if (turnLeft) {
                        Quaternion rotateL = new Quaternion().fromAngleAxis(FastMath.PI * tpf, Vector3f.UNIT_Y);
                        rotateL.multLocal(viewDirection);
                    } else if (turnRight) {
                        Quaternion rotateR = new Quaternion().fromAngleAxis(-FastMath.PI * tpf, Vector3f.UNIT_Y);
                        rotateR.multLocal(viewDirection);
                    }

//                    if(forward || backward || turnLeft || turnRight){
//                        e.set(new Movement(walkDirection.getX(),walkDirection.getY(),walkDirection.getZ(),viewDirection.getX(),viewDirection.getY(),viewDirection.getZ()));
//                    }else if((walkDirection.length() == 0) && ( mov.getWalkDirection().length() != 0)){
                        e.set(new Movement(walkDirection.getX(),walkDirection.getY(),walkDirection.getZ(),viewDirection.getX(),viewDirection.getY(),viewDirection.getZ()));
//                    }
                    
//                    playerControl.setWalkDirection(walkDirection);
//                    playerControl.setViewDirection(viewDirection);
                    
//                    if(jumping){
//                        playerControl.jump();
//                    }                   
                }        

                turnLeft = turnRight = forward = backward = jumping = false;
            }       
        }
    }

    @Override
    protected void controlRender(RenderManager rm, ViewPort vp) {
        //Only needed for rendering-related operations,
        //not called when spatial is culled.
    }

    public Control cloneForSpatial(Spatial spatial) {
        throw new NotImplementedException();
    }

    @Override
    public void read(JmeImporter im) throws IOException {
        super.read(im);
        InputCapsule in = im.getCapsule(this);
        //TODO: load properties of this Control, e.g.
        //this.value = in.readFloat("name", defaultValue);
    }

    @Override
    public void write(JmeExporter ex) throws IOException {
        super.write(ex);
        OutputCapsule out = ex.getCapsule(this);
        //TODO: save properties of this Control, e.g.
        //out.write(this.value, "name", defaultValue);
    }
      

    public void onAnalog(String name, float value, float tpf) {

        if (name.equals("Player_Left")) {
            if (value != 0f) {
                turnLeft = true;
            } else {
                turnLeft = false;
            }
        }

        if (name.equals("Player_Right")) {
            if (value != 0f) {
                turnRight = true;
            } else {
                turnRight = false;
            }
        }

        if (name.equals("Player_Forward")) {
            if (value != 0f) {
                forward = true;
            } else {
                forward = false;
            }
        }

        if (name.equals("Player_Backward")) {
            if (value != 0f) {
                backward = true;
            } else {
                backward = false;
            }
        }

        if (name.equals("Player_Jump")) {
            if (value != 0f) {
                jumping = true;
            } else {
                jumping = false;
            }
        }
    }

    private void registerPlayerActionInputs() {

        this.inputManager.addMapping("Player_Forward", new KeyTrigger(KeyInput.KEY_I));
        this.inputManager.addMapping("Player_Backward", new KeyTrigger(KeyInput.KEY_M));
        this.inputManager.addMapping("Player_Left", new KeyTrigger(KeyInput.KEY_J));
        this.inputManager.addMapping("Player_Right", new KeyTrigger(KeyInput.KEY_L));
        this.inputManager.addMapping("Player_Jump", new KeyTrigger(KeyInput.KEY_SPACE));

//        this.inputManager.addListener(this, actionNames);
    }
}
