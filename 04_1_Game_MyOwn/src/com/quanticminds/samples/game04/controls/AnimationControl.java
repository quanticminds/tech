/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quanticminds.samples.game04.controls;

import com.jme3.animation.AnimChannel;
import com.jme3.animation.AnimControl;
import com.jme3.animation.AnimEventListener;
import com.jme3.animation.LoopMode;
import com.jme3.export.InputCapsule;
import com.jme3.export.JmeExporter;
import com.jme3.export.JmeImporter;
import com.jme3.export.OutputCapsule;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.SceneGraphVisitorAdapter;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;
import com.jme3.scene.control.Control;
import com.quanticminds.samples.game04.components.Movement;
import com.quanticminds.tech.utils.Pair;
import com.simsilica.es.Entity;
import com.simsilica.es.EntityId;
import com.simsilica.es.EntitySet;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

/**
 *
 * @author adriano
 */
public class AnimationControl extends AbstractControl implements AnimEventListener {

    private Set<String> currentAnimations = new HashSet<String>();
    private Map<String, List<Pair<String, LoopMode>>> animations = new HashMap<String, List<Pair<String, LoopMode>>>();
    private AnimControl animControl;
    private EntitySet entitySet;
    private EntityId entityId;

    public AnimationControl(EntityId entityId, EntitySet entitySet, Map<String, List<Pair<String, LoopMode>>> animations) {
        this.entitySet = entitySet;
        this.entityId = entityId;
        this.animations.putAll(animations);
    }

//    public void playAnimation(String spatialName, String animationName) {
////        this.animationName = animationName;
////        this.spatialName = spatialName;
//    }
    @Override
    public void setSpatial(Spatial spatial) {
        super.setSpatial(spatial);

//        SceneGraphVisitorAdapter visitor = new SceneGraphVisitorAdapter() {
//            @Override
//            public void visit(Geometry geom) {
//                super.visit(geom);
//                checkForAnimControl(geom);
//            }
//
//            @Override
//            public void visit(Node geom) {
//                super.visit(geom);
//                checkForAnimControl(geom);
//            }
//
//            private void checkForAnimControl(Spatial geom) {
//                AnimControl control = geom.getControl(AnimControl.class);
//                if (control == null) {
//                    return;
//                }
////                if (!animControl.containsKey(geom.getName())) {
////                    animControl.put(geom.getName(), control);
////                }
//            }
//        };
//        spatial.depthFirstTraversal(visitor);

//        ComponentFilter filter = new FieldFilter(Player.class, "leader", true);

    }

    @Override
    protected void controlUpdate(float tpf) {
        if (isEnabled()) {

            Entity e = this.entitySet.getEntity(entityId);
            Movement mov = e.get(Movement.class);
            
            String animationName = "Idle";
            int i = 0;

            if (mov.IsMoving()) {
                animationName = "Run";
            }

            if (this.currentAnimations.contains(animationName)) {
                return;
            }

            if(this.animControl == null){
                this.animControl = this.getSpatial().getControl(AnimControl.class);
            }
            
            this.animControl.clearChannels(); // checck possible cleanup of unused channels
            this.currentAnimations.clear();

            List<Pair<String, LoopMode>> anis = this.animations.get(animationName);
            while (i < anis.size()) {
                if ((animControl.getNumChannels() < anis.size())) {
                    animControl.createChannel();
                }

                Pair<String, LoopMode> ani = anis.get(i);
                animControl.getChannel(i).setAnim(ani.first);
                animControl.getChannel(i).setLoopMode(ani.second);
                i++;
            }

            this.currentAnimations.add(animationName);
        }
    }

    @Override
    protected void controlRender(RenderManager rm, ViewPort vp) {
        //Only needed for rendering-related operations,
        //not called when spatial is culled.
    }

    public Control cloneForSpatial(Spatial spatial) {
        throw new NotImplementedException();
    }

    @Override
    public void read(JmeImporter im) throws IOException {
        super.read(im);
        InputCapsule in = im.getCapsule(this);
        //TODO: load properties of this Control, e.g.
        //this.value = in.readFloat("name", defaultValue);
    }

    @Override
    public void write(JmeExporter ex) throws IOException {
        super.write(ex);
        OutputCapsule out = ex.getCapsule(this);
        //TODO: save properties of this Control, e.g.
        //out.write(this.value, "name", defaultValue);
    }

    public void onAnimCycleDone(AnimControl control, AnimChannel channel, String animName) {
    }

    public void onAnimChange(AnimControl control, AnimChannel channel, String animName) {
    }
}
