/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quanticminds.tech.commands.base;

/**
 *
 * @author adriano.ribeiro
 */
public abstract class CommandDecorator extends AbstractCommand{

    protected Command command;
    
    public CommandDecorator(DefaultWorld world, Command command) {
        this(world, command, "Command Decorated Name");        
    }
    
    public CommandDecorator(DefaultWorld world, Command command, String name) {
        super(world, name);
        
        this.command = command;
        this.command.getController().setCommand(this);        
    }

    @Override
    public boolean isConditionsOk(){
        return this.command.isConditionsOk();
    }

    @Override
    public void start(){
        this.command.start();
    }

    @Override
    public void finish(){
        this.command.finish();
    }

    @Override
    public CommandController getController() {
        return this.command.getController();
    }
}
