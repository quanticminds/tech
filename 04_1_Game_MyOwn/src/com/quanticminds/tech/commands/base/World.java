/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quanticminds.tech.commands.base;

/**
 *
 * @author adriano.ribeiro
 */
public interface World {

    void clear();
    
    boolean containsData(String dataKey);
            
    /**
     * query data from world shared data by key and Type
     * @param <T>
     * @param dataKey
     * @return return null if htere is not exist data for this key.
     */
    <T> T getData(String dataKey);

    /**
     * add or update new data to world shared structure.
     * @param dataKey
     * @param value
     */
    void setData(String dataKey, Object value);
    
}
