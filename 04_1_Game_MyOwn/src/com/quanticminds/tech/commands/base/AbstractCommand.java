/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quanticminds.tech.commands.base;

/**
 *
 * @author adriano.ribeiro
 */
public abstract class AbstractCommand implements Command{

    protected World world;    
    protected String name;
    protected CommandController controller;

    public AbstractCommand(World world) {
        this(world,"Command Name");        
    }
    
    public AbstractCommand(World world, String name) {
        this.world = world;
        this.name = name;
        this.controller = new CommandController(this);
    }
        
    
    public String getName() {
        return this.name;
    }
    
    public CommandController getController() {
        return this.controller;
    }
    
    public abstract boolean isConditionsOk();

    public abstract void start();

    public abstract void finish();
}
