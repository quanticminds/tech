/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quanticminds.tech.commands.base;

/**
 *
 * @author adriano.ribeiro
 */
public interface Command {

    /*
     * command name
     */
    public String getName();

    /*
     * commando pre-conditions for doAction
     */
    public boolean isConditionsOk();

    /*
     * command initialization
     */
    public void start();
    
    /**
     * command real user code.
     */
    public void doAction();
    
    /**
     * comand cleanup
     */
    public void finish();

    /**
     * command controller object
     * @return CommandController
     */
    public CommandController getController();
}
