/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quanticminds.tech.commands.base;

import java.util.Iterator;

/**
 *
 * @author adriano.ribeiro
 */
public class Selector extends AbstractMultiCommand {

    public Selector(World world) {
        super(world);
    }

    public Selector(World world, String name) {
        super(world, name);
    }
    
    public Command chooseNewCommand(){
        Iterator<Command> itr = this.controller.getCommands();
        while(itr.hasNext()){
            Command command = itr.next();
            if(command.isConditionsOk()){
               return command; 
            }
        }        
        return null;
    }

    @Override
    public void start() {
         Command command = chooseNewCommand();
         command.getController().commandStart();
         this.controller.setCommand(command);
         
    }
    
    @Override
    public void commandFailed() {
        Command command = chooseNewCommand();
        
        if(command == null){
            this.controller.finalizeWithFailure();
        }
    }

    @Override
    public void commandSucceeded() {
        this.controller.finalizeWithSuccess();
    }
}
