/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quanticminds.tech.commands.base;

import java.util.Iterator;
import java.util.LinkedList;

/**
 *
 * @author adriano.ribeiro
 */
public class MultiCommandController extends CommandController{
  
    protected LinkedList<Command> commands = new LinkedList<Command>();

    public MultiCommandController(Command command) {
        super(command);
    }
    
    public void addCommand(Command command){
        commands.add(command);
    }
    
    public int getCommandsCount(){
        return this.commands.size();
    }
    
    public Command getCommand(){
        return this.currentCommand;
    }
    
    public Command getFirstCommand(){
        return this.commands.getFirst();
    }
    
    public Iterator<Command> getCommands(){
        return this.commands.iterator();
    }
    
    public boolean hasNextCommand(){
        if((this.commands.indexOf(this.currentCommand)+1) < this.commands.size()){
            return true;
        }
        return false;
    }
        
    public Command getNextCommand(){
        int next = this.commands.indexOf(this.currentCommand)+1;
        if(next < this.commands.size()){
            Command command = this.commands.get(next);
            setCommand(command);
            return command;
        }else{
            return null;
        }
    }
           
    @Override
    public void reset(){
        super.reset();
        this.setCommand(this.commands.getFirst());
    }
}
