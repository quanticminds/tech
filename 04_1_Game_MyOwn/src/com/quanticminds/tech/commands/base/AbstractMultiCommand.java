/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quanticminds.tech.commands.base;

/**
 *
 * @author adriano.ribeiro
 */
public abstract class AbstractMultiCommand implements Command{

    private String name;
    protected World world;
    protected MultiCommandController controller;
    
    /**
     *
     */
    public AbstractMultiCommand(World world) {
        this(world,"Multi Commands");
    }
    
    /**
     *
     */ 
    public AbstractMultiCommand(World world, String name) {
        this.world = world;
        this.name = name;
        this.controller = new MultiCommandController(this);
    }
    
    /**
     *
     */ 
    public String getName(){
        return this.name;
    }
     
   /**
    *
    */    
    public CommandController getController() {
        return this.controller;
    }

   /**
    *
    */
    public boolean isConditionsOk(){
        return this.controller.getCommandsCount() > 0;
    }
    
   /**
    *
    */    
    public void doAction(){
        
        if(this.controller.isFinished()){
            return;
        }
        
        if(this.controller.getCommand() == null){
            return;
        }
        
        if(!this.controller.isStarted()){
            this.controller.commandStart();
        }
        
        CommandController commandController = this.controller.getCommand().getController();
        
        if(!commandController.isStarted()){
            commandController.commandStart();
        }
        
        this.controller.getCommand().doAction();

        if(commandController.isFinished()){

            commandController.commandFinish();
            if(commandController.isSucceded()){
            
                this.commandSucceeded();
            }else if(commandController.isFailed()){
                
                this.commandFailed();
            }
        }
    }
    
   /**
    *
    */    
    public void start(){

    }

   /**
    *
    */
    public void finish(){
        
    }
    
   /**
    *
    */
    public abstract void commandSucceeded();
    
   /**
    *
    */
    public abstract void commandFailed();
}
