/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quanticminds.tech.commands.base;

/**
 *
 * @author adriano.ribeiro
 */
public class Parallel extends AbstractMultiCommand {

    public Parallel(World world) {
        super(world);
    }

    public Parallel(World world, String name) {
        super(world, name);
    }

    @Override
    public void commandFailed() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void commandSucceeded() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
