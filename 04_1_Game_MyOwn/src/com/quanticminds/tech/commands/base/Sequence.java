/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quanticminds.tech.commands.base;

/**
 *
 * @author adriano.ribeiro
 */
public class Sequence extends AbstractMultiCommand{

    public Sequence(World world) {
        super(world);
    }

    public Sequence(World world, String name) {
        super(world, name);
    }

    @Override
    public void start() {
                
        this.controller.setCommand(this.controller.getFirstCommand());
        this.controller.getCommand().getController().commandStart();
    }    
    
        
    @Override
    public void commandFailed() {
        this.controller.finalizeWithFailure();
    }
    
    @Override
    public void commandSucceeded() {
        
        if(!this.controller.hasNextCommand()){
            this.controller.finalizeWithSuccess();
            
        }else{
            if(!this.controller.getNextCommand().isConditionsOk()){
                this.controller.finalizeWithFailure();
            }
        }
    }
}
