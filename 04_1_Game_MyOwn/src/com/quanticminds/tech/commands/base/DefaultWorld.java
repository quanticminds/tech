/* * 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quanticminds.tech.commands.base;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 *
 * @author adriano.ribeiro
 */
public class DefaultWorld implements World{

    /**
     * thread safe data storage.
     */
    private ConcurrentMap<String,Object> worldData = new ConcurrentHashMap<String, Object>();

    public boolean containsData(String dataKey){
        if(this.worldData.containsKey(dataKey)){
            return true;
        }        
        return false;
    }
    /**
     * add or update new data to world shared structure.
     * @param dataKey
     * @param value 
     */
    public void setData(String dataKey, Object value){
        this.worldData.put(dataKey, value);
    }
    
    /**
     * query data from world shared data by key and Type
     * @param <T>
     * @param dataKey
     * @return return null if htere is not exist data for this key.
     */
    public <T> T getData(String dataKey){
        return (T)this.worldData.get(dataKey);  
    }
    
    public void clear(){
        this.worldData.clear();
    }
}
