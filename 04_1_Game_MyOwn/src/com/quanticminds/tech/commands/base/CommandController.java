/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quanticminds.tech.commands.base;

/**
 *
 * @author adriano.ribeiro
 */
public class CommandController {

    /**
    * true is command is finished
    */
    private boolean finished;
    
    /** 
     * true command finish ok, false command failed
     */
    private boolean success;
    
    /**
     * command already running
     */
    protected boolean started;
    
    /**
     * real code to doAction
     */
    protected Command currentCommand;
    

    public CommandController(Command command) {
        this.finished = false;
        this.success = false;
        this.started = false;
        this.currentCommand = command;
    }
    
    /*
     * controlled init for command
     */
    public void commandStart(){
        this.started = true;
        this.currentCommand.start();
    }
    
    /*
     * controlled finish for command
     */
    public void commandFinish(){
        this.finished = false;
        this.started = false;
        this.currentCommand.finish();
    }

    /*
     * set new command to control
     */
    public void setCommand(Command command) {
        this.currentCommand = command;
    }
    
    /*
     * force command terminate with success
     */
    public void finalizeWithSuccess(){
        this.finished = true;
        this.success = true;
    }
    
    /*
     * force command terminate with failure
     */
    public void finalizeWithFailure(){
        this.finished = true;
        this.success = false;        
    }

    /*
     * command finish with success
     */
    public boolean isSucceded() {
        return this.success;
    }
    
    /*
     * command failed
     */
    public boolean isFailed() {
        return !this.success;
    }

    /*
     * command isn't running
     */
    public boolean isFinished() {
        return this.finished;
    }

    /*
     * command already initialized
     */
    public boolean isStarted() {
        return this.started;
    }
    
    /*
     * not initialized yet.
     */
    public void reset(){
        this.finished = false;
    }
}
