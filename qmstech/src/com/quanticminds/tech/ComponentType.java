//Copyright 2011 GAMADU.COM. All rights reserved.
//
//Redistribution and use in source and binary forms, with or without modification, are
//permitted provided that the following conditions are met:
//
//   1. Redistributions of source code must retain the above copyright notice, this list of
//      conditions and the following disclaimer.
//
//   2. Redistributions in binary form must reproduce the above copyright notice, this list
//      of conditions and the following disclaimer in the documentation and/or other materials
//      provided with the distribution.
//
//THIS SOFTWARE IS PROVIDED BY GAMADU.COM ``AS IS'' AND ANY EXPRESS OR IMPLIED
//WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
//FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL GAMADU.COM OR
//CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
//CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
//SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
//ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
//ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//The views and conclusions contained in the software and documentation are those of the
//authors and should not be interpreted as representing official policies, either expressed
//or implied, of GAMADU.COM.

package com.quanticminds.tech;

import java.util.HashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author adriano.ribeiro
 */
public class ComponentType {
	private static AtomicInteger INDEX = new AtomicInteger(0);

	private final int index;
	private final Class<? extends Component> type;

	private ComponentType(Class<? extends Component> type) {
		index = INDEX.incrementAndGet();
		this.type = type;
	}

	/**
     *
     * @return
     */
    public int getIndex() {
		return index;
	}
	
	@Override
	public String toString() {
		return "ComponentType["+type.getSimpleName()+"] ("+index+")";
	}

	private static HashMap<Class<? extends Component>, ComponentType> componentTypes = 
                new HashMap<Class<? extends Component>, ComponentType>();

	/**
     *
     * @param c
     * @return
     */
    public static ComponentType getTypeFor(Class<? extends Component> c) {
		ComponentType type = componentTypes.get(c);

		if (type == null) {
			type = new ComponentType(c);
			componentTypes.put(c, type);
		}

		return type;
	}

	/**
     *
     * @param c
     * @return
     */
    public static int getIndexFor(Class<? extends Component> c) {
		return getTypeFor(c).getIndex();
	}
}
