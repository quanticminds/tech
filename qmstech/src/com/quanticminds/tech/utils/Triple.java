/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quanticminds.tech.utils;

/**
 *
 * @param <T> 
 * @param <K> 
 * @param <L> 
 * @author adriano.ribeiro
 */
public class Triple<T,K,L> {
    /**
     * first element from data pair
     */
    public T first;
    /**
     * second element from data pair.
     */
    public K second;
    
    public L third;
    
    /**
     *
     * @param first
     * @param second
     */
    public Triple(T first, K second, L third){
        this.first = first;
        this.second = second;
        this.third = third;
    }    
}
