/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quanticminds.tech;

/**
 *
 * @author adriano.ribeiro
 */
public final class Entity {

    private Long id;
    private EntityDB db;

    /**
     *
     * @param id
     */
    public Entity(Long id, EntityDB db) {
        this.id = id;
        this.db = db;
    }

    
    /**
     *
     * @param <T>
     * @param controlType
     * @return
     */
    public <T extends Component> T getComponent(Class<T> controlType) {
        return db.getComponent(id, controlType);
    }
   
    /**
     *
     * @param component
     * @param type
     * @return
     */
    public void setComponent(Component component) {
        db.setComponent(id, component);
    }

    
    /**
     *
     * @param componentType
     */
    public void clearComponent(Class componentType) {
        db.clearComponent(id, componentType);
    }

    public void remove(){

    }
    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Entity) {
            Entity entity = (Entity) o;
            return entity.getId() == id;
        }
        return super.equals(o);
    }

    //very good explanation about hashCode 
    //http://stackoverflow.com/questions/4045063/how-should-i-map-long-to-int-in-hashcode
    @Override
    public int hashCode() {
        return (int) ((id >> 32) ^ id);
    }
}
