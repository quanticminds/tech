/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quanticminds.tech;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicLong;

/**
 *
 * @author adriano.ribeiro
 */
public class EntityDB {

    AtomicLong idx = new AtomicLong(0);

    //saved data
    ConcurrentMap<Integer, ConcurrentMap<Long, Component>> componentsByType =
            new ConcurrentHashMap<Integer, ConcurrentMap<Long, Component>>();
    //component changes
    ConcurrentMap<Integer, ConcurrentMap<Long, Component>> setupComponentsByType =
            new ConcurrentHashMap<Integer, ConcurrentMap<Long, Component>>();
    
    ConcurrentMap<Integer, ConcurrentMap<Long, Class>> clearComponentsByType =
            new ConcurrentHashMap<Integer, ConcurrentMap<Long, Class>>();
    
    //entities created or destroied
    ConcurrentMap<Long, Entity> changeEntities = 
            new ConcurrentHashMap<Long, Entity>();
    //entities running
    ConcurrentMap<Long, Entity> entities =
            new ConcurrentHashMap<Long, Entity>();

    public EntityDB() {
        setupComponentsByType = new ConcurrentHashMap<Integer, ConcurrentMap<Long, Component>>();
        clearComponentsByType = new ConcurrentHashMap<Integer, ConcurrentMap<Long, Class>>();

        // operations at entities level
        changeEntities = new ConcurrentHashMap<Long, Entity>();
    }

    /**
     *
     * @return
     */
    public Entity getNewEntity() {
        Entity e = new Entity(idx.incrementAndGet(), this);

        //TODO: check for id collisions
        entities.put(e.getId(), e);
        return e;
    }

    /**
     *
     * @param entityId
     * @return
     */
    public Entity getEntity(Long entityId) {
        return entities.get(entityId);
    }

    /**
     *
     * @param entityId
     */
    public void removeEntity(Long entityId) {
        Entity et = entities.get(entityId);
        changeEntities.put(et.getId(), et);
    }

    public <T extends Component> EntitySet<T> getComponents(Class<T> comp) {
        ComponentType type = ComponentType.getTypeFor(comp);
                
        EntitySet<T> comps = null;
        
        if(componentsByType.containsKey(type.getIndex())){
            comps = new EntitySet<T>(type, this, (ConcurrentMap<Long, T>)
                componentsByType.get(type.getIndex()));
        }
        return comps;
    }

    public <T extends Component> ConcurrentMap<Long,T> getComponents(ComponentType type) {
        ConcurrentMap<Long,T> comps = null;
        
        if(componentsByType.containsKey(type.getIndex())){
            comps = (ConcurrentMap<Long, T>)componentsByType.get(type.getIndex());
        }
        return comps;
    }
        
    /**
     *
     * @param <T>
     * @param entityId
     * @param controlType
     * @return
     */
    public <T extends Component> T getComponent(Long entityId, Class<T> controlType) {
        ComponentType type = ComponentType.getTypeFor(controlType);
        ConcurrentMap<Long, Component> comps = componentsByType.get(type.getIndex());
        if (comps != null) {
            return (T) comps.get(entityId);
        }
        return null;
    }

    public void setComponent(Long entityId, Component component) {
        ComponentType type = ComponentType.getTypeFor(component.getClass());


        if(!setupComponentsByType.containsKey(type.getIndex())){
            setupComponentsByType.put(type.getIndex(), 
                    new ConcurrentHashMap<Long, Component>());
        }
        setupComponentsByType.get(type.getIndex()).put(entityId, component);
    }

    /**
     *
     * @param entityId
     * @param componentClass
     */
    public void clearComponent(Long entityId, Class componentClass) {
        ComponentType type = ComponentType.getTypeFor(componentClass);

        if(!clearComponentsByType.containsKey(type.getIndex())){
            clearComponentsByType.put(type.getIndex(), 
                    new ConcurrentHashMap<Long, Class>());
        }

        clearComponentsByType.get(type.getIndex()).put(entityId, componentClass);
    }
    
    
    public void applySetComponent(Long entityId, ComponentType type, 
                            Component component) {
        if(!componentsByType.containsKey(type.getIndex())){
            ConcurrentMap<Long, Component> comps = componentsByType
                    .get(type.getIndex());
            
            if(comps == null){
                comps = new ConcurrentHashMap<Long, Component>();
                componentsByType.put(type.getIndex(), comps);
            }
            
            comps.put(entityId, component);
        }
    }

    
    public void applyClearComponent(Long entityId, ComponentType type) {
        if(!componentsByType.containsKey(type.getIndex())){
            ConcurrentMap<Long, Component> comps = componentsByType
                    .get(type.getIndex());
            if(comps.containsKey(entityId)){
                comps.remove(entityId);
            }
        }
    }

    
    
    /**
     *
     * @param component
     * @return
     */
    public List<Entity> getEntities(Class component) {
        ComponentType type = ComponentType.getTypeFor(component);

        ConcurrentMap<Long, Component> comps = componentsByType.get(type.getIndex());
        if (comps != null) {
            LinkedList<Entity> list = new LinkedList<Entity>();
            for (Map.Entry<Long, Component> e : comps.entrySet()) {
                list.add(new Entity(e.getKey(), this));
            }

            return list;
        }

        return null;
    }

    public boolean hasChanges() {
        return !setupComponentsByType.isEmpty() || !clearComponentsByType.isEmpty() ||
               !changeEntities.isEmpty();
    }

    public void processChanges() {

        processEntitiesDestroy();
        processEntitiesChanged();

    }

    private void processEntitiesDestroy() {

        Iterator<ConcurrentMap.Entry<Long, Entity>> itrChanges =
                changeEntities.entrySet().iterator();

        while (itrChanges.hasNext()) {
            Map.Entry<Long, Entity> etd = itrChanges.next();

            //actually destroy the entity
            Iterator<ConcurrentMap.Entry<Integer, ConcurrentMap<Long, Component>>> itrComps =
                    componentsByType.entrySet().iterator();

            while (itrComps.hasNext()) {
                Map.Entry<Integer, ConcurrentMap<Long, Component>> comp =
                        itrComps.next();

                if (comp.getValue().containsKey(etd.getKey())) {
                    comp.getValue().remove(etd.getKey());
                }
            }

            entities.remove(etd.getKey());

        }

        changeEntities.clear();
    }

    private void processEntitiesChanged() {

        //set components
        if (setupComponentsByType.isEmpty()){
            return;
        }
        
        Iterator<ConcurrentMap.Entry<Integer, 
                ConcurrentMap<Long, Component>>> itrSetup =
            setupComponentsByType.entrySet().iterator();

        while (itrSetup.hasNext()) {
            Map.Entry<Integer, ConcurrentMap<Long, Component>> cec =
                    itrSetup.next();

            //actually override components the entity
            if(!componentsByType.containsKey(cec.getKey())){
                componentsByType.put(cec.getKey(), new ConcurrentHashMap<Long, Component>());
            }
            componentsByType.get(cec.getKey()).putAll(cec.getValue());
        }

        setupComponentsByType.clear();
        

        //removed or unset components
        if (clearComponentsByType.isEmpty()){
            return;
        }
            
        Iterator<ConcurrentMap.Entry<Integer, 
                ConcurrentMap<Long, Class>>> itrClear =
            clearComponentsByType.entrySet().iterator();

        while (itrClear.hasNext()) {
            Map.Entry<Integer, ConcurrentMap<Long, Class>> cec =
                    itrClear.next();

            Iterator<ConcurrentMap.Entry<Long, Class>> itrEnts =
                    cec.getValue().entrySet().iterator();

            while (itrEnts.hasNext()) {
                Map.Entry<Long, Class> ent = itrEnts.next();
                
                if(componentsByType.containsKey(cec.getKey())){
                    componentsByType.get(cec.getKey()).remove(ent.getKey());
                }
            }
            clearComponentsByType.clear();
        }
    }
}
