/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quanticminds.tech;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 *
 * @author adriano.ribeiro
 */
public class EntitySet<T extends Component>
        extends ConcurrentHashMap<Long, T> {

    private ComponentType type;
    private EntityDB entityDB;
    private Map<Long, T> setChanges = new ConcurrentHashMap<Long, T>();
    private List<Long> clearChanges = new LinkedList<Long>();

    /**
     * create new empty instance to a specific aspect of and entity
     *
     * @param entityId
     * @param componentClass
     */
    public EntitySet(ComponentType type, EntityDB entityDB) {
        this.type = type;
        this.entityDB = entityDB;
    }

    /**
     * create new instance and feed with components of specific aspect of and
     * entity
     *
     * @param entityId
     * @param componentClass
     */
    public EntitySet(ComponentType type, EntityDB entityDB,
            ConcurrentMap<Long, T> components) {
        this(type, entityDB);

        this.putAll(components);
    }

    /**
     * return index of component this EntitySet
     */
    public int getTypeIndex() {
        return type.getIndex();
    }

    /**
     * return a component of an specific entity
     *
     * @param entityId
     */
    public T getComponent(Long entityId) {
        return (T) this.get(entityId);
    }

    public Iterator<Entry<Long, T>> getComponents() {
        this.clear();
        this.putAll((ConcurrentHashMap<Long, T>) this.entityDB.getComponents(type));
        return this.entrySet().iterator();
    }

    /**
     * true if there are components for processing
     * @return 
     */
    public boolean hasComponents() {
        return !this.isEmpty();
    }
    
    /**
     * set new value to specific aspect of an entity
     *
     * @param entityId
     * @param component 
     */
    public void setComponent(Long entityId, T component) {
        this.setChanges.put(entityId, component);
    }

    /**
     * remove a specfic aspect of an entity
     *
     * @param entityId
     * @param componentClass
     */
    public void clearComponent(Long entityId) {
        this.clearChanges.add(entityId);
    }

    public void applyChanges() {
        for (long entityId : this.clearChanges) {
            this.entityDB.applyClearComponent(entityId, type);
        }
        
        for (Map.Entry<Long, T> e : this.setChanges.entrySet()) {
            this.entityDB.applySetComponent(e.getKey(), type, e.getValue());
        }
    }
}
