/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quanticminds.tech;

/**
 *
 * @author adriano.ribeiro
 */
public final class Constants {
    public final class Operations{
        public static final int ENTITY_NEW = 1;        
        public static final int ENTITY_DESTROY = 2;
        public static final int COMPONENT_SETUP = 4;
        public static final int COMPONENT_CLEAR = 8;
    }    
}
