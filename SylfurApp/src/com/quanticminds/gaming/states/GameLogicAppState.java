/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quanticminds.gaming.states;

import com.jme3.animation.AnimChannel;
import com.jme3.animation.AnimControl;
import com.jme3.animation.AnimEventListener;
import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
//import com.jme3.app.state.VideoRecorderAppState;
import com.jme3.asset.AssetManager;
import com.jme3.asset.DesktopAssetManager;
import com.jme3.asset.plugins.ZipLocator;
import com.jme3.bullet.BulletAppState;
import com.jme3.input.InputManager;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import java.io.File;

/**
 *
 * @author Adriano Ribeiro
 */
public class GameLogicAppState extends AbstractAppState 
                                implements AnimEventListener {
    
    SimpleApplication app;
    Node rootNode; //mover isso pro State de visão do jogo.
    Node mapRootNode;
    Node playerNode;
    AssetManager assetManager;
    AppStateManager stateManager;
    BulletAppState bulletPhysics;
    InputManager inputManager;
//    VideoRecorderAppState videoRecordAppState;
    AnimChannel playerACH;
    AnimControl playerACO;
    
    String actionRecord = "Record";
    boolean running = false;
    String mapName;
    
    String assetScene;
    
    public GameLogicAppState(){
        
    }
    
    public GameLogicAppState(String assetScene){
        this.assetScene = assetScene;
    }
    
    @Override
    public void initialize(AppStateManager stateManager, Application app){
        super.initialize(stateManager,app);
        
        this.app = (SimpleApplication)app;
        this.app.setDisplayFps(true);
        this.app.setDisplayStatView(true);
        this.rootNode = this.app.getRootNode();
        this.mapRootNode = new Node();
        
        this.assetManager = this.app.getAssetManager();
        this.stateManager = this.app.getStateManager();
        this.inputManager = this.app.getInputManager();
        this.bulletPhysics = new BulletAppState();
        this.stateManager.attach(this.bulletPhysics);
        
//        videoRecordAppState = 
//                new VideoRecorderAppState(new File("SylfurApp.avi"));        
        initInputMapping();
    }
    
    @Override
    public void setEnabled(boolean enabled){
        super.setEnabled(enabled);
    }
    
    @Override
    public void update(float tpf){
        if(isEnabled()){
            
        }
    }
    /**
     * loads the specified level node
     * @param name
     */
    public void loadLevel(String name) {
        this.mapRootNode = (Node) assetManager.loadModel(name);
        this.bulletPhysics.getPhysicsSpace().addAll(rootNode);
    }
    
    public void loadPlayer(){
        // Load a model from test_data (OgreXML + material + texture)
        this.playerNode = (Node) assetManager.loadModel("Models/Ninja/Ninja.mesh.xml");
        this.playerNode.scale(0.05f, 0.05f, 0.05f);
        this.playerNode.rotate(0.0f, -3.0f, 0.0f);
        this.playerNode.setLocalTranslation(0.0f, 0.0f, -2.0f);
        this.mapRootNode.attachChild(playerNode);
        
        this.playerACO = playerNode.getControl(AnimControl.class);
        this.playerACO.addListener(this);
        this.playerACH = playerACO.createChannel();
        this.playerACH.setAnim("Idle2");
        
        this.bulletPhysics.getPhysicsSpace().addAll(this.playerNode);
    }

    /**
     * detaches the level and clears the cache
     */
    public void closeLevel() {
//        for (Iterator<PlayerData> it = PlayerData.getPlayers().iterator(); it.hasNext();) {
//            PlayerData playerData = it.next();
//            playerData.setData("entity_id", -1l);
//        }
//        if (isServer()) {
//            for (Iterator<PlayerData> it = PlayerData.getAIPlayers().iterator(); it.hasNext();) {
//                PlayerData playerData = it.next();
//                removePlayer(playerData.getId());
//            }
//        }
//        for (Iterator<Long> et = new LinkedList(entities.keySet()).iterator(); et.hasNext();) {
//            Long entry = et.next();
//            syncManager.removeObject(entry);
//        }
//        syncManager.clearObjects();
//        entities.clear();
//        newId = 0;
//        space.removeAll(this.mapRootNode);
        rootNode.detachChild(this.mapRootNode);
        ((DesktopAssetManager) assetManager).clearCache();
    }

    /**
     * preloads the models with the given names
     * @param modelNames
     */
    public void preloadModels(String[] modelNames) {
        for (int i = 0; i < modelNames.length; i++) {
            String string = modelNames[i];
            assetManager.loadModel(string);
        }
    }

    /**
     * creates the nav mesh for the loaded level
     */
    public void createNavMesh() {

//        Mesh mesh = new Mesh();
//
//        //version a: from mesh
//        GeometryBatchFactory.mergeGeometries(findGeometries(worldRoot, new LinkedList<Geometry>()), mesh);
//        Mesh optiMesh = generator.optimize(mesh);
//
//        navMesh.loadFromMesh(optiMesh);

        //TODO: navmesh only for debug
//        Geometry navGeom = new Geometry("NavMesh");
//        navGeom.setMesh(optiMesh);
//        Material green = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
//        green.setColor("Color", ColorRGBA.Green);
//        green.getAdditionalRenderState().setWireframe(true);
//        navGeom.setMaterial(green);

//        worldRoot.attachChild(navGeom);
    }

    /**
     * attaches the level node to the rootnode
     */
    public void attachLevel() {
        bulletPhysics.getPhysicsSpace().addAll(this.mapRootNode);
        rootNode.attachChild(this.mapRootNode);
    }
   
    void initInputMapping(){
//        this.inputManager.addMapping(actionRecord, 
//                new KeyTrigger(KeyInput.KEY_R));
//        this.inputManager.addListener(actionListener, actionRecord);
        
    }
    
    ActionListener actionListener = new ActionListener() {

        public void onAction(String name, boolean isPressed, float tpf) {
            if(name.equals(actionRecord) && isPressed){
//                stateManager.attach(videoRecordAppState);                    
            }
        }
    };
 
    @Override
    public void cleanup() {
        rootNode.detachChild(mapRootNode);
//        guiNode.detachChild(localGuiNode);
    }
    
    // AnimListener implementation
    public void onAnimCycleDone(AnimControl control,
                                AnimChannel channel,
                                String animName){
        
    }
    
    public void onAnimChange(AnimControl control,
                                AnimChannel channel,
                                String animName){
        
    }
}