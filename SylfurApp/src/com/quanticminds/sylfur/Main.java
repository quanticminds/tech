package com.quanticminds.sylfur;

import com.jme3.app.SimpleApplication;
import com.jme3.niftygui.NiftyJmeDisplay;
import com.jme3.renderer.RenderManager;
//import com.jme3.material.Material;
//import com.jme3.math.ColorRGBA;
//import com.jme3.math.Vector3f;
//import com.jme3.scene.Geometry;
//import com.jme3.scene.shape.Box;
import com.jme3.system.AppSettings;
import com.quanticminds.gaming.states.GameLogicAppState;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.render.TextRenderer;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;
import java.util.concurrent.Callable;

/**
 * test
 * @author normenhansen
 */
public class Main extends SimpleApplication  implements ScreenController {
    GameLogicAppState gameLogicState;
    private Nifty nifty;
    private NiftyJmeDisplay niftyDisplay;
    private TextRenderer statusText;
    
    public static void main(String[] args) {
        
        AppSettings settings = new AppSettings(true);
        settings.setResolution(1280,800);
        
//        settings.setFrameRate(Globals.SCENE_FPS);
//        settings.setRenderer(null);
        //FIXME: strange way of setting null audio renderer..
//        settings.setAudioRenderer(null);
//        for (int i = 0; i < args.length; i++) {
//            String string = args[i];
//            if ("-display".equals(string)) {
//                settings.setRenderer(AppSettings.LWJGL_OPENGL2);
//            }
//        }
        
        Main app = new Main();
        app.setSettings(settings);
        app.setShowSettings(false);
        app.start();
    }

    @Override
    public void simpleInitApp() {
        //gui system
        startNifty();
        
        flyCam.setMoveSpeed(100);
        // create initial AppState like GameMenu, the GameMenu call GameLogic
        // gameLogic start the game, loading, saved game or initial level and 
        // add player(s)
        // gameLogic will start Ai manager add NPC(s)
        // physics
        // create game level event dispatch
        // player configure their input actions and commands
        // player configure their restive camera

        gameLogicState=new GameLogicAppState();
        getStateManager().attach(gameLogicState);        
        //loadlevel is threaded and cares for OpenGL thread itself
        this.loadLevel("Scenes/town/main.j3o");
        
//        gameLogicState.loadMap("Scenes/town/main.j3o");

    }

    @Override
    public void simpleUpdate(float tpf) {
        //TODO: add update code
    }
//
//    @Override
//    public void simpleRender(RenderManager rm) {
//        //TODO: add render code
//    }

    public void bind(Nifty nifty, Screen screen) {

    }

    public void onStartScreen() {

    }

    public void onEndScreen() {

    }
    
    /**
     * loads a level, basically does everything on a seprate thread except
     * updating the UI and attaching the level
     * @param name
     * @param modelNames
     */
//    public void loadLevel(final String name, final String[] modelNames) {
    public void loadLevel(final String name) {
        statusText = nifty.getScreen("load_level")
                .findElementByName("layer").findElementByName("panel")
                .findElementByName("status_text")
                .getRenderer(TextRenderer.class);
        
        if (name.equals("null")) {
            enqueue(new Callable<Void>() {

                public Void call() throws Exception {
                        gameLogicState.closeLevel();
//                    worldManager.closeLevel();
//                    lobby();
                    return null;
                }
            });
            return;
        }
        new Thread(new Runnable() {

            public void run() {
                try {
                    enqueue(new Callable<Void>() {

                        public Void call() throws Exception {
                            nifty.gotoScreen("load_level");
                            statusText.setText("Loading Terrain..");
                            return null;
                        }
                    }).get();
                    gameLogicState.loadLevel(name);
                    gameLogicState.loadPlayer();
//                    worldManager.loadLevel(name);
//                    enqueue(new Callable<Void>() {
//
//                        public Void call() throws Exception {
//                            statusText.setText("Creating NavMesh..");
//                            return null;
//                        }
//                    }).get();
                    
//                    gameLogicState.createNavMesh();
////                    worldManager.createNavMesh();
//                    enqueue(new Callable<Void>() {
//
//                        public Void call() throws Exception {
//                            statusText.setText("Loading Models..");
//                            return null;
//                        }
//                    }).get();
//                    
////                    gameLogicState.preloadModels(modelNames);
////                    worldManager.preloadModels(modelNames);
                    enqueue(new Callable<Void>() {

                        public Void call() throws Exception {
                            gameLogicState.attachLevel();
//                            worldManager.attachLevel();
//                            statusText.setText("Done Loading!");
                            nifty.getScreen("load_level")
                                .findElementByName("layer").hide();
                            
//                            nifty.gotoScreen("default_hud");
//                            inputManager.setCursorVisible(false);
//                            chaseCam.setDragToRotate(false);
                            return null;
                        }
                    }).get();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
    
     /**
     * starts the nifty gui system
     */
    private void startNifty() {
        guiNode.detachAllChildren();
        guiNode.attachChild(fpsText);
        niftyDisplay = new NiftyJmeDisplay(assetManager,
                inputManager,
                audioRenderer,
                guiViewPort);
        nifty = niftyDisplay.getNifty();
        try {
            nifty.fromXml("Interface/ClientUI.xml", "start", this);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
//        statusText = nifty.getScreen("start").findElementByName("layer")
//                .findElementByName("panel")
//                .findElementByName("status_text")
//                .getRenderer(TextRenderer.class);
        guiViewPort.addProcessor(niftyDisplay);
    }
}
