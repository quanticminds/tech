package com.quanticminds.samples.game01;

import com.jme3.app.SimpleApplication;
import com.jme3.renderer.RenderManager;
import com.jme3.system.AppSettings;

/**
 * test
 * @author normenhansen
 */
public class GameApp extends SimpleApplication {
    
    WorldManager gameWorld;
    GameManager gameManager;
            
    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        GameApp app = new GameApp();
        AppSettings settings = new AppSettings(true);
//        settings.setResolution(640, 480);
        settings.setResolution(1360, 720);
        settings.setRenderer(AppSettings.LWJGL_OPENGL2);
        app.setSettings(settings);
        app.setShowSettings(false);
        app.start();
    }

    /**
     *
     */
    @Override
    public void simpleInitApp() {

        this.getStateManager().attach(new WorldManager());
        this.flyCam.setEnabled(false);    
        this.flyCam.setMoveSpeed(40);

        //        gameManager = new GameManager(this);   
        
//        this.enqueue(new Callable<Void>(){
//            public Void call()throws Exception{
//                gameWorld.loadLevel(Constants.Maps.TownFile);
//                gameWorld.attachLevel();
//                return null;
//            }
//        });
        
//        gameManager.startGame();
    }

    /**
     *
     * @param tpf
     */
    @Override
    public void simpleUpdate(float tpf) {
        //TODO: add update code
    }

    /**
     *
     * @param rm
     */
    @Override
    public void simpleRender(RenderManager rm) {
        //TODO: add render code
    }
    
    
}
