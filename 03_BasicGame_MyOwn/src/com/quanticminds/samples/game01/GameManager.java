/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quanticminds.samples.game01;

import com.jme3.app.SimpleApplication;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import java.util.concurrent.Callable;

/**
 *
 * @author adriano
 */
public class GameManager {

    WorldManager gameWorld;
    SimpleApplication app;

    /**
     *
     * @param app
     */
    public GameManager(SimpleApplication app) {
        this.app = app;
        this.gameWorld = app.getStateManager().getState(WorldManager.class);
    }

    /**
     *
     * @return
     */
    public synchronized boolean startGame() {

        app.enqueue(new Callable<Void>() {
            public Void call() throws Exception {
                gameWorld.loadLevel(Constants.Maps.TownFile);
                return null;
            }
        });
        app.enqueue(new Callable<Void>() {
            public Void call() throws Exception {
                gameWorld.attachLevel();
                return null;
            }
        });

//        app.enqueue(new Callable<Void>() {
//            public Void call() throws Exception {
//                EntityFactory.createPlayer("Player 01", new Vector3f(-3, -10, 2),
//                        new Vector3f(0, 0, 0));
//                return null;
//            }
//        });


        return true;
    }

    /**
     *
     * @param gameName
     * @return
     */
    public synchronized boolean loadGame(String gameName) {
        return false;
    }

    /**
     *
     * @param gameName
     * @return
     */
    public synchronized boolean saveGame(String gameName) {
        return false;
    }

    /**
     *
     * @return
     */
    public synchronized boolean removeGame() {
        return false;
    }
}
