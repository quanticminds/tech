/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quanticminds.samples.game01.systems.appstates;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.quanticminds.samples.game01.WorldManager;
import com.quanticminds.samples.game01.components.TimeDelayComponent;
import com.quanticminds.tech.EntitySet;
import java.util.Iterator;
import java.util.Map;

/**
 *
 * @author adriano
 */
public class TimeDelayAppState extends AbstractAppState {

    WorldManager world;

    /**
     *
     * @param stateManager
     * @param app
     */
    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);

        this.world = ((SimpleApplication) app)
                .getStateManager().getState(WorldManager.class);

    }

    @Override
    public void update(float tpf) {

        EntitySet<TimeDelayComponent> comps =
                this.world.getEntityDB().getComponents(TimeDelayComponent.class);

        if (comps == null) {
            return;
        }
        
        Iterator<Map.Entry<Long, TimeDelayComponent>> itr =
                comps.entrySet().iterator();
        while (itr.hasNext()) {
            Map.Entry<Long, TimeDelayComponent> e = itr.next();

            if (!e.getValue().isElapsedTimeOk()) {
                TimeDelayComponent td = new TimeDelayComponent(
                        e.getValue().getDelayTime(),
                        e.getValue().getElapsedTime() + tpf);

                this.world.getEntityDB().setComponent(e.getKey(), td);
            } else {
                this.world.getEntityDB().clearComponent(e.getKey(),
                        TimeDelayComponent.class);
            }
        }
    }
}
