/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quanticminds.samples.game01.systems.appstates;

import com.jme3.animation.LoopMode;
import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.quanticminds.samples.game01.WorldManager;
import com.quanticminds.samples.game01.components.AnimationComponent;
import com.quanticminds.samples.game01.components.MovementComponent;
import com.quanticminds.samples.game01.components.PlayerComponent;
import com.quanticminds.tech.EntitySet;
import com.quanticminds.tech.utils.Triple;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 *
 * @author adriano.ribeiro
 */
public class PlayerAppState extends AbstractAppState {

    private SimpleApplication simpleApp;
    private WorldManager world;

    /**
     *
     * @param stateManager
     * @param app
     */
    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
        this.simpleApp = (SimpleApplication) app;
        this.world = this.simpleApp.getStateManager()
                .getState(WorldManager.class);
        
//        EntitySet<PlayerComponent> comps = world.getEntityDB()
//                    .getComponents(PlayerComponent.class);
    }

    @Override
    public void update(float tpf) {

        if (isEnabled()) {

            // - alterações e adições
            EntitySet<AnimationComponent> comps = world.getEntityDB()
                    .getComponents(AnimationComponent.class);

            if (comps == null) {
                return;
            }

            Iterator<Map.Entry<Long, AnimationComponent>> itr =
                    comps.getComponents();

            while (itr.hasNext()) {
                Map.Entry<Long, AnimationComponent> e = itr.next();

                MovementComponent mv = this.world.getEntityDB()
                        .getComponent(e.getKey(), MovementComponent.class);

//                if ((mv != null) && (mv.getWalkDirection().length() > 0)) {
                if (mv != null) {
                    this.world.getEntityDB().setComponent(e.getKey(),
                            new AnimationComponent(
                            "Walk",
                            new Triple<String, LoopMode, Float>(
                                "RunTop", LoopMode.Loop, 0.15f),
                            new Triple<String, LoopMode, Float>(
                                "HandsClosed", LoopMode.Loop, 0.15f),
                            new Triple<String, LoopMode, Float>(
                                "RunBase", LoopMode.Loop, 0.15f)));

                } else {
                    this.world.getEntityDB().setComponent(e.getKey(),
                            new AnimationComponent(
                            "Idle",
                            new Triple<String, LoopMode, Float>(
                            "IdleTop", LoopMode.Loop, 0.15f),
                            new Triple<String, LoopMode, Float>(
                            "IdleBase", LoopMode.Loop, 0.15f)));

                }
            }
        }
    }

    @Override
    public void cleanup() {

    }
}
