/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quanticminds.samples.game01.systems.appstates;

import com.jme3.animation.AnimChannel;
import com.jme3.animation.AnimControl;
import com.jme3.animation.AnimEventListener;
import com.jme3.animation.LoopMode;
import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.PhysicsSpace;
import com.jme3.bullet.collision.PhysicsRayTestResult;
import com.jme3.bullet.collision.shapes.CapsuleCollisionShape;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.bullet.objects.PhysicsRigidBody;
import com.jme3.input.ChaseCamera;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Spatial;
import com.quanticminds.samples.game01.WorldManager;
import com.quanticminds.samples.game01.components.ActionComponent;
import com.quanticminds.samples.game01.components.AnimationComponent;
import com.quanticminds.samples.game01.components.CollisionComponent;
import com.quanticminds.samples.game01.components.MovementComponent;
import com.quanticminds.samples.game01.components.PlayerComponent;
import com.quanticminds.samples.game01.components.PositionComponent;
import com.quanticminds.samples.game01.components.VisualRepComponent;
import com.quanticminds.tech.Entity;
import com.quanticminds.tech.EntitySet;
import com.quanticminds.tech.utils.Triple;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Callable;

/**
 *
 * @author adriano.ribeiro
 */
public class RenderSyncAppState extends AbstractAppState
        implements AnimEventListener {

    private SimpleApplication app;
    private WorldManager world;
    private PhysicsSpace physics;
    private ChaseCamera playerCamera;
    private Map<Long, String> currentAnimation = new HashMap<Long, String>();
    private Map<Long, Spatial> sceneEntities = new HashMap<Long, Spatial>();
    private Map<Long, AnimControl> animControls = new HashMap<Long, AnimControl>();

    /**
     *
     * @param stateManager
     * @param app
     */
    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);

        this.app = (SimpleApplication) app;
        this.world = this.app.getStateManager()
                .getState(WorldManager.class);
        this.physics = this.app.getStateManager()
                .getState(BulletAppState.class).getPhysicsSpace();
    }

    @Override
    public void update(float tpf) {

        if (isEnabled()) {
            loadNewVisualizationEntities();
            updatePositionList();
            updateAnimations();
        }
    }

    @Override
    public void cleanup() {
        sceneEntities.clear();
    }

    /**
     *
     * @param control
     * @param channel
     * @param animName
     */
    public void onAnimCycleDone(AnimControl control, AnimChannel channel, String animName) {
//        throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     *
     * @param control
     * @param channel
     * @param animName
     */
    public void onAnimChange(AnimControl control, AnimChannel channel, String animName) {
//        throw new UnsupportedOperationException("Not supported yet.");
    }

    private void updateAnimations() {
        EntitySet<AnimationComponent> entities = this.world.getEntityDB()
                .getComponents(AnimationComponent.class);

        if (entities == null) {
            return;
        }

        Iterator<Entry<Long, AnimationComponent>> itr = entities.getComponents();

        while (itr.hasNext()) {
            Entry<Long, AnimationComponent> et = itr.next();
            AnimationComponent ac = et.getValue();

            if (currentAnimation.containsKey(et.getKey())
                    && currentAnimation.get(et.getKey())
                    .equals(ac.getAnimationName())) {
                continue;
            } else {
                currentAnimation.put(et.getKey(), ac.getAnimationName());
            }

            playAnimationsEntity(et.getKey(), ac.getAnimations());
        }
    }

    private void loadNewVisualizationEntities() {

        // - alterações e adições
        EntitySet<VisualRepComponent> comps = this.world.getEntityDB()
                .getComponents(VisualRepComponent.class);

        if (comps == null) {
            return;
        }

        Iterator<Entry<Long, VisualRepComponent>> itr = comps.getComponents();

        while (itr.hasNext()) {
            Entry<Long, VisualRepComponent> e = itr.next();


            if (!this.sceneEntities.containsKey(e.getKey())) {

                Long entityId = e.getKey();
                String modelFile = e.getValue().getAssetFile();

                PositionComponent pos =
                        this.world.getEntityDB().getComponent(e.getKey(),
                        PositionComponent.class);

                PlayerComponent localPlayer =
                        this.world.getEntityDB().getComponent(e.getKey(),
                        PlayerComponent.class);

                CollisionComponent coll =
                        this.world.getEntityDB().getComponent(e.getKey(),
                        CollisionComponent.class);

                AnimationComponent ac =
                        this.world.getEntityDB().getComponent(e.getKey(),
                        AnimationComponent.class);

                Spatial spatial = app.getAssetManager()
                        .loadModel(modelFile);

                spatial.setUserData("EntityID", entityId);
                spatial.setLocalScale(pos.getScale());

//                RigidBodyControl body = new RigidBodyControl(
//                        new CapsuleCollisionShape(
//                        coll.getRadius(),
//                        coll.getHeight()),
//                        coll.getMass());
//                body.setAngularFactor(0);
////                body.setKinematic(false);
//                body.setFriction(coll.getFriction());
//                body.setCollisionGroup(coll.getCollisionGroup());
//
                spatial.setLocalTranslation(pos.getLocation());
                spatial.setLocalRotation(pos.getRotation());
////                body.setKinematicSpatial(true);
////                body.setKinematic(false);
//
//                for (int cg : coll.getCollisionWith()) {
//                    body.addCollideWithGroup(cg);
//                }

                if (localPlayer != null) {
                    playerCamera = new ChaseCamera(app.getCamera(), spatial,
                            app.getInputManager());
                    playerCamera.setSmoothMotion(true);
                    playerCamera.setChasingSensitivity(5);
                }

                AnimControl animControl = spatial
                        .getControl(AnimControl.class);

                if (animControl != null) {
                    loadAnimationControl(entityId, animControl);

                    if (ac != null) {
                        currentAnimation.put(entityId, ac.getAnimationName());
                        playAnimationsEntity(entityId, ac.getAnimations());
                    }
                }

                world.getWorldRootNode().attachChild(spatial);
//                spatial.addControl(body);
//                body.setPhysicsLocation(pos.getLocation().clone());
//                body.setPhysicsRotation(pos.getRotation().clone());
//                physics.add(body);
                sceneEntities.put(entityId, spatial);
            }
        }
    }

    private void loadAnimationControl(Long entityId, AnimControl animationControl) {

        if (!animControls.containsKey(entityId)) {
            animControls.put(entityId, animationControl);
        }
    }

    private void playAnimationsEntity(Long entityId,
            Triple<String, LoopMode, Float>... animationDesc) {
        if (!animControls.containsKey(entityId)) {
            return;
        }

        AnimControl ac = animControls.get(entityId);

        while (ac.getNumChannels() < animationDesc.length) {
            ac.createChannel();
        }

        int i = 0;
        while (i < animationDesc.length) {
            if ((ac.getNumChannels() < animationDesc.length)) {
                ac.createChannel();
            }

            ac.getChannel(i).setAnim(animationDesc[i].first,
                    animationDesc[i].third);
            ac.getChannel(i).setLoopMode(animationDesc[i].second);
            i++;
        }
    }

    private void updatePositionList() {

        // - alterações e adições
        EntitySet<PositionComponent> comps =
                world.getEntityDB().getComponents(PositionComponent.class);

        if (comps == null) {
            return;
        }

        Iterator<Entry<Long, PositionComponent>> itr =
                comps.entrySet().iterator();

        while (itr.hasNext()) {
            Entry<Long, PositionComponent> e = itr.next();

            MovementComponent mv = this.world.getEntityDB()
                    .getComponent(e.getKey(), MovementComponent.class);

            CollisionComponent col = this.world.getEntityDB()
                    .getComponent(e.getKey(), CollisionComponent.class);

            if (this.sceneEntities.containsKey(e.getKey())) {
                Spatial spatial = this.sceneEntities.get(e.getKey());
//                RigidBodyControl body = spatial.getControl(RigidBodyControl.class);

                Vector3f location = e.getValue().getLocation().clone();
                Quaternion rotation = e.getValue().getRotation().clone();

                
                spatial.setLocalTranslation(location);
                spatial.setLocalRotation(rotation);
                
//                body.setPhysicsLocation(location);
//                body.setPhysicsRotation(rotation);
//                body.setLinearVelocity(location);
//                applyPhysicsTransform(spatial,location,rotation);

                ActionComponent acc =
                        this.world.getEntityDB().getComponent(e.getKey(),
                        ActionComponent.class);

//                if (acc != null) {
//                    Vector3f rotatedJumpForce = new Vector3f();
//                    rotatedJumpForce.set(new Vector3f(0, 5f, 0));
//                    body.applyImpulse(rot.multLocal(rotatedJumpForce), Vector3f.ZERO);
//                }
            }
        }
    }

    private void applyPhysicsTransform(Spatial spatial, Vector3f worldLocation, Quaternion worldRotation) {
        final Quaternion tmp_inverseWorldRotation = new Quaternion(); //private
        boolean applyLocal = false; //protected
        
        Vector3f localLocation = spatial.getLocalTranslation();
        Quaternion localRotationQuat = spatial.getLocalRotation();
        if (!applyLocal && spatial.getParent() != null) {
            localLocation.set(worldLocation).subtractLocal(spatial.getParent().getWorldTranslation());
            localLocation.divideLocal(spatial.getParent().getWorldScale());
            tmp_inverseWorldRotation.set(spatial.getParent().getWorldRotation()).inverseLocal().multLocal(localLocation);
            localRotationQuat.set(worldRotation);
            tmp_inverseWorldRotation.set(spatial.getParent().getWorldRotation()).inverseLocal().mult(localRotationQuat, localRotationQuat);

            spatial.setLocalTranslation(localLocation);
            spatial.setLocalRotation(localRotationQuat);
        } else {
            spatial.setLocalTranslation(worldLocation);
            spatial.setLocalRotation(worldRotation);
        }
    }
}
