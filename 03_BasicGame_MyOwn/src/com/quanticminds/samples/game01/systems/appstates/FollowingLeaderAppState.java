/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quanticminds.samples.game01.systems.appstates;

import com.jme3.ai.navmesh.NavMeshPathfinder;
import com.jme3.ai.navmesh.Path;
import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.util.TempVars;
import com.quanticminds.samples.game01.WorldManager;
import com.quanticminds.samples.game01.components.FollowLeaderComponent;
import com.quanticminds.samples.game01.components.MovementComponent;
import com.quanticminds.samples.game01.components.PlayerComponent;
import com.quanticminds.samples.game01.components.PositionComponent;
import com.quanticminds.tech.EntitySet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

/**
 *
 * @author adriano
 */
public class FollowingLeaderAppState extends AbstractAppState {

    private SimpleApplication simpleApp;
    private WorldManager world;
    private long activePlayerEntityId = 0;
    private Vector3f walkDirection = new Vector3f(0, 0, 0);
    private Vector3f lasPlayerPosition = new Vector3f(0, 0, 0);
    private Vector3f viewDirection = new Vector3f(1, 0, 0);
    private HashMap<Long, NavMeshPathfinder> navMeshPaths =
            new HashMap<Long, NavMeshPathfinder>();

    /**
     *
     * @param stateManager
     * @param app
     */
    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
        this.simpleApp = (SimpleApplication) app;
        this.world = this.simpleApp.getStateManager()
                .getState(WorldManager.class);
    }

    @Override
    public void update(float tpf) {
        if (isEnabled()) {

            updateActiveControledPlayer();

            PositionComponent playerPosition = this.world.getEntityDB()
                    .getComponent(this.activePlayerEntityId, PositionComponent.class);

            EntitySet<FollowLeaderComponent> followers = this.world.getEntityDB()
                    .getComponents(FollowLeaderComponent.class);

            if (followers == null) {
                return;
            }

            Iterator<Entry<Long, FollowLeaderComponent>> itr = followers.getComponents();

            while (itr.hasNext()) {

                Entry<Long, FollowLeaderComponent> e = itr.next();

                long entityId = e.getKey();
                FollowLeaderComponent flc = e.getValue();
                Vector3f start = new Vector3f();
                Vector3f finish = new Vector3f();

                start.set(flc.getStart());
                finish.set(flc.getFinish());

                PositionComponent myps = this.world.getEntityDB()
                        .getComponent(entityId, PositionComponent.class);

                NavMeshPathfinder npf = null;
                if (!navMeshPaths.containsKey(entityId)) {
                    navMeshPaths.put(entityId, new NavMeshPathfinder(this.world.getNavMesh()));
                    npf = navMeshPaths.get(entityId);
                    
                    start.set(myps.getLocation().clone());
                    npf.setPosition(start);

                    finish.set(playerPosition.getLocation().clone());
                    boolean pathOk = npf.computePath(finish);

                }else{
                    npf = navMeshPaths.get(entityId);
                }

                if (!finish.equals(playerPosition.getLocation())) {
                    start.set(myps.getLocation());
                    finish.set(playerPosition.getLocation());

                    npf.setPosition(start);
                    npf.computePath(finish);
                }

                followers.setComponent(entityId, new FollowLeaderComponent(
                        flc.getDistance(), start, finish));

                Path.Waypoint nextPoint = npf.getNextWaypoint();
                if (nextPoint == null) {
                    continue;
                }

                float angleY = myps.getLocation().normalize()
                        .angleBetween(playerPosition.getLocation().normalize());

                Quaternion rotate = new Quaternion();
                if (angleY > 0) {
                    //rotate.fromAngles(0, angleY, 0);
                    rotate.lookAt(nextPoint.getPosition().normalize(), Vector3f.UNIT_Y);
                }

                System.out.printf("id: %d angle: %f \n", entityId, angleY);
//                rotate.lookAt(nextPoint.getPosition().normalize(),Vector3f.UNIT_Y);

                if (myps.getLocation().distance(playerPosition.getLocation())
                        > flc.getDistance()) {
//                    walkDirection.set(myps.getLocation())
//                            .subtract(nextPoint.getPosition())
//                            .negate().mult(5f);
                    walkDirection.set(nextPoint.getPosition());

                    this.world.getEntityDB().setComponent(entityId,
                            new MovementComponent(
                            walkDirection,
                            rotate));
                } else {
                    this.world.getEntityDB().clearComponent(entityId, MovementComponent.class);
                }

                //persist data
                followers.applyChanges();
            }
        }
    }

    @Override
    public void cleanup() {
        super.cleanup();
    }

    private void updateActiveControledPlayer() {

        EntitySet<PlayerComponent> players = this.world.getEntityDB()
                .getComponents(PlayerComponent.class);

        if (players == null) {
            return;
        }

        Iterator<Entry<Long, PlayerComponent>> itr = players.getComponents();

        while (itr.hasNext()) {
            Entry<Long, PlayerComponent> e = itr.next();
            long entityId = e.getKey();
            PlayerComponent player = e.getValue();
            if (player.isControlActive() && (entityId != activePlayerEntityId)) {
                activePlayerEntityId = entityId;
                break;
            }
        }
    }
}
