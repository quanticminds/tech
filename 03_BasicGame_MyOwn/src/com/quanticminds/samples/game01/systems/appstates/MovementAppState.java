/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quanticminds.samples.game01.systems.appstates;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.PhysicsSpace;
import com.jme3.bullet.collision.shapes.CapsuleCollisionShape;
import com.jme3.bullet.objects.PhysicsRigidBody;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.quanticminds.samples.game01.WorldManager;
import com.quanticminds.samples.game01.components.ActionComponent;
import com.quanticminds.samples.game01.components.CollisionComponent;
import com.quanticminds.samples.game01.components.MovementComponent;
import com.quanticminds.samples.game01.components.PositionComponent;
import com.quanticminds.tech.EntitySet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

/**
 *
 * @author adriano.ribeiro
 */
public class MovementAppState extends AbstractAppState {

    private Vector3f walkDirection = new Vector3f(0, 0, 0);
    private Vector3f viewDirection = new Vector3f(0, 0, 1);
    private SimpleApplication simpleApp;
    private WorldManager world;
    private PhysicsSpace physicsSpace;
    private HashMap<Long,PhysicsRigidBody> physicsRigidBody = 
            new HashMap<Long, PhysicsRigidBody>();
    

    /**
     *
     * @param stateManager
     * @param app
     */
    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
        this.simpleApp = (SimpleApplication) app;
        this.world = this.simpleApp.getStateManager()
                .getState(WorldManager.class);
        this.physicsSpace = this.simpleApp.getStateManager()
                .getState(BulletAppState.class).getPhysicsSpace();
        
    }

    @Override
    public void update(float tpf) {
        if (isEnabled()) {
            
            updateMovement();
            
//            // - alterações e adições
//            EntitySet<MovementComponent> comps =
//                    world.getEntityDB().getComponents(MovementComponent.class);
//
//            if (comps == null) {
//                return;
//            }
//
//            Iterator<Entry<Long, MovementComponent>> itr =
//                    comps.entrySet().iterator();
//
//            while (itr.hasNext()) {
//                Entry<Long, MovementComponent> e = itr.next();
//
//                PositionComponent ps = this.world.getEntityDB()
//                        .getComponent(e.getKey(), PositionComponent.class);
//
//                Quaternion rot = ps.getRotation().clone();
//                if (e.getValue().getViewDirection() != null) {
//                    rot.multLocal(e.getValue().getViewDirection().clone());
//                }
//
//                this.world.getEntityDB().setComponent(e.getKey(),
//                        new PositionComponent(
//                        ps.getLocation().add(e.getValue().getWalkDirection()),
//                        rot,
//                        ps.getScale()));
//            }
        }
    }

    @Override
    public void cleanup() {
    }

    private void updateMovement(){
            
            // - alterações e adições
            EntitySet<PositionComponent> comps =
                    world.getEntityDB().getComponents(PositionComponent.class);

            if (comps == null) {
                return;
            }

            Iterator<Entry<Long, PositionComponent>> itr =
                    comps.entrySet().iterator();

            while (itr.hasNext()) {
                Entry<Long, PositionComponent> e = itr.next();
                
                long entityId = e.getKey();
                PositionComponent positionComponent = e.getValue();
                
                MovementComponent mv = this.world.getEntityDB()
                        .getComponent(entityId, MovementComponent.class);

                if(!physicsRigidBody.containsKey(entityId)){
                    CollisionComponent col = this.world.getEntityDB()
                        .getComponent(entityId, CollisionComponent.class);
                    
                    PhysicsRigidBody body = new PhysicsRigidBody(
                        new CapsuleCollisionShape(
                            col.getRadius(),
                            col.getHeight()),
                            col.getMass());
                    body.setAngularFactor(0);
//                    body.setFriction(col.getFriction());
                    body.setCollisionGroup(col.getCollisionGroup());

                    body.setPhysicsLocation(positionComponent.getLocation());
                    body.setPhysicsRotation(positionComponent.getRotation());


                    for (int cg : col.getCollisionWith()) {
                        body.addCollideWithGroup(cg);
                    }
                    this.physicsRigidBody.put(entityId, body);
                    this.physicsSpace.add(body);                
                }
                
                PhysicsRigidBody body =  this.physicsRigidBody.get(entityId);
                
                Quaternion rot = positionComponent.getRotation().clone();
                if (mv != null){
                    
                    if (!mv.getViewDirection().equals(Quaternion.ZERO)) {
                        rot.multLocal(mv.getViewDirection());
                    }
                    body.setLinearVelocity(mv.getWalkDirection());
                }                
                ActionComponent ac = this.world.getEntityDB()
                        .getComponent(entityId, ActionComponent.class);
                
                if((ac != null) && ac.getName().equals("FirstJump") && !ac.isJumped()){
                    body.applyImpulse(new Vector3f(0, 2*5, 0), Vector3f.ZERO);
                    body.setGravity(new Vector3f(0, -9.8f, 0));
                }
                
                Vector3f location = new Vector3f();
                body.getPhysicsLocation(location);
                this.world.getEntityDB().setComponent(entityId,
                        new PositionComponent(
                            location,
                            rot,
                            positionComponent.getScale()));
            }
    }
    
//    private final void calculateNewForward(Quaternion rotation, Vector3f direction, Vector3f worldUpVector) {
//        if (direction == null) {
//            return;
//        }
//        TempVars vars = TempVars.get();
//        Vector3f newLeft = vars.vect1;
//        Vector3f newLeftNegate = vars.vect2;
//
//        newLeft.set(worldUpVector).crossLocal(direction).normalizeLocal();
//        if (newLeft.equals(Vector3f.ZERO)) {
//            if (direction.x != 0) {
//                newLeft.set(direction.y, -direction.x, 0f).normalizeLocal();
//            } else {
//                newLeft.set(0f, direction.z, -direction.y).normalizeLocal();
//            }
////            logger.log(Level.INFO, "Zero left for direction {0}, up {1}", new Object[]{direction, worldUpVector});
//        }
//        newLeftNegate.set(newLeft).negateLocal();
//        direction.set(worldUpVector).crossLocal(newLeftNegate).normalizeLocal();
//        if (direction.equals(Vector3f.ZERO)) {
//            direction.set(Vector3f.UNIT_Z);
////            logger.log(Level.INFO, "Zero left for left {0}, up {1}", new Object[]{newLeft, worldUpVector});
//        }
//        if (rotation != null) {
//            rotation.fromAxes(newLeft, worldUpVector, direction);
//        }
//        vars.release();
//    }
//    
//    private void updateLocalCoordinateSystem() {
//        //gravity vector has possibly changed, calculate new world forward (UNIT_Z)
//        calculateNewForward(localForwardRotation, localForward, localUp);
//        localLeft.set(localUp).crossLocal(localForward);
//        rigidBody.setPhysicsRotation(localForwardRotation);
//        updateLocalViewDirection();
//    }
//
//    private void updateLocalViewDirection() {
//        //update local rotation quaternion to use for view rotation
//        localForwardRotation.multLocal(rotatedViewDirection.set(viewDirection));
//        calculateNewForward(rotation, rotatedViewDirection, localUp);
//    }
}
