/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quanticminds.samples.game01.systems.appstates;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.AnalogListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.quanticminds.samples.game01.WorldManager;
import com.quanticminds.samples.game01.components.ActionComponent;
import com.quanticminds.samples.game01.components.MovementComponent;
import com.quanticminds.samples.game01.components.PlayerComponent;
import com.quanticminds.samples.game01.components.PositionComponent;
import com.quanticminds.tech.EntitySet;
import java.util.Iterator;
import java.util.Map.Entry;

/**
 *
 * @author adriano
 */
public class PlayerInputAppState extends AbstractAppState
        implements AnalogListener {

    private SimpleApplication simpleApp;
    private WorldManager world;
    private SimpleApplication app;
    private boolean forward;
    private boolean backward;
    private boolean turnLeft;
    private boolean turnRight;
    private boolean jumping;
    private long activePlayerEntityId = 0;
    private Vector3f walkDirection = new Vector3f(0, 0, 0);
    private Vector3f viewDirection = new Vector3f(1, 0, 0);
    private String[] actionNames = new String[]{"Player_Forward",
        "Player_Backward", "Player_Left",
        "Player_Right", "Player_Jump"};

    /**
     *
     * @param stateManager
     * @param app
     */
    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
        this.simpleApp = (SimpleApplication) app;
        this.world = this.simpleApp.getStateManager()
                .getState(WorldManager.class);

        registerPlayerActionInputs();
    }

    @Override
    public void update(float tpf) {
        if (isEnabled()) {

            //checking active player changed
            EntitySet<PlayerComponent> players = this.world.getEntityDB()
                    .getComponents(PlayerComponent.class);

            if (players == null) {
                return;
            }

            Iterator<Entry<Long, PlayerComponent>> itr = players.getComponents();

            while (itr.hasNext()) {
                Entry<Long, PlayerComponent> e = itr.next();
                
                long entityId = e.getKey();
                PlayerComponent player = e.getValue();
                
                if (player.isControlActive() && (entityId != activePlayerEntityId)) {
                    
                    // zero is not valid Entity
                    if(activePlayerEntityId > 0){
                        players.setComponent(activePlayerEntityId, new PlayerComponent(false));
                        
                        //persist changes from EntitySet to Database
                        players.applyChanges();
                    }
                    activePlayerEntityId = entityId;
                    break;
                }
            }

            // Get current forward and left vectors of model by using its rotation
            // to rotate the unit vectors
            PositionComponent ps = this.world.getEntityDB()
                    .getComponent(this.activePlayerEntityId,
                    PositionComponent.class);

            walkDirection.set(0, 0, 0);


            Quaternion rotate = new Quaternion();
            if (turnLeft) {
                rotate.fromAngleAxis(FastMath.PI * tpf, Vector3f.UNIT_Y);
            } else if (turnRight) {
                rotate.fromAngleAxis(-FastMath.PI * tpf, Vector3f.UNIT_Y);
            }

            if (forward) {
                walkDirection.addLocal(ps.getRotation()
                        .mult(Vector3f.UNIT_Z).mult(10f));
            } else if (backward) {
                walkDirection.addLocal(ps.getRotation()
                        .mult(Vector3f.UNIT_Z).mult(10f).negate());
            }

            if (turnLeft || turnRight || forward || backward) {
                this.world.getEntityDB().setComponent(this.activePlayerEntityId,
                        new MovementComponent(
                        walkDirection.clone(),
                        rotate));
            } else {
                this.world.getEntityDB()
                        .clearComponent(this.activePlayerEntityId,
                        MovementComponent.class);
            }

            if (jumping) {

                ActionComponent acc = this.world.getEntityDB()
                        .getComponent(activePlayerEntityId,
                        ActionComponent.class);

                if (acc != null) {
                    if (acc.getName().equals("FirstJump") && !acc.isJumped()) {
                        this.world.getEntityDB().setComponent(this.activePlayerEntityId,
                                new ActionComponent("FirstJump", true));
                    } else {
                        this.world.getEntityDB()
                                .clearComponent(this.activePlayerEntityId,
                                ActionComponent.class);
                    }
                } else {
                    this.world.getEntityDB().setComponent(this.activePlayerEntityId,
                            new ActionComponent("FirstJump", false));
                }
            }

            turnLeft = turnRight = forward = backward = jumping = false;

            players.applyChanges();
        }
    }

    @Override
    public void cleanup() {
        super.cleanup();
    }

    public void onAnalog(String name, float value, float tpf) {

        if (name.equals("Player_Left")) {
            if (value != 0f) {
                turnLeft = true;
            } else {
                turnLeft = false;
            }
        }

        if (name.equals("Player_Right")) {
            if (value != 0f) {
                turnRight = true;
            } else {
                turnRight = false;
            }
        }

        if (name.equals("Player_Forward")) {
            if (value != 0f) {
                forward = true;
            } else {
                forward = false;
            }
        }

        if (name.equals("Player_Backward")) {
            if (value != 0f) {
                backward = true;
            } else {
                backward = false;
            }
        }

        if (name.equals("Player_Jump")) {
            if (value != 0f) {
                jumping = true;
            } else {
                jumping = false;
            }
        }
    }

    private void registerPlayerActionInputs() {

        this.simpleApp.getInputManager().addMapping("Player_Forward",
                new KeyTrigger(KeyInput.KEY_I));
        this.simpleApp.getInputManager().addMapping("Player_Backward",
                new KeyTrigger(KeyInput.KEY_M));
        this.simpleApp.getInputManager().addMapping("Player_Left",
                new KeyTrigger(KeyInput.KEY_J));
        this.simpleApp.getInputManager().addMapping("Player_Right",
                new KeyTrigger(KeyInput.KEY_L));
        this.simpleApp.getInputManager().addMapping("Player_Jump",
                new KeyTrigger(KeyInput.KEY_SPACE));

        this.simpleApp.getInputManager().addListener(this, actionNames);
    }
}
