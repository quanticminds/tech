/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quanticminds.samples.game01;

import com.jme3.ai.navmesh.NavMesh;
import com.jme3.app.AppTask;
import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.asset.AssetManager;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.quanticminds.samples.game01.systems.appstates.AnimationAppState;
import com.quanticminds.samples.game01.systems.appstates.FollowingLeaderAppState;
import com.quanticminds.samples.game01.systems.appstates.MovementAppState;
import com.quanticminds.samples.game01.systems.appstates.PlayerInputAppState;
import com.quanticminds.samples.game01.systems.appstates.RenderSyncAppState;
import com.quanticminds.samples.game01.systems.appstates.TimeDelayAppState;
import com.quanticminds.tech.EntityDB;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 *
 * @author adriano.ribeiro
 */
public class WorldManager extends AbstractAppState {

    BulletAppState physicsSpace;
    SimpleApplication app;
    Node worldRootNode = new Node("WorldRootNode");
    TimeDelayAppState timeDelaySystem;
    RenderSyncAppState renderSync;
    MovementAppState movement;
    EntityDB entityDB;
    NavMesh navMesh;
                
    final ConcurrentLinkedQueue<AppTask<?>> taskQueue =
            new ConcurrentLinkedQueue<AppTask<?>>();

    /**
     *
     * @return
     */
    public Node getWorldRootNode() {
        return worldRootNode;
    }

    /**
     *
     * @return
     */
    public AssetManager getAssetManager() {
        return this.app.getAssetManager();
    }
    
    /**
     *
     * @return
     */
    public EntityDB getEntityDB(){
        return this.entityDB;
    }
    
    /**
     *
     * @return
     */
    public NavMesh getNavMesh(){
        return navMesh;
    }

    /**
     *
     * @param stateManager
     * @param app
     */
    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
        
        this.entityDB = new EntityDB();
        this.app = (SimpleApplication) app;
        this.physicsSpace = new BulletAppState();
        
        //physics app state with debug enable
        this.app.getStateManager().attach(physicsSpace);
        this.physicsSpace.getPhysicsSpace().enableDebug(this.getAssetManager());
//        this.physicsSpace.setDebugEnabled(true);

        //systems
        this.app.getStateManager().attach(new TimeDelayAppState());
        this.app.getStateManager().attach(new PlayerInputAppState());
        this.app.getStateManager().attach(new FollowingLeaderAppState());
        this.app.getStateManager().attach(new AnimationAppState());
        this.app.getStateManager().attach(new MovementAppState());
        this.app.getStateManager().attach(new RenderSyncAppState());
        
        loadLevel(Constants.Maps.TownFile);
        attachLevel();
        
        EntityFactory.createPlayer(entityDB, "Player 01", 
                new Vector3f(0f,-9.5f,0f), Quaternion.DIRECTION_Z);
        
        for(int i =0; i < 2; i++){
            EntityFactory.createSinbadNPC(entityDB, String.format("NPC Sinbad %2d",i), 
                new Vector3f((i*5) - 30f, -9.5f, 20f), Quaternion.DIRECTION_Z);
        }

        for(int i =0; i < 5; i++){
            EntityFactory.createOtoNPC(entityDB, String.format("NPC Oto %2d",i), 
                new Vector3f((i*5) - 30f, -9.5f, -20f), Quaternion.DIRECTION_Z);
        }    
    }

    @Override
    public void update(float tpf) {
        if(isEnabled()){
            
            if(entityDB.hasChanges()){
                entityDB.processChanges();
            }
        }
    }

    @Override
    public void cleanup() {
        super.cleanup();

        this.unloadLevel();
        this.detachLevel();
    }

    /**
     *
     * @param map
     */
    public void loadLevel(String map) {
        final String mapFile = map;

        app.enqueue(new Callable<Void>(){
            public Void call() throws Exception{
                worldRootNode = (Node)app.getAssetManager().loadModel(mapFile);
                return null;
            }
        });        
    }

    /**
     *
     */
    public void unloadLevel() {
        this.worldRootNode.detachAllChildren();
    }

    /**
     *
     */
    public void attachLevel() {
        app.enqueue(new Callable<Void>(){
            public Void call() throws Exception{
                
                Geometry navGeo = (Geometry)worldRootNode.getChild("NavMesh");
                navMesh = new NavMesh(navGeo.getMesh());
                
                app.getRootNode().attachChild(worldRootNode);
                RigidBodyControl rb = new RigidBodyControl(0);
                rb.setCollisionGroup(Constants.CollisionGroups.Ground);
                worldRootNode.addControl(rb);
                physicsSpace.getPhysicsSpace().add(rb);
                return null;
            }
        });        
    }

    /**
     *
     */
    public void detachLevel() {
        this.app.getRootNode().detachChild(this.worldRootNode);
        RigidBodyControl rb = this.worldRootNode.getControl(RigidBodyControl.class);

        if (rb != null) {
            this.physicsSpace.getPhysicsSpace().remove(rb);
        }
    }
   

//    /**
//     * Enqueues a task/callable object to execute in the jME3 rendering thread.
//     * <p> Callables are executed right at the beginning of the main loop. They
//     * are executed even if the application is currently paused or out of focus.
//     */
//    public <V> Future<V> enqueue(Callable<V> callable) {
//        AppTask<V> task = new AppTask<V>(callable);
//        taskQueue.add(task);
//        return task;
//    }
//
//    /**
//     * Runs tasks enqueued via {@link #enqueue(Callable)}
//     */
//    private void runQueuedTasks() {
//        AppTask<?> task;
//        while ((task = taskQueue.poll()) != null) {
//            if (!task.isCancelled()) {
//                task.invoke();
//            }
//        }
//    }
}
