package com.quanticminds.samples.game01;

import com.jme3.animation.LoopMode;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.quanticminds.samples.game01.components.AnimationComponent;
import com.quanticminds.samples.game01.components.CollisionComponent;
import com.quanticminds.samples.game01.components.FollowLeaderComponent;
import com.quanticminds.samples.game01.components.InformationComponent;
import com.quanticminds.samples.game01.components.EnemyComponent;
import com.quanticminds.samples.game01.components.PlayerComponent;
import com.quanticminds.samples.game01.components.PositionComponent;
import com.quanticminds.samples.game01.components.TimeDelayComponent;
import com.quanticminds.samples.game01.components.VisualRepComponent;
import com.quanticminds.tech.Entity;
import com.quanticminds.tech.EntityDB;
import com.quanticminds.tech.utils.Triple;

/**
 *
 * @author adriano
 */
public class EntityFactory {

    /**
     *
     * @param db
     * @param name
     * @param position
     * @param rotation
     * @return
     */
    public static Entity createPlayer(EntityDB db, String name, 
                    Vector3f position, Quaternion rotation) {
        Entity e = db.getNewEntity();

        e.setComponent(new TimeDelayComponent(
                4, //time to go in seconds
                0));
        e.setComponent(new InformationComponent(
                name));
        e.setComponent(new PlayerComponent(
                true));
        e.setComponent(new AnimationComponent(
                    "Idle",
                    new Triple<String, LoopMode, Float>(
                        "IdleTop", LoopMode.Loop,0.15f
                    ),
                    new Triple<String, LoopMode, Float>(
                        "IdleBase", LoopMode.Loop,0.15f
                    )
                ));
        e.setComponent(new PositionComponent(
                position,
                rotation,
                new Vector3f(0.37f, 0.37f, 0.37f)
                ));
        e.setComponent(new VisualRepComponent(
                Constants.Models.SinbadFile));

        e.setComponent(new CollisionComponent(
                Constants.CollisionGroups.Players,
                new int[]{
                    Constants.CollisionGroups.Ground,
                    Constants.CollisionGroups.Enemies
                },
                2f,
                1f,
                1f,
                11f));

        return e;
    }

    /**
     *
     * @param db
     * @param name
     * @param position
     * @param rotation
     * @return
     */
    public static Entity createSinbadNPC(EntityDB db, String name, 
                Vector3f position, Quaternion rotation) {
        Entity e = db.getNewEntity();
        e.setComponent(new InformationComponent(
                name));
        e.setComponent(new TimeDelayComponent(
                8, //time to go in seconds
                0));

        e.setComponent(new PositionComponent(
                position,
                rotation,
                new Vector3f(0.37f, 0.37f, 0.37f)
                ));
        
        e.setComponent(new EnemyComponent());
        
        e.setComponent(new AnimationComponent(
                    "Idle",
                    new Triple<String, LoopMode, Float>(
                        "IdleTop", LoopMode.Loop,0.15f
                    ),
                    new Triple<String, LoopMode, Float>(
                        "IdleBase", LoopMode.Loop,0.15f
                    )
                ));        
        e.setComponent(new VisualRepComponent(
                Constants.Models.SinbadFile));

        e.setComponent(new CollisionComponent(
                Constants.CollisionGroups.Enemies,
                new int[]{
                    Constants.CollisionGroups.Ground,
                    Constants.CollisionGroups.Players
                },
                2f,
                1f,
                8f,
                11f));
        e.setComponent(new FollowLeaderComponent(
                    7,
                    Vector3f.ZERO,
                    Vector3f.ZERO
                ));
        return e;
    }
    
    /**
     *
     * @param db
     * @param name
     * @param position
     * @param rotation
     * @return
     */
    public static Entity createOtoNPC(EntityDB db, String name, 
                Vector3f position, Quaternion rotation) {
        Entity e = db.getNewEntity();
        e.setComponent(new InformationComponent(
                name));
        
        e.setComponent(new TimeDelayComponent(
                12, //time to go in seconds
                0));

        e.setComponent(new PositionComponent(
                position,
                rotation,
                new Vector3f(0.37f, 0.37f, 0.37f)
                ));
        
        e.setComponent(new EnemyComponent());
        
        e.setComponent(new AnimationComponent(
                    "Idle",
                    new Triple<String, LoopMode, Float>(
                        "stand", LoopMode.Loop,0.15f
                    )
                ));        
        e.setComponent(new VisualRepComponent(
                Constants.Models.OtoFile));

        e.setComponent(new CollisionComponent(
                Constants.CollisionGroups.Enemies,
                new int[]{
                    Constants.CollisionGroups.Ground,
                    Constants.CollisionGroups.Players
                },
                2f,
                1f,
                10f,
                11f));

        return e;
    }
}
