/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quanticminds.samples.game01;

/**
 *
 * @author adriano.ribeiro
 */
public final class Constants {
    /**
     *
     */
    public final class Models{
        /**
         *
         */
        public static final String SinbadFile = "Models/Sinbad/Sinbad.mesh.j3o";
        /**
         *
         */
        public static final String OtoFile = "Models/Oto/Oto.mesh.j3o";
    }
    /**
     *
     */
    public final class Maps{
        /**
         *
         */
        public static final String TownFile = "Scenes/city.j3o";
    }
    /**
     *
     */
    public final class CollisionGroups{
        /**
         *
         */
        public static final int Ground = 1;
        /**
         *
         */
        public static final int Players = 2;
        /**
         *
         */
        public static final int PlayerShoot = 3;
        /**
         *
         */
        public static final int Enemies = 4;
        /**
         *
         */
        public static final int Doors = 5;
        /**
         *
         */
        public static final int Triggers = 6;
    }
    
}
