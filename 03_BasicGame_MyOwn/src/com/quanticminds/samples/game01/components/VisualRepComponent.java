/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quanticminds.samples.game01.components;

import com.quanticminds.tech.Component;

/**
 *
 * @author adriano.ribeiro
 */
public class VisualRepComponent extends Component{

    String assetFile;

    /**
     *
     * @param assetFile
     */
    public VisualRepComponent(String assetFile) {
        this.assetFile = assetFile;
    }

    /**
     *
     * @return
     */
    public String getAssetFile() {
        return assetFile;
    }
}
