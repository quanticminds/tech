/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quanticminds.samples.game01.components;

import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.quanticminds.tech.Component;

/**
 *
 * @author adriano.ribeiro
 */
public class PositionComponent extends Component{

    Vector3f location  = new Vector3f();
    Quaternion rotation = new Quaternion();
    Vector3f scale  = new Vector3f();

    /**
     *
     * @param location
     * @param rotation
     * @param scale
     */
    public PositionComponent(Vector3f location, Quaternion rotation, 
                            Vector3f scale) {
        this.location.set(location);
        this.rotation.set(rotation);
        this.scale.set(scale);
    }

    /**
     *
     * @return
     */
    public Quaternion getRotation() {
        return rotation;
    }

    /**
     *
     * @return
     */
    public Vector3f getLocation() {
        return location;
    }

    /**
     *
     * @return
     */
    public Vector3f getScale() {
        return scale;
    }
}
