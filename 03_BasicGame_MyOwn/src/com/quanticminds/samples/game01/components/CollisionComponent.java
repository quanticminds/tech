/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quanticminds.samples.game01.components;

import com.jme3.math.Vector3f;
import com.quanticminds.tech.Component;

/**
 *
 * @author adriano
 */
public class CollisionComponent extends Component{
   
    //capsule shape
    int collisionGroup;
    int[] collisionWith;
    float height;
    float radius;
    float mass;
    float friction;
   

    /**
     *
     * @param collisionGroup
     * @param collisionWith
     * @param height
     * @param radius
     * @param mass
     * @param friction
     */
    public CollisionComponent(int collisionGroup, int[] collisionWith, float height, 
                            float radius, float mass, float friction) {
        this.collisionGroup = collisionGroup;
        this.collisionWith = collisionWith;
        this.height = height;
        this.radius = radius;
        this.mass = mass;
        this.friction = friction;
    }

    /**
     *
     * @return
     */
    public int getCollisionGroup() {
        return collisionGroup;
    }

    /**
     *
     * @return
     */
    public int[] getCollisionWith() {
        return collisionWith;
    }

    /**
     *
     * @return
     */
    public float getHeight() {
        return height;
    }

    /**
     *
     * @return
     */
    public float getRadius() {
        return radius;
    }
    
    /**
     *
     * @return
     */
    public float getMass() {
        return mass;
    }

    /**
     *
     * @return
     */
    public float getFriction() {
        return friction;
    }
}
