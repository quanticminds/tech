/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quanticminds.samples.game01.components;

import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.quanticminds.tech.Component;

/**
 *
 * @author adriano.ribeiro
 */
public class MovementComponent extends Component{

    Vector3f walkDirection  = new Vector3f();
    Quaternion viewDirection = new Quaternion();

    /**
     *
     * @param walkDirection
     * @param viewDirection
     * @param jumping
     */
    public MovementComponent(Vector3f walkDirection, Quaternion viewDirection) {
        this.walkDirection.set(walkDirection);
        this.viewDirection.set(viewDirection);
    }

    /**
     *
     * @return
     */
    public Quaternion getViewDirection() {
        return viewDirection;
    }

    /**
     *
     * @return
     */
    public Vector3f getWalkDirection() {
        return walkDirection;
    }
}
