/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quanticminds.samples.game01.components;

import com.jme3.animation.LoopMode;
import com.quanticminds.tech.Component;
import com.quanticminds.tech.utils.Triple;


/**
 *
 * @author Ariano Ribeiro
 */
public class AnimationComponent extends Component{
    
    String animationName;
    Triple<String, LoopMode, Float>[] animations;

    /**
     *
     * @param animationName
     * @param animations
     */
    public AnimationComponent(String animationName, Triple<String, LoopMode, Float> ... animations) {
        this.animationName = animationName;
        this.animations = animations;
    }

    /**
     *
     * @return
     */
    public Triple<String, LoopMode, Float>[] getAnimations() {
        return animations;
    }

    /**
     *
     * @return
     */
    public String getAnimationName() {
        return animationName;
    }
}
