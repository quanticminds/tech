/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quanticminds.samples.game01.components;

import com.jme3.math.Vector3f;

/**
 *
 * @author adriano.ribeiro
 */
public final class ProfileComponent {
    
    Vector3f walkSpeed;
    Vector3f walkSpeedMax;
    Vector3f turnSpeed;

    /**
     *
     * @param walkSpeed
     * @param walkSpeedMax
     * @param turnSpeed
     */
    public ProfileComponent(Vector3f walkSpeed, Vector3f walkSpeedMax, Vector3f turnSpeed) {
        this.walkSpeed = walkSpeed;
        this.walkSpeedMax = walkSpeedMax;
        this.turnSpeed = turnSpeed;
    }

    /**
     *
     * @return
     */
    public Vector3f getTurnSpeed() {
        return turnSpeed;
    }

    /**
     *
     * @return
     */
    public Vector3f getWalkSpeed() {
        return walkSpeed;
    }

    /**
     *
     * @return
     */
    public Vector3f getWalkSpeedMax() {
        return walkSpeedMax;
    }
}
