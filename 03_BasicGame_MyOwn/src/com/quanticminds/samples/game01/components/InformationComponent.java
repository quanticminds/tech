/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quanticminds.samples.game01.components;

import com.quanticminds.tech.Component;

/**
 *
 * @author adriano
 */
public class InformationComponent extends Component{

    String name;

    /**
     *
     * @param name
     */
    public InformationComponent(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     */
    public String getName() {
        return name;
    }
}
