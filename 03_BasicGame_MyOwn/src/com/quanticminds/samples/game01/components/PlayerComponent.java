/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quanticminds.samples.game01.components;

import com.quanticminds.tech.Component;

/**
 *
 * @author adriano
 */
public class PlayerComponent extends Component{

    String name;
    boolean controlActive;

    /**
     *
     * @param name
     * @param controlActive
     */
    public PlayerComponent(boolean controlActive) {
        this.controlActive = controlActive;
    }

    /**
     *
     * @return
     */
    public boolean isControlActive() {
        return controlActive;
    }
}
