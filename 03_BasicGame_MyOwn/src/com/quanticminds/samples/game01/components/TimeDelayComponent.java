/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quanticminds.samples.game01.components;

import com.quanticminds.tech.Component;

/**
 *
 * @author adriano
 */
public class TimeDelayComponent extends Component{
    private float delayTime;
    private float elapsedTime;

    /**
     *
     * @param delayTime
     * @param elapsedTime
     */
    public TimeDelayComponent(float delayTime, float elapsedTime) {
        this.delayTime = delayTime;
        this.elapsedTime = elapsedTime;
    }
    
    /**
     *
     * @return
     */
    public boolean isElapsedTimeOk() {
        return  this.elapsedTime >= this.delayTime;
    }
    
    /**
     *
     * @return
     */
    public float getDelayTime() {
        return delayTime;
    }

    /**
     *
     * @return
     */
    public float getElapsedTime() {
        return elapsedTime;
    }
}
