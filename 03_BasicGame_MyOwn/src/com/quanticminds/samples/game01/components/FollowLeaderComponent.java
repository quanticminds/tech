/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quanticminds.samples.game01.components;

import com.jme3.math.Vector3f;
import com.quanticminds.tech.Component;

/**
 *
 * @author adriano
 */
public class FollowLeaderComponent extends Component{

    float distance;
    Vector3f start = new Vector3f();
    Vector3f finish = new Vector3f();

    /**
     *
     * @param distance
     */
    public FollowLeaderComponent(float distance, Vector3f start, Vector3f finish) {
        this.distance = distance;
        this.start.set(start);
        this.finish.set(finish);
    }

    /**
     *
     * @return
     */
    public float getDistance() {
        return distance;
    }

    /**
     *
     * @return
     */
   
    public Vector3f getStart() {
        return start;
    }

    /**
     *
     * @return
     */
    public Vector3f getFinish() {
        return finish;
    }

    
}
