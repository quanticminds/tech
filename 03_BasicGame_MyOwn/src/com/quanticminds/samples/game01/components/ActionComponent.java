/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quanticminds.samples.game01.components;

import com.quanticminds.tech.Component;

/**
 *
 * @author adriano.ribeiro
 */
public class ActionComponent extends Component{

    String name;
    boolean jumped;

    /**
     *
     * @param name
     */
    public ActionComponent(String name, boolean jumped) {
        this.name = name;
        this.jumped = jumped;
    }


    /**
     *
     * @return
     */
    public String getName() {
        return name;
    }

    public boolean isJumped() {
        return jumped;
    }
}
